import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../Utility/index"
import Icon from 'react-native-vector-icons/Entypo'
import Ionicons from "react-native-vector-icons/Ionicons"
import FontAwesome from "react-native-vector-icons/FontAwesome"

const SubHeader = ({ navigation, onPress, title, showBack, click,showFilter,filterPress }) => {

    return (

        <View>
            {showBack == true ?
                <View style={{

                    flexDirection: 'row',
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 6,
                    },
                    shadowOpacity: 0.39,
                    shadowRadius: 8.30,

                    elevation: 13,
                    backgroundColor: "white",
                    justifyContent: "space-between", height: 49, alignItems: "center", paddingLeft: 14, paddingRight: 10
                }}>
                    <TouchableOpacity onPress={click}>
                        <Icon
                            style={styles.icon}
                            name="arrow-long-left"
                            size={24}
                            color="black"
                        />
                    </TouchableOpacity>
                    <Text style={{ fontSize: 17, color: "black", }}>{title}</Text>
                    {/* <TouchableOpacity onPress={filterPress}> */}
                    <TouchableOpacity>
                    <FontAwesome
                            style={styles.icon,[{marginRight:10}]}
                            name={showFilter==true? "":''}
                            size={20}
                            color="#0aa116"
                        />
</TouchableOpacity>
                </View>
                :
                <View style={{
                    flexDirection: 'row',
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 6,
                    },
                    shadowOpacity: 0.39,
                    shadowRadius: 8.30,

                    elevation: 13,
                    backgroundColor: "white",
                    justifyContent: "space-between", height: 49, alignItems: "center", paddingLeft: 14, paddingRight: 10
                }}>
                    {/* <Text style={{ fontSize: wp('6%'), color: "black", }}>𝐁𝐢𝐳𝐀𝐫𝐦𝐲</Text> */}
                    <Image source={require("../Assets/Icons/Biz-Army-LOGO-black.png")} style={{ height: 49, width: 100, }} resizeMode={"contain"}></Image>

                    <View style={{ flexDirection: "row" }}>
                        <Ionicons
                            style={styles.iconnn}
                            name="search"
                            size={24}
                            color="black"
                        />
                        <TouchableOpacity onPress={() => navigation.navigate('Notification')}>

                            <Icon
                                style={styles.iconnn}
                                name="bell"
                                size={24}
                                color="black"
                            />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.openDrawer()}>
                            <Icon
                                style={styles.iconnn}
                                name="menu"
                                size={24}
                                color="black"
                            />
                        </TouchableOpacity>
                    </View>

                </View>}
        </View>

    );
};

export default SubHeader;

const styles = StyleSheet.create({
    submitView: {
        borderWidth: 1, backgroundColor: "#0aa116", flexDirection: "row", justifyContent: "space-between",
        marginTop: hp("5%"), width: wp("80%"), height: hp("7%"), borderColor: "#0aa116", alignSelf: "center"
    },
    submitText: { color: "white", fontSize: 17, textAlign: "center", marginTop: hp("1.5%"), marginLeft: wp("4%") },
    iconnn: { alignSelf: "flex-end", marginRight: 10 },
});