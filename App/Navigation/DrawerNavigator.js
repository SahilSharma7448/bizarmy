import React from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerData from "../Constants/Drawer"
import BottomTab from './BottomTab';
import Home from "../Screens/Home"
const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
    return (
        <Drawer.Navigator
            drawerContent={DrawerData}
            drawerContentOptions={{
                activeTintColor: 'black',
            }}>
            <Drawer.Screen
                name="Home"
                component={Home}
                options={{ drawerLabel: 'open drawer' }}
            />
        </Drawer.Navigator>
    );
};

export default DrawerNavigator;
