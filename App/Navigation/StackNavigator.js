import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Splash from '../Screens/AuthScreens/Splash';
import Register from "../Screens/AuthScreens/Reigster"
import Login from "../Screens/AuthScreens/Login"
import ForgotPassword from "../Screens/AuthScreens/ForgotPassword"
import DashBoard from "../Screens/TabScreens/DashBoard"
import PD from "../Screens/TabScreens/PD"
import Add from "../Screens/TabScreens/Add"
import Bussiness from "../Screens/TabScreens/Bussiness"
import Invoices from "../Screens/TabScreens/Invoices"
import Notification from "../Screens/Notification"
import DashBoardDetail from "../Screens/TabScreens/DashBoard/DashBoardDetail"
import CommiteMember from "../Screens/CommiteMember"
import MyProfile from "../Screens/MyProfile"
import BottomTab from "./BottomTab"
import SideDrawer from "./DrawerNavigator"
import Home from "../Screens/Home"
import PdList from "../Screens/TabScreens/PdList"
import RecommendationList from "../Screens/TabScreens/RecommendationList"
import MemberDetail from "../Screens/MemberDetail"
import Attendence from "../Screens/Attendence"
import AboutUs from "../Screens/AppDetail/AboutUs"
import ReadMore from "../Screens/ReadMore"
import MemberShip from "../Screens/MemberShip"
import PdfView from "../Screens/PdfView"
import EditRecommendation from "../Screens/TabScreens/EditRecommendation"
import CreateStory from "../Screens/StoryModule/CreateStory"
import CreateStrong from "../Screens/StoryModule/CreateStrong"
import CreateVistor from "../Screens/StoryModule/CreateVisitor"
import EventList from "../Screens/StoryModule/EventList"
import EventDetail from "../Screens/StoryModule/EventDetail"
import StrongList from "../Screens/StoryModule/StrongList"
import SuccessList from "../Screens/StoryModule/SuccessList"
import SuccessDetail from "../Screens/StoryModule/SuccessDetail"
import CreateSuccess from "../Screens/StoryModule/CreateSuccess"
import UpdateStory from "../Screens/StoryModule/UpdateStory"
import EditPD from "../Screens/TabScreens/EditPD"
import SchemeList from "../Screens/StoryModule/SchemeList"
import EditScheme from "../Screens/StoryModule/EditScheme"
import GuestUser from "../Screens/TabScreens/GuestUser"
import Badges from "../Screens/TabScreens/Badges"
const Stack = createStackNavigator();

const MainStackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="DashBoard"
        component={DashBoard}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PD"
        component={PD}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Add"
        component={Add}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Bussiness"
        component={Bussiness}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Invoices"
        component={Invoices}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="BottomTab"
        component={BottomTab}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SideDrawer"
        component={SideDrawer}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Notification"
        component={Notification}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="DashBoardDetail"
        component={DashBoardDetail}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="MyProfile"
        component={MyProfile}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="CommiteMember"
        component={CommiteMember}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PdList"
        component={PdList}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="RecommendationList"
        component={RecommendationList}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="MemberDetail"
        component={MemberDetail}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Attendence"
        component={Attendence}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="AboutUs"
        component={AboutUs}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ReadMore"
        component={ReadMore}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="MemberShip"
        component={MemberShip}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="EditRecommendation"
        component={EditRecommendation}
        options={{ headerShown: false }}
      />
        <Stack.Screen
        name="PdfView"
        component={PdfView}
        options={{ headerShown: false }}
      />
        <Stack.Screen
        name="CreateStory"
        component={CreateStory}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="CreateStrong"
        component={CreateStrong}
        options={{ headerShown: false }}
      />
        <Stack.Screen
        name="CreateVistor"
        component={CreateVistor}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="EventList"
        component={EventList}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="EventDetail"
        component={EventDetail}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="StrongList"
        component={StrongList}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="SuccessList"
        component={SuccessList}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="SuccessDetail"
        component={SuccessDetail}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="CreateSuccess"
        component={CreateSuccess}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="UpdateStory"
        component={UpdateStory}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="EditPD"
        component={EditPD}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="SchemeList"
        component={SchemeList}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="EditScheme"
        component={EditScheme}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="GuestUser"
        component={GuestUser}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="Badges"
        component={Badges}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

export { MainStackNavigator };
