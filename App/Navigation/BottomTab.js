import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Alert, Image, StyleSheet } from 'react-native';
import DashBoardDetail from "../Screens/TabScreens/DashBoard/DashBoardDetail"
import DashBoard from "../Screens/TabScreens/DashBoard"
import PD from "../Screens/TabScreens/PD"
import Add from "../Screens/TabScreens/Add"
import Bussiness from "../Screens/TabScreens/Bussiness"
import Invoices from "../Screens/TabScreens/Invoices"
import Icon from 'react-native-vector-icons/FontAwesome'
import Ionicons from "react-native-vector-icons/Ionicons"
import Octicons from 'react-native-vector-icons/Octicons'
const Tab = createBottomTabNavigator();
const BottomTab = () => {
    return (
        <Tab.Navigator tabBarOptions={{ activeTintColor: "#0aa116", labelStyle: { fontSize: 10, width: 100, fontWeight: "600" } }}>
            <Tab.Screen

                name="My DashBoard"
                component={DashBoard}

                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        // <Octicons
                        //     name='graph'
                        //     size={17}
                        //     color={'#0aa116'}

                        // />
                        <Image source={require("../Assets/Icons/dashboard.png")} style={{ height: 20, width: 20, }} resizeMode={"contain"}></Image>

                    ),
                })}
            />

            <Tab.Screen
                name="PD's"
                component={PD}
                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        // <Icon
                        //     name='users'
                        //     size={17}
                        //     color={'#0aa116'}

                        // />
                        <Image source={require("../Assets/Icons/PD.png")} style={{ height: 20, width: 20, }} resizeMode={"contain"}></Image>

                    ),
                })}

            />
            <Tab.Screen
                name=" "
                component={Add}
                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        <Ionicons
                            name='add-circle-outline'
                            size={49}
                            color={'#0aa116'}
                            style={{ marginTop: 10 }}
                        />
                    ),
                })}
            />
            <Tab.Screen

                name="Bussiness Booked"
                component={Bussiness}

                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        // <Icon
                        //     name='handshake-o'
                        //     size={17}
                        //     color={'#0aa116'}

                        // />
                        <Image source={require("../Assets/Icons/BussinessLogo.png")} style={{ height: 30, width: 30, }} resizeMode={"contain"}></Image>

                    ),
                })}
            />
            <Tab.Screen

                name="Invoices"
                component={Invoices}

                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        // <Icon
                        //     name='file-text-o'
                        //     size={17}
                        //     color={'#0aa116'}

                        // />
                        <Image source={require("../Assets/Icons/Invoices.png")} style={{ height: 20, width: 20, }} resizeMode={"contain"}></Image>

                    ),
                })}
            />
        </Tab.Navigator>
    );
};

export default BottomTab;
const styles = StyleSheet.create({

    tabImg2: {
        alignSelf: 'center',
        tintColor: 'grey',
        height: 25,
        width: 30,
        backgroundColor: "red"
    },
    selectedTabImg2: {
        alignSelf: 'center',
        tintColor: 'black',
        height: 25,
        width: 30,
        backgroundColor: "red"

    },

});
