import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground,BackHandler, Image, ScrollView, FlatList } from 'react-native';
import MainHeader from '../../Components/MainHeader';
import { MyMemberShip } from "../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../Utility"
const MemberShip = ({ navigation }) => {
    const dispatch = useDispatch()
    const [detail, setDetail] = useState('')
    const listData = [
        { name: 'rajat' },
        { name: 'rajat' },
        { name: 'rajat' },
        { name: 'rajat' },
        // { name: 'rajat' },
        // { name: 'rajat' },

    ]



    useEffect(() => {

        getDropDownData()
    }, []);
    const getDropDownData = async () => {


        let res = await dispatch(MyMemberShip())
        // await setMember(response.data)
        if (res.status == true) {
            setDetail(res.msg)
        }
    }
    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);
    return (

        <View>
            <MainHeader navigation={navigation} showBack={true} title={'My MemberShip'}></MainHeader>
            <ScrollView>
                <Text style={{ marginLeft: 20, marginRight: 20, marginTop: 20, fontSize: 17 }}>{detail}</Text>
            </ScrollView>
        </View>
    );
};

export default MemberShip;

