import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground,BackHandler, Image, ScrollView,Linking, FlatList,TouchableOpacity } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import { useDispatch, useSelector } from "react-redux"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import FontAwesome from "react-native-vector-icons/FontAwesome"
import Ionicons from "react-native-vector-icons/Ionicons"
import { List_Scheme } from "../../../Redux/Action"
import * as Utility from "../../../Utility"
import Clipboard from '@react-native-clipboard/clipboard';

var userId=''
const SchemeList = ({ navigation,handlePress }) => {
 const dispatch=useDispatch()
 const[connectData,setConnectData]=useState([])
    // useEffect(() => {

    //     getDropDownData()
    // }, []);

    useEffect(() => {
      getDropDownData()


      const unsubscribe = navigation.addListener('focus', () => {
          getDropDownData()

      });

      return () => {
          unsubscribe;
      };
  }, [navigation]);
    const getDropDownData=async()=>{
        let res=await dispatch(List_Scheme())
        if(res.status==true){
            let id=await Utility.getFromLocalStorge('userId')
            userId=id
            setConnectData(res.data)
        }
    }
    useEffect(() => {
       
        BackHandler.addEventListener('hardwareBackPress', goToBack);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', goToBack);
        };
      }, []);

      const goToBack=()=>{
          navigation.goBack()
          return true
      }
      const openWhatsApp = (mobile) => {
        let msg = 'hello';
        if (mobile) {
          if (msg) {
            let url =
              "whatsapp://send?text=" +
            '' +
              "&phone=91" +
              mobile;
            Linking.openURL(url)
              .then(data => {
                console.log("WhatsApp Opened successfully " );
              })
              .catch(() => {
                alert("Make sure WhatsApp installed on your device");
              });
          } else {
            alert("Please enter message to send");
          }
        } else {
          alert("Please enter mobile no");
        }
      };
    return (

        <View>
            <MainHeader navigation={navigation} showBack={true} title={' Scheme'} showPlus={true}   pressPlus={()=>navigation.navigate('CreateSuccess')}></MainHeader>
          
                <ScrollView style={{marginBottom:20}}>
                <FlatList
                    data={connectData}
                    renderItem={({ item }) =>
<View>
                  
                            <View style={{


                                backgroundColor: "white",
                                shadowColor: "green",
                                shadowOffset: {
                                    width: 0,
                                    height: 4,
                                },
                                shadowOpacity: 0.20,
                                shadowRadius: 4.30,

                                elevation: 3,
                                alignSelf: "center", width: wp("90%"),
                                // padding: 10,
                                marginTop: 20,
                                marginBottom: 20,
                                borderColor:item.user_id==userId? "#ffd633":'#0aa116',
                                borderWidth: 2,
                                borderRadius: 10,
                                paddingBottom: 10,
                            }}>
                                 { item.user_id==userId?
                                     <TouchableOpacity onPress={()=>navigation.navigate('EditScheme',{
                                         item:item
                                     })}>
                                    <FontAwesome
                                        name='edit'
                                        size={20}
                                        color='grey'
                                        style={{marginRight:20,marginTop:5,alignSelf:"flex-end" }}
                                    />
                                    </TouchableOpacity>:null}

                                <View style={{ flexDirection: "row", marginTop: 10 }}>
                                    <View style={{ flexDirection: "column", width: wp('37%'), paddingLeft: 10 }}>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10, }}>Scheme Name </Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Scheme Starting Date</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Scheme Ending Date</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Scheme Description</Text>



                                    </View>
                                    <View style={{ flexDirection: "column", width: wp('43%'), paddingLeft: 10 }}>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10, }}>:  {item.scheme_name}</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>:  {item.scheme_starting_date }</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>:  {item.scheme_ending_date}</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>:  {item.scheme_description}</Text>



                                    </View>
                                   
                                </View>

                            </View>
                    </View>
                    }
                    />
                  
                    
                 
                </ScrollView>


        </View>
    );
};

export default SchemeList;

