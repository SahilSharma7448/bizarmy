import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image, ScrollView,BackHandler, FlatList } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import { Input, CheckBox, BottomSheet, ListItem } from 'react-native-elements';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import * as Utility from "../../../Utility"

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import { TouchableOpacity } from 'react-native-gesture-handler';
import { PostPDRecords, PdListRecordApi } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
import FontAwesome from "react-native-vector-icons/FontAwesome"
import { Event_List_api } from "../../../Redux/Action"

const EventList = ({ navigation,handlePress }) => {
 const EventData=[
     {id:1},
     {id:2},

 ]

 const dispatch=useDispatch()
 const[connectData,setConnectData]=useState([])
    useEffect(() => {

        getDropDownData()
    }, []);
    const getDropDownData=async()=>{
        let res=await dispatch(Event_List_api())
        if(res.status==true){
           
            setConnectData(res.data)
        }
    }
    useEffect(() => {
     
        BackHandler.addEventListener('hardwareBackPress', goToBack);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', goToBack);
        };
      }, []);

      const goToBack=()=>{
          navigation.goBack()
          return true
      }
const goToDetail=()=>{

}
    return (

        <View style={{}}>
            <MainHeader navigation={navigation} showBack={true} title={'Events'}></MainHeader>
            <View style={{ marginBottom: 50 }}>
                <FlatList
                    data={connectData}
                    renderItem={({ item }) =>
                    <TouchableOpacity onPress={()=>navigation.navigate('EventDetail',{item:item})}>
                        <View style={{
                            backgroundColor: "white",
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 6,
                            },
                            shadowOpacity: 0.39,
                            shadowRadius: 8.30,

                            elevation: 7,
                            alignSelf: "center", width: wp("90%"),
                            padding: 10,
                            marginTop: 30,
                            marginBottom: 10,
                            borderColor:"#0aa116",
                            borderWidth:1,
                            borderRadius:8
                        }}>
                            <View style={{ flexDirection: "row", }}>
                                <Image
                                    source={{ uri:'http://dilkhushpvtltd.com/bizarmy/public/' + item.image}}
                                    style={{
                                        alignSelf: "center", height: hp('10%'), width: wp("24%"),

                                    }}></Image>
                                <View style={{ width: wp("60%"), alignItems: 'flex-start',marginLeft:16, flexDirection: "column" }}>
                                    <Text style={{ fontSize: 15, fontWeight: 'bold', lineHeight: 24 }}>{item.title}</Text>
                                    <Text style={{ fontSize: 10, fontWeight: '100' }}>Date : {item.event_date}</Text>
                                    <Text style={{ fontSize: 10, fontWeight: '100' }}>Location : {item.location} </Text>

                                </View>
                                {/* <FontAwesome
                                        name='whatsapp'
                                        size={27}
                                        color='#0aa116'
                                        style={{ marginTop: 30 }}
                                    /> */}
                            </View>
                            <Text style={{ fontSize: 12,marginTop:hp('2%') }}> Description </Text>

                            <View style={{ flexDirection: "row", marginTop: 10 }}>
                                <View style={{ borderTopColor: "black", borderTopWidth: 2, width: wp("80%"),  }}></View>
                            </View>
                            <Text style={{ fontSize: 12, fontWeight: '100', marginTop: 8 }}>{item.detail}</Text>

                        </View>
                        </TouchableOpacity>
                    }
                /> 
            </View>


        </View>
    );
};

export default EventList;

