import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground,BackHandler, Image, ScrollView,Linking, FlatList,TouchableOpacity } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import { useDispatch, useSelector } from "react-redux"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import FontAwesome from "react-native-vector-icons/FontAwesome"
import Ionicons from "react-native-vector-icons/Ionicons"
import { Get_Connects } from "../../../Redux/Action"
import * as Utility from "../../../Utility"
import Clipboard from '@react-native-clipboard/clipboard';

var userId=''
const StrongList = ({ navigation,handlePress }) => {
 const dispatch=useDispatch()
 const[connectData,setConnectData]=useState([])
    useEffect(() => {

        getDropDownData()
    }, []);
    const getDropDownData=async()=>{
        let res=await dispatch(Get_Connects())
        if(res.status==true){
            let id=await Utility.getFromLocalStorge('userId')
            userId=id
            setConnectData(res.data)
        }
    }
    useEffect(() => {
       
        BackHandler.addEventListener('hardwareBackPress', goToBack);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', goToBack);
        };
      }, []);

      const goToBack=()=>{
          navigation.goBack()
          return true
      }
      const openWhatsApp = (mobile) => {
        let msg = 'hello';
        if (mobile) {
          if (msg) {
            let url =
              "whatsapp://send?text=" +
            '' +
              "&phone=91" +
              mobile;
            Linking.openURL(url)
              .then(data => {
                console.log("WhatsApp Opened successfully " );
              })
              .catch(() => {
                alert("Make sure WhatsApp installed on your device");
              });
          } else {
            alert("Please enter message to send");
          }
        } else {
          alert("Please enter mobile no");
        }
      };
    return (

        <View>
            <MainHeader navigation={navigation} showBack={true} title={'Strong Connects'} showPlus={true} pressPlus={()=>navigation.navigate('CreateStrong')}></MainHeader>
          
                <ScrollView style={{marginBottom:20}}>
                <FlatList
                    data={connectData}
                    renderItem={({ item }) =>
<View>
                  
                            <View style={{


                                backgroundColor: "white",
                                shadowColor: "green",
                                shadowOffset: {
                                    width: 0,
                                    height: 4,
                                },
                                shadowOpacity: 0.20,
                                shadowRadius: 4.30,

                                elevation: 3,
                                alignSelf: "center", width: wp("90%"),
                                // padding: 10,
                                marginTop: 20,
                                marginBottom: 20,
                                borderColor:item.user_id==userId? "#ffd633":'#0aa116',
                                borderWidth: 2,
                                borderRadius: 10,
                                paddingBottom: 10,
                            }}>
                                <View style={{ flexDirection: 'row', justifyContent: "space-between",marginTop:15 }}>
                                    <View style={{ height: hp('7%'), width: wp('60%'), backgroundColor:item.user_id==userId? "#ffd633":'#0aa116', paddingLeft:10, justifyContent: "center" }}>
                                        <Text style={{ fontSize: 12, color: "white" ,fontWeight:"bold"}}>{item.referar_name}</Text>
                                        <Text style={{ fontSize: 9, color: "white" }}>Referred by  {item.name}</Text>

                                    </View>
                                </View>

                                <View style={{ flexDirection: "row", marginTop: 20 }}>
                                    <View style={{ flexDirection: "column", width: wp('34%'), paddingLeft: 10 }}>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10, }}>Category </Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Firm Name</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Contact No</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Email</Text>



                                    </View>
                                    <View style={{ flexDirection: "column", width: wp('43%'), paddingLeft: 10 }}>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10, }}>: {item.category_id}</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: {item.firm_name}</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: {item.mobile_no}</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: {item.email}</Text>



                                    </View>
                                    <View>
                                    <TouchableOpacity onPress={()=>openWhatsApp(item.mobile_no)}>
                                    <FontAwesome
                                        name='whatsapp'
                                        size={20}
                                        color='#0aa116'
                                        style={{ marginTop: hp('8%') }}
                                    />
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                    onPress={()=> Clipboard.setString(item.email)}
                                    >
                                    <Ionicons
                                        name='copy'
                                        size={20}
                                        color='grey'
                                        style={{ marginTop: 10 }}

                                    />
                                    <Text style={{ fontSize: 8, color: "grey" }}>copy </Text>
                                    </TouchableOpacity>
                                </View>
                                </View>

                            </View>
                    </View>
                    }
                    />
                  
                    
                 
                </ScrollView>


        </View>
    );
};

export default StrongList;

