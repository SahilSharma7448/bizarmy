import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text,BackHandler, ImageBackground, Image, ScrollView,ToastAndroid, Button, TextInput } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import styles from "./style"
import { Input, CheckBox, } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Picker } from '@react-native-community/picker';
import { GetBusinessCategory,Create_Visitor } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
import * as Utility from "../../../Utility"

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import { TouchableOpacity } from 'react-native-gesture-handler';
const CreateVisitor = ({ navigation }) => {
    const dispatch = useDispatch()

    const [bussinessCategory, setBusniessCategory] = useState([])
    const [selectedValue, setSelectedValue] = useState("");
    const [loading, setLoading] = useState(false)

const [name,setName]=useState('')
const [mobileNo,setMobileNo]=useState('')
const [category,setCategory]=useState('')
const [firmName,setFirmName]=useState('')
const [email,setEmail]=useState('')

    useEffect(() => {

        getDropDownData()
    }, []);
    const getDropDownData = async () => {

     
        let response = await dispatch(GetBusinessCategory())
        await setBusniessCategory(response.data)
    }

    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);

    const submit=async()=>{
        if(await Utility.isFieldEmpty(name,mobileNo,category,firmName,email)){
    ToastAndroid.show("Please fill all fields.", ToastAndroid.showWithGravityAndOffset);

        }
        else if(await Utility.isValidEmail(email)){
    ToastAndroid.show("Please fill valid email", ToastAndroid.showWithGravityAndOffset);

        }
        else{
            setLoading(true)
            let res=await dispatch(Create_Visitor(name,mobileNo,category,firmName,email))
       if(res==true){
          await  setLoading(false)
    ToastAndroid.show("Connects Add Successfully", ToastAndroid.showWithGravityAndOffset);
navigation.navigate('SideDrawer')
       }
       else{
        await  setLoading(false)
        ToastAndroid.show("SomeWent Wrong.", ToastAndroid.showWithGravityAndOffset);

       }
        }
    }
    return (

        <View>
<Loader isLoading={loading}></Loader>

            <MainHeader navigation={navigation} showBack={true} title={'Add Visitor'}></MainHeader>
            <ScrollView>
                <View style={{ marginTop: 30 }}>



                 
                  
                <Text style={styles.inputTitle}>Name</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            placeholder={"Enter Name"}
                            onChangeText={(text)=>setName(text)}
                        />
                    </View>


                    <Text style={styles.inputTitle}>Mobile No</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            placeholder={"Enter Mobile No "}
                            onChangeText={(text)=>setMobileNo(text)}

keyboardType={"number-pad"}
                        />
                    </View>

                    <Text style={styles.inputTitle}>Category</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            placeholder={"Enter Category"}
                            onChangeText={(text)=>setCategory(text)}

                        />
                    </View>


                    <Text style={styles.inputTitle}>Firm Name</Text>
                    <View style={styles.inputMainView}>

                        <Input
                            placeholder={"Enter Firm Name "}
                            onChangeText={(text)=>setFirmName(text)}

                        />
                    </View>

                 





               

              
                    <Text style={styles.inputTitle}>Email </Text>
                    <View style={styles.inputMainView}>

                        <Input
                            placeholder={"Enter Email  "}
                            onChangeText={(text)=>setEmail(text)}

                        />
                    </View>

                    <TouchableOpacity style={{marginTop:20}} onPress={()=>submit()}>
                        <View style={styles.submitBtn}>
                            <Text style={{ color: "white" }}>Submit</Text>

                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
          
        </View>
    );
};

export default CreateVisitor;

