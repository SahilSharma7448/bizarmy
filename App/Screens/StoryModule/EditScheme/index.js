import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground,BackHandler, Image, ScrollView, Alert ,ToastAndroid} from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import styles from "./style"
import { Input, CheckBox, BottomSheet, ListItem } from 'react-native-elements';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import * as Utility from "../../../Utility"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import { TouchableOpacity } from 'react-native-gesture-handler';
import { PostPDRecords, GetMyBridage,Update_Scheme } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
import DateTimePicker from '@react-native-community/datetimepicker';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from "moment";
import { Picker } from '@react-native-community/picker';

const EditScheme = ({route, navigation }) => {
    const {item}=route.params
    const dispatch = useDispatch()
    const [loading, setLoading] = useState(false)
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [showEnd,setShowEnd]=useState(false)
    const [SelectedDate, setSelectedDate] = useState(item.scheme_starting_date)
    const [SelectedDateEnd, setSelectedDateEnd] = useState(item.scheme_ending_date)
const [name,setName]=useState(item.scheme_name)
const [des,setDes]=useState(item.scheme_description)


   
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
        let dates = moment(selectedDate).format("YYYY-MM-DD")
        console.log("check selected date", dates)
        setSelectedDate(dates)
        setShow(false)
        setShowEnd(false)
    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {

        showMode('date');
    };

   



    const onChange2 = async(event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
        let dates = moment(selectedDate).format("YYYY-MM-DD")
        console.log("check selected date", dates)
        setSelectedDateEnd(dates)
        setShow(false)
       await setShowEnd(false)

    };

    const showMode2 = (currentMode) => {
        setShowEnd(true);
        setMode(currentMode);
    };

    const showDatepicker2 = () => {
        showMode2('date');
    };

    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);

    const submit=async()=>{
if(await Utility.isFieldEmpty(name,SelectedDate,SelectedDateEnd,des)){
    ToastAndroid.show("Please fill all fields.", ToastAndroid.showWithGravityAndOffset);

}
else {
    setLoading(true)
    let res=await dispatch(Update_Scheme(item.id,name,SelectedDate,SelectedDateEnd,des))
    if(res==true){
        setLoading(false)
    ToastAndroid.show("Scheme Update Successfully.", ToastAndroid.showWithGravityAndOffset);
navigation.navigate('SchemeList')
    }
    else{
        setLoading(false)

    ToastAndroid.show("SomeWent Wrong.", ToastAndroid.showWithGravityAndOffset);

    }
}
    }
    return (

        <View>
<Loader isLoading={loading}></Loader>
            <MainHeader navigation={navigation} showBack={true} title={'Update Scheme'} ></MainHeader>
            <ScrollView>
               <View style={{marginTop:hp('5%')}}>
            <Text style={styles.inputTitle}>Scheme Name</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            placeholder={"Enter Name"}
                            onChangeText={(text)=>setName(text)}
                            value={name}
                        />
                    </View>
                <Text style={styles.inputTitle}>Scheme Strating Date</Text>
                <TouchableOpacity onPress={showDatepicker}>

                <View style={styles.inputMainView}>
                    <View style={{flexDirection:"row"}}>
                   
                    <Text style={{marginLeft:10,height:hp('7%'),marginTop:10, justifyContent:"center",alignItems:"center",width:wp('69%')}}>{SelectedDate==''?'YYYY/MM/DD':SelectedDate}</Text>
                    <Icon
                                    name='chevron-down'
                                    size={17}
                                    color='black'
                                    style={{alignSelf:"center",alignItems:"center",justifyContent:"center"}}

                                />
                    </View>
                </View>
                </TouchableOpacity>
                <View style={{ borderBottomColor: "black", backgroundColor: "#dfdfdf", borderBottomWidth: 1, width: "74%", marginBottom: 20, alignSelf: "center", marginRight: 20 }}>

</View>
                <Text style={styles.inputTitle}>Scheme Ending Date</Text>
                <TouchableOpacity onPress={showDatepicker2}>

                <View style={styles.inputMainView}>
                    <View style={{flexDirection:"row"}}>
                   
                    <Text style={{marginLeft:10,height:hp('7%'),marginTop:10, justifyContent:"center",alignItems:"center",width:wp('69%')}}>{SelectedDateEnd==''?'YYYY/MM/DD':SelectedDateEnd}</Text>
                    <Icon
                                    name='chevron-down'
                                    size={17}
                                    color='black'
                                    style={{alignSelf:"center",alignItems:"center",justifyContent:"center"}}

                                />
                    </View>
                </View>
                </TouchableOpacity>
                <View style={{ borderBottomColor: "black", backgroundColor: "#dfdfdf", borderBottomWidth: 1, width: "74%", marginBottom: 20, alignSelf: "center", marginRight: 20 }}>

</View>
<Text style={styles.inputTitle}>Scheme Detail</Text>
                <View style={styles.inputMainView}>
                    <Input
                        style={styles.sqaureBorderInput}
                        onChangeText={(text) => setDes(text)}
                        multiline={true}
                        textAlign="left"
                        value={des}

                    />
                </View>
                </View>
                <TouchableOpacity onPress={() => submit()}>
                    <View style={styles.submitBtn}>
                        <Text style={{ color: "white" }}>Submit</Text>

                    </View>
                </TouchableOpacity>
               </ScrollView>
            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                />
            )}
            {showEnd && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={onChange2}
                />
            )}
        </View>
    );
};

export default EditScheme;

