import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text,BackHandler, ImageBackground, Image, ScrollView,ToastAndroid, Button, TextInput } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import styles from "./style"
import { Input, CheckBox, } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Picker } from '@react-native-community/picker';
import { GetBridageCategory,update_story } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
import * as Utility from "../../../Utility"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import { TouchableOpacity } from 'react-native-gesture-handler';
const UpdateStory = ({route, navigation }) => {
    const {item}=route.params
    const dispatch = useDispatch()
    const [bussinessCategory, setBusniessCategory] = useState([])
    const [myBridage, setBridage] = useState([])
    const [selectedValue, setSelectedValue] = useState("");
    const [title, setTitle] = useState(item.title)

    const [loading, setLoading] = useState(false)

    const [story , setStory] = useState(item.story)
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [SelectedDate, setSelectedDate] = useState('')
    useEffect(() => {

        getDropDownData()
    }, []);
    const getDropDownData = async () => {

     
        let response = await dispatch(GetBridageCategory())
        await setBridage(response.data)
    }
  
    

    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);

    const submit=async()=>{
        setLoading(true)
        let res=await dispatch(update_story(item.id,title,item.member_id,item.brigade_id,story))
   if(res.status==true){
       setLoading(false)
    ToastAndroid.show(res.data, ToastAndroid.showWithGravityAndOffset);
navigation.navigate('SuccessList')
   }
   else{
    setLoading(false)
    ToastAndroid.show("SomeWent Wrong.", ToastAndroid.showWithGravityAndOffset);


   }
    }
    return (

        <View>
            <Loader isLoading={loading}></Loader>

            <MainHeader navigation={navigation} showBack={true} title={'Update Story'}></MainHeader>
            <ScrollView>
                <View style={{ marginTop: 30 }}>



                 
                  



                    <Text style={styles.inputTitle}>Title</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            placeholder={"Enter Title "}
value={title}
onChangeText={(text)=>setTitle(text)}
                        />
                    </View>



              
                    <Text style={styles.inputTitle}>Story</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            style={styles.sqaureBorderInput}
                          value={story}
                          onChangeText={(text)=>setStory(text)}
                        />
                    </View>
                    <TouchableOpacity onPress={()=>submit()}>
                        <View style={styles.submitBtn}>
                            <Text style={{ color: "white" }}>Submit</Text>

                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
          
        </View>
    );
};

export default UpdateStory;

