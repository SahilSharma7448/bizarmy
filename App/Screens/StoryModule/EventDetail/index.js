import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image, ScrollView,BackHandler, FlatList } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import { Input, CheckBox, BottomSheet, ListItem } from 'react-native-elements';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import * as Utility from "../../../Utility"

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import { TouchableOpacity } from 'react-native-gesture-handler';
import { PostPDRecords, PdListRecordApi } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
import FontAwesome from "react-native-vector-icons/FontAwesome"

const EventDetail = ({route, navigation,handlePress }) => {
    const {item}=route.params
 const EventData=[
     {id:1},
     {id:2},

 ]
    useEffect(() => {
     
        BackHandler.addEventListener('hardwareBackPress', goToBack);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', goToBack);
        };
      }, []);

      const goToBack=()=>{
          navigation.goBack()
          return true
      }

    return (

        <View style={{}}>
            <MainHeader navigation={navigation} showBack={true} title={'Event Detail'}></MainHeader>
            {/* <View style={{ marginBottom: 200 }}> */}
              <ScrollView>
                        <View style={{
                          
                            alignSelf: "center", width: wp("90%"),
                            padding: 10,
                            marginTop: 30,
                            marginBottom: 10,
                         
                        }}>
                            <View style={{ flexDirection: "row",justifyContent:"space-between" }}>
                              
                                <View style={{ width: wp("80%"), alignItems: 'flex-start', flexDirection: "column" }}>
                                    <Text style={{ fontSize: 15, fontWeight: 'bold', lineHeight: 24 }}>{item.title}</Text>
                                    <Text style={{ fontSize: 10, fontWeight: '100' }}>Date : {item.event_date}</Text>
                                    <Text style={{ fontSize: 10, fontWeight: '100' }}>Location : {item. location}</Text>

                                </View>
                                {/* <FontAwesome
                                        name='whatsapp'
                                        size={27}
                                        color='#0aa116'
                                        style={{ marginTop: 20 }}
                                    /> */}
                            </View>
                            <Image
                                    source={{ uri: 'http://dilkhushpvtltd.com/bizarmy/public/' + item.image }}
                                    style={{
                                        alignSelf: "center",
                                        marginTop:hp('5%'),
                                        height: hp('30%'), width: wp("90%"),

                                    }}></Image>
                           
                            <Text style={{ fontSize: 12, fontWeight: '100', marginTop: hp('5%'),width:wp('90%') }}>{item.detail} </Text>

                        </View>

                 
            {/* </View> */}
            </ScrollView>

        </View>
    );
};

export default EventDetail;

