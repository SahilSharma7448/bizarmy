import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image, ScrollView,BackHandler, FlatList } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import { Input, CheckBox, BottomSheet, ListItem } from 'react-native-elements';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import * as Utility from "../../../Utility"

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import { TouchableOpacity } from 'react-native-gesture-handler';
import { PostPDRecords, PdListRecordApi } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
import FontAwesome from "react-native-vector-icons/FontAwesome"

const SuccessDetail = ({route, navigation,handlePress }) => {
    const {item}=route.params
 const EventData=[
     {id:1},
     {id:2},

 ]
    useEffect(() => {
     
        BackHandler.addEventListener('hardwareBackPress', goToBack);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', goToBack);
        };
      }, []);

      const goToBack=()=>{
          navigation.goBack()
          return true
      }

    return (

        <View style={{}}>
            <MainHeader navigation={navigation} showBack={true} title={'Success Story'}></MainHeader>
            {/* <View style={{ marginBottom: 200 }}> */}
              <ScrollView>
                        <View style={{
                          
                            alignSelf: "center", width: wp("90%"),
                            marginTop: 30,
                            marginBottom: 10,
                         
                        }}>
                              
                                    <Text style={{ fontSize: 15, fontWeight: 'bold',textAlign:"center", lineHeight: 24 }}>{item.title}</Text>
                                    <Text style={{ fontSize: 12,  lineHeight: 24 ,textAlign:"center"}}>From {item.name}</Text>
                                  

                               
                          
                           
                            <Text style={{ fontSize: 12, fontWeight: '100', marginTop: hp('5%'),width:wp('90%'), }}> {item.story} </Text>

                        </View>

                 
            {/* </View> */}
            </ScrollView>

        </View>
    );
};

export default SuccessDetail;

