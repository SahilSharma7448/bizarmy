import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground,BackHandler, Image, ScrollView, FlatList,TouchableOpacity } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import { useDispatch, useSelector } from "react-redux"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import FontAwesome from "react-native-vector-icons/FontAwesome"
import Ionicons from "react-native-vector-icons/Ionicons"
import { Get_Story } from "../../../Redux/Action"
import * as Utility from "../../../Utility"

var userId=''
const SuccessList = ({ navigation,handlePress }) => {
    const dispatch=useDispatch()
    const[connectData,setConnectData]=useState([])
    // useEffect(() => {

    //     getDropDownData()
    // }, []);

    useEffect(() => {
        getDropDownData()


        const unsubscribe = navigation.addListener('focus', () => {
            getDropDownData()

        });

        return () => {
            unsubscribe;
        };
    }, [navigation]);
    const getDropDownData=async()=>{
        let res=await dispatch(Get_Story())
        if(res.status==true){
            let id=await Utility.getFromLocalStorge('userId')
            userId=id
            setConnectData(res.data)
        }
    }
    useEffect(() => {
       
        BackHandler.addEventListener('hardwareBackPress', goToBack);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', goToBack);
        };
      }, []);

      const goToBack=()=>{
          navigation.goBack()
          return true
      }
    
    return (

        <View>
            <MainHeader navigation={navigation} showBack={true} title={'Success Stories'} showPlus={true}  pressPlus={()=>navigation.navigate('CreateStory')}></MainHeader>
          
                <ScrollView style={{marginBottom:50}}>
<View>
<FlatList
                    data={connectData}
                    renderItem={({ item }) =>
                  
                            <View style={{


                                backgroundColor: "white",
                                shadowColor: "green",
                                shadowOffset: {
                                    width: 0,
                                    height: 4,
                                },
                                shadowOpacity: 0.20,
                                shadowRadius: 4.30,

                                elevation: 3,
                                alignSelf: "center", width: wp("90%"),
                                // padding: 10,
                                marginTop: 20,
                                marginBottom: 20,
                                borderColor:item.user_id==userId? "#ffd633":'#0aa116',
                                borderWidth: 2,
                                borderRadius: 10,
                                paddingBottom: 10,
                            }}>
                                <View style={{ flexDirection: 'row', justifyContent: "space-between",marginTop:15 }}>
                                    <View style={{ height: hp('7%'), width: wp('60%'), backgroundColor:item.user_id==userId? "#ffd633":'#0aa116', paddingLeft:10, justifyContent: "center" }}>
                                        <Text style={{ fontSize: 12, color: "black" ,fontWeight:"bold"}}>{item.title}</Text>
                                        <Text style={{ fontSize: 9, color: "black" }}>From  {item.name}</Text>

                                    </View>
                                    { item.user_id==userId?
                                     <TouchableOpacity onPress={()=>navigation.navigate('UpdateStory',{
                                         item:item
                                     })}>
                                    <FontAwesome
                                        name='edit'
                                        size={20}
                                        color='grey'
                                        style={{marginRight:20,marginTop:10 }}
                                    />
                                    </TouchableOpacity>:null}
                                </View>

                                <View style={{ flexDirection: "row", marginTop: 20 }}>
                                    <View style={{ flexDirection: "column", width: wp('75%'), paddingLeft: 10 }}>
                            <Text style={{ fontSize: 12, fontWeight: '100', color:"black" }}> {item.story} </Text>
                                     
                                    </View>
                                   
                                    <View>
                                    {/* <TouchableOpacity >
                                    <FontAwesome
                                        name='whatsapp'
                                        size={20}
                                        color='#0aa116'
                                        style={{marginLeft:20 }}
                                    />
                                    </TouchableOpacity> */}
                                  
                                </View>
                               
                                </View>
                                <TouchableOpacity onPress={()=>navigation.navigate('SuccessDetail',{
                                    item:item
                                })}>
                                <View style={{ height: 30, width: wp('24%'), borderRadius: 30, alignSelf: "flex-end", alignItems: 'center', justifyContent: "center",marginRight:20,marginTop:10,backgroundColor:'#ffd633' }}>
                    <Text style={{ fontSize: 10, color: "black",fontWeight:"bold" }}>Read More</Text>

                </View>
                </TouchableOpacity>
                            </View>
                    }
                    />
                    </View>
                    
                  
                    
                 
                </ScrollView>


        </View>
    );
};

export default SuccessList;

