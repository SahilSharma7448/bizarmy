import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text,BackHandler, ImageBackground, Image, ScrollView,ToastAndroid, Button, TextInput } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import styles from "./style"
import { Input, CheckBox, } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Picker } from '@react-native-community/picker';
import { GetBridageCategory,add_story } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
import * as Utility from "../../../Utility"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import { TouchableOpacity } from 'react-native-gesture-handler';
const CreateStory = ({ navigation }) => {
    const dispatch = useDispatch()
    const [bussinessCategory, setBusniessCategory] = useState([])
    const [myBridage, setBridage] = useState([])
    const [selectedValue, setSelectedValue] = useState("");
    const [givenName, setGivenName] = useState('')

    const [loading, setLoading] = useState(false)

 const [title,setTitle]=useState('')
 const [member,setMember]=useState('')
 const [brigade,SetBrigade]=useState('')
 const [story,setStory]=useState('')
    useEffect(() => {

        getDropDownData()
    }, []);
    const getDropDownData = async () => {

     
        let response = await dispatch(GetBridageCategory())
        await setBridage(response.data)
    }
  
    

    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);

    const submit=async()=>{
if(await Utility.isFieldEmpty(title,member,story)){
    ToastAndroid.show("Please fill valid email", ToastAndroid.showWithGravityAndOffset);

}
else {
    setLoading(true)
    let res=await dispatch(add_story(title,story))
    if(res.status==true){
        setLoading(false)
     ToastAndroid.show(res.data, ToastAndroid.showWithGravityAndOffset);
 navigation.navigate('SuccessList')
    }
    else{
     setLoading(false)
     ToastAndroid.show("SomeWent Wrong.", ToastAndroid.showWithGravityAndOffset);
 
 
    }
}
    }
    return (

        <View>
            <Loader isLoading={loading}></Loader>

            <MainHeader navigation={navigation} showBack={true} title={'Add Stories'}></MainHeader>
            <ScrollView>
                <View style={{ marginTop: 30 }}>



                 
                  



                    <Text style={styles.inputTitle}>Title</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            placeholder={"Enter Title "}
onChangeText={(text)=>setTitle(text)}
                        />
                    </View>




                    <Text style={styles.inputTitle}>Tag Member</Text>
                    <View style={styles.inputMainView}>

                        <Input
                            placeholder={"Member Name "}
onChangeText={(text)=>setMember(text)}

                        />
                    </View>

                    {/* <Text style={styles.inputTitle}>Select Bridage </Text>
                    




                    <View style={styles.inputMainView}>

<Input
    placeholder={"Select Bridage "}
onChangeText={(text)=>setBridage(text)}

/>
</View> */}

              
                    <Text style={styles.inputTitle}>Story</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            style={styles.sqaureBorderInput}
onChangeText={(text)=>setStory(text)}

                        />
                    </View>
                    <TouchableOpacity onPress={()=>submit()}>
                        <View style={styles.submitBtn}>
                            <Text style={{ color: "white" }}>Submit</Text>

                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
          
        </View>
    );
};

export default CreateStory;

