import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, TouchableOpacity,BackHandler, Image, ScrollView,ToastAndroid, FlatList } from 'react-native';
import MainHeader from '../../Components/MainHeader';
import FontAwesome from "react-native-vector-icons/FontAwesome"
import Loader from "../../Constants/Loader"

import { add_Attendence_api, AttendanceList } from "../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../Utility"
import Modal from 'react-native-modal';
import { Input, CheckBox, } from 'react-native-elements';
import SyncStorage from 'sync-storage';
var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = mm + '/' + dd + '/' + yyyy;
const Attendence = ({ navigation }) => {
    const dispatch = useDispatch()
    const [attendanceData, setAttendance] = useState([])
    const [showModal,setShowModal]=useState(false)
    const [loading, setLoading] = useState(false)

    const [currentStatus,setCurrentStatus]=useState('All')
    const listData = [
        { id: 1 },
        { id: 2 },
        { id: 3 },
        { id: 4 },

    ]
    useEffect(() => {
        myDashBoardData()


        const unsubscribe = navigation.addListener('focus', () => {
            myDashBoardData()

        });

        return () => {
            unsubscribe;
        };
    }, [navigation]);
    const myDashBoardData = async () => {
        let res = await dispatch(AttendanceList())
        if (res.status == false) {
            // setNoRecord(true)
        }
        else {
            // setMyData(res)
            setAttendance(res.data)
        }
    }
    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);
    const modalOff=async()=>{
        console.log("check")
        setShowModal(false)
    }
    const changeStatus=async(value)=>{
        setCurrentStatus(value)
        setShowModal(false)

      }
      const num=()=>{
        // let userName=await Utility.getFromLocalStorge('name')
        const result = SyncStorage.get('user');
    console.log(result,"????????????????????????////"); // 'bar'
    console.log(SyncStorage.getAllKeys())
    
    return   <Text style={{ fontSize: 14,  fontWeight: "700",textAlign:"center" }}>  Name of Member: {result}</Text>
    }


    const addAttendence=async()=>{
        await setShowModal(false)
        setLoading(true)
let res=await dispatch(add_Attendence_api())
if (res.status == true) {
    setLoading(false)
    // Alert.alert(
    //     '',
    //     res.msg,
    //     [{ text: 'Ok', onPress: () => navigation.goBack() }],
    //     {
    //         cancelable: false,
    //     },
    // );
    // return true;
    ToastAndroid.show(res.msg, ToastAndroid.showWithGravityAndOffset);
  await  myDashBoardData()

    return
}
else {
    setLoading(false)
    ToastAndroid.show(res.msg, ToastAndroid.showWithGravityAndOffset);

    return 
}
    }
    return (

        <View>
            <Loader isLoading={loading}></Loader>

            <MainHeader navigation={navigation} showBack={true} title={'Attendance'} showPlus={true} pressPlus={()=>setShowModal(true)}></MainHeader>
            <View style={{ marginBottom: 100 }}>
                {currentStatus=='All'?
                <View style={{}}>
                <FlatList
                    data={attendanceData}
                    renderItem={({ item }) =>
                    <View>
                        <View style={{
                            width: wp('90%'), alignSelf: "center", borderWidth: 1, borderColor: item.is_present == 1 ? "#0aa116" : 'red',
                            justifyContent: "space-between",
                            padding: 10, flexDirection: "row", marginTop: 40,
                            borderRadius: 10
                        }}>
                            <View>
                                <Text style={{ lineHeight: 40, fontSize: 12 }}>Metting Date</Text>
                                <Text style={{ fontSize: 17 }}>{item.meeting_date}</Text>

                            </View>
                            <View>
                                <Text style={{ lineHeight: 40, fontSize: 12 }}>Attendance</Text>
                                <Text style={{ fontSize: 17 }}>{item.is_present == 1 ? "PRESENT" : 'ABSENT'}</Text>

                            </View>
                        </View>
                        </View>
                    }
                />
                </View>:
 <View style={{}}>
 <FlatList
     data={attendanceData}
     renderItem={({ item }) =>
     <View>
         {item.is_present==currentStatus?
         <View style={{
             width: wp('90%'), alignSelf: "center", borderWidth: 1, borderColor: item.is_present == 1 ? "#0aa116" : 'red',
             justifyContent: "space-between",
             padding: 10, flexDirection: "row", marginTop: 40,
             borderRadius: 10
         }}>
             <View>
                 <Text style={{ lineHeight: 40, fontSize: 12 }}>Metting Date</Text>
                 <Text style={{ fontSize: 17 }}>{item.meeting_date}</Text>

             </View>
             <View>
                 <Text style={{ lineHeight: 40, fontSize: 12 }}>Attendance</Text>
                 <Text style={{ fontSize: 17 }}>{item.is_present == 1 ? "PRESENT" : 'ABSENT'}</Text>

             </View>
         </View>:null}
         </View>
     }
 />
 </View>

                }
                {/* <View style={{
                    width: wp('90%'), alignSelf: "center", borderWidth: 1, borderColor: "red",
                    justifyContent: "space-between",
                    padding: 10, flexDirection: "row", marginTop: 40,
                    borderRadius: 10
                }}>
                    <View>
                        <Text style={{ lineHeight: 40, fontSize: 12 }}>Metting Date</Text>
                        <Text style={{ fontSize: 17 }}>23 March 2021</Text>

                    </View>
                    <View>
                        <Text style={{ lineHeight: 40, fontSize: 12 }}>Attendence</Text>
                        <Text style={{ fontSize: 17 }}>ABSENT</Text>

                    </View>
                </View>

 */}



                {/* /////////////////// education //////////////////// */}

            </View>


<Modal

backdropOpacity={0.5}
backdropColor="black"
hasBackdrop={true}
onBackdropPress={() => modalOff()}
onRequestClose={() => modalOff()}
isVisible={showModal}>
<View style={{ width: wp("80%"), height: hp("30%"), backgroundColor: "white", alignSelf: "center", justifyContent: "center", borderRadius: 10 }}>
{num()}
<Text style={{ fontSize: 14,  fontWeight: "700",textAlign:"center",marginTop:10 }}>  Date: {today}</Text>
<TouchableOpacity onPress={()=>addAttendence()}>
<View style={{marginTop:30,
                                        height: 30, width: wp('40%'),
                                        backgroundColor: "#0aa116",
                                        borderRadius: 30, alignSelf: 'center', alignItems: 'center', justifyContent: "center",
                                    }}>
                                        <Text style={{ fontSize: 10, color: "white", }} >Mark Attendence</Text>

                                    </View>
                                    </TouchableOpacity>
</View>

</Modal>
        </View>
    );
};

export default Attendence;

