import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground,BackHandler, Image, ScrollView, FlatList } from 'react-native';
import MainHeader from '../../Components/MainHeader';
import { Commite_Member_Api } from "../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../Utility"
const CommiteMember = ({ navigation }) => {
    const dispatch = useDispatch()
    const listData = [
        { name: 'rajat' },
        { name: 'rajat' },
        { name: 'rajat' },
        { name: 'rajat' },
        // { name: 'rajat' },
        // { name: 'rajat' },

    ]

const [member,setMember]=useState([])

    useEffect(() => {

        getDropDownData()
    }, []);
    const getDropDownData = async () => {


        let response = await dispatch(Commite_Member_Api())
        // await setMember(response.data)
        if(response.status==true){
            setMember(response.data)
        }
    }
    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);
    return (

        <View>
            <MainHeader navigation={navigation} showBack={true} title={'Committee Member'}></MainHeader>
            <ScrollView>
                <View style={{ marginBottom: 100 }}>
                    {/* /////////////////// education //////////////////// */}
                    <FlatList
                            data={member}
                            renderItem={({ item }) =>
                    <View style={{ width: wp("90%"), alignSelf: "center", marginTop: 20, borderColor: "#80ccff", borderWidth: 1 }}>


                        <View style={{ height: hp('22%'), backgroundColor: "#80ccff", flexDirection: "row" }}>
                            <View style={{ width: wp('54%'), alignItems: "center", justifyContent: "center", marginLeft: 10 }}>
                                <Text style={{ fontSize: 30, color: "white", fontWeight: '900' }}>{item.name}</Text>

                            </View>
                            <View style={{ borderLeftColor: "white", borderLeftWidth: 1, height: hp('14%'), justifyContent: "center", alignSelf: "center" }}></View>
                            <View style={{ width: wp('34%'), alignItems: "center" }}>
                                <Image

                                    source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRLNbGpMAAWHzxJBqXLuGRNy_sFJ9tjtzSxjQ&usqp=CAU' }}

                                    style={{ height: hp('9%'), width: wp('17%'), borderRadius: 100, borderWidth: 2, borderColor: "white", marginTop: 10 }} resizeMode={'cover'}></Image>
                                <Text style={{ fontSize: 14, color: "white", fontWeight: '900', textAlign: "center", marginTop: 10 }}>{item.coordinator_name}</Text>
                                <Text style={{ fontSize: 10, color: "white", fontWeight: '900', textAlign: "center", }}>(Cordinator)</Text>

                            </View>

                        </View>


                        <FlatList
                            data={item.tag_person_data}
                            renderItem={({ item }) =>
                                <View style={{ marginLeft: wp('17%'), marginBottom: 20 }}>
                                    <Image

                                        source={{ uri: 'http://dilkhushpvtltd.com/bizarmy/public/' + item.profile_image }}

                                        style={{ height: hp('9%'), width: wp('17%'), borderRadius: 100, borderWidth: 2, borderColor: "white", marginTop: 10 }} resizeMode={'cover'}></Image>
                                    <Text style={{ fontSize: 14, color: "black", fontWeight: '900', textAlign: "center", }}>{item.name}</Text>
                                    <Text style={{ fontSize: 10, color: "black", fontWeight: '900', textAlign: "center", marginTop: 7 }}>(brigade{item.brigade})</Text>
                                </View>
                            }
                            numColumns={2}
                        // style={{ alignSelf: "center", }}

                        />





                    </View>
                            }
                            />
                   
                </View>
            </ScrollView>
        </View>
    );
};

export default CommiteMember;

