import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../../Utility/index';

const styles = StyleSheet.create({
    addPhotoBtn: {
        height: 30,
        borderRadius: 30,
        marginBottom: 30,
        width: wp("30%"), borderColor: "#0aa116", borderWidth: 1, alignSelf: "center", alignItems: "center", justifyContent: "center", marginTop: 20
    },
    inputTitle: { fontSize: 10, left: 4, color: "grey", width: wp('80%'), top: 10, alignSelf: 'center', },
    inputMainView: { width: wp('80%'), alignSelf: 'center', },

});

export default styles;