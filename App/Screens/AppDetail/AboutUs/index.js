import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image,BackHandler } from 'react-native';
import MainHeader from '../../../Components/MainHeader';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
const AboutUs = ({ navigation }) => {
    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);
    return (

        <View>
            <MainHeader navigation={navigation} showBack={true} title={'About Us   '}></MainHeader>
            <View style={{ padding: 20 }}>
                <Text style={{ fontSize: 20, fontWeight: "bold", marginTop: 20 }}>What is BizArmy?</Text>
                <Text style={{ fontSize: 14, marginTop: 10 }}>BizArmy is the infromal business sharing group amoung member who are diversified fileds are expert in thier respective business.BizArmy is a platform where member from diffrent  sphers and stream come togther and generate incremental business.</Text>
                <Text style={{ fontSize: 20, fontWeight: "bold", marginTop: 20 }}>Feature of BizArmy</Text>
                <Text style={{ fontSize: 14, marginTop: 20 }}>1). Efficient sourse to generate incremental business at almost no cost</Text>

                <Text style={{ fontSize: 14, marginTop: 20 }}>2). Get inhance social visibility</Text>

                <Text style={{ fontSize: 14, marginTop: 20 }}>3). Relationship Building</Text>

                <Text style={{ fontSize: 14, marginTop: 20 }}>4). Complete trasparent operation</Text>
                <Text style={{ fontSize: 14, marginTop: 20 }}>5). Increase your chances of your collaborations through reliable contacts</Text>

            </View>
        </View>
    );
};

export default AboutUs;

