import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground,
    BackHandler,
    Alert,
    Image, ScrollView, TouchableOpacity } from 'react-native';
import MainHeader from '../../Components/SubHeader';
import DashBoard from "../TabScreens/DashBoard"
import RecommendationList from "../TabScreens/RecommendationList"
import Entypo from "react-native-vector-icons/Entypo"
import Ionicons from "react-native-vector-icons/Ionicons"
import Invoices from "../TabScreens/Invoices"
import Business from "../TabScreens/Bussiness"
import PdList from "../TabScreens/PdList"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../Utility"
import Modal from 'react-native-modal';
import {useDispatch} from "react-redux"
import {Header_Status,Current_Recommendation_Status,ModalStatus,ModalStatusBusiness,BackStatus} from "../../Redux/Action"
const Home = ({ navigation }) => {
    const dispatch=useDispatch()
    const [activeTab, setActiveTab] = useState('')
    const [showModaL, setShowModal] = useState(false)
    const[current,setCurrent]=useState('ss')
    const goToNavigate = (value) => {
        setShowModal(false)
        navigation.navigate(value)
    }
   
  
            const changeTab=async(value)=>{
                await dispatch(ModalStatus(false))
await dispatch(ModalStatusBusiness(false))
await dispatch(Header_Status(false))
await dispatch(BackStatus(false))

await dispatch(Current_Recommendation_Status('All'))
setActiveTab(value)
            }
            const backPress=()=>{
                console.log("chgeck;okjlj")
                setActiveTab('')
                return true;
            }
            const openModal=async()=>{
                console.log("check")
                setCurrent('checkhhhhh')
await dispatch(ModalStatus(true))
await dispatch(ModalStatusBusiness(true))


                // await dispatch(Current_Recommendation_Status('lalhdkjsbd'))
              }
            
    return (

        <View style={{ flex: 1 }}>
            {activeTab !== '' ?
                <MainHeader navigation={navigation} showBack={true} title={activeTab} click={() => setActiveTab('')} showFilter={true} filterPress={()=>openModal()}></MainHeader>
                : null}
            <View style={{ flex: 1, marginBottom: 50 }}>
                {activeTab == '' ?
                    <DashBoard navigation={navigation}></DashBoard> : null}
                {activeTab == 'Invoices' ?
                    <Invoices navigation={navigation} handlePress={()=>backPress()}></Invoices> : null
                }
                {activeTab == 'Business Booked' ?
                    <Business navigation={navigation}  handlePress={()=>backPress()} ></Business> : null
                }
                {activeTab == 'Personal Disscusion' ?
                    <PdList navigation={navigation}  handlePress={()=>backPress()}></PdList> : null
                }
                {activeTab == 'Recommendations' ?
                    <RecommendationList navigation={navigation}  handlePress={()=>backPress()} data={current} ></RecommendationList> : null
                }
            </View>
            <View style={{
                position: "absolute", bottom: 0, height: 49,

                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 6,
                },
                shadowOpacity: 0.39,
                shadowRadius: 8.30,

                elevation: 13,
                backgroundColor: "white",

                width: wp('100%'),

            }}>
                {/* <Image source={require("../../Assets/Icons/dashboard.png")} style={{ height: 20, width: 20, }} resizeMode={"contain"}></Image> */}
                <View style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                    alignItems: "center",
                    bottom: 7

                }}>
                    <TouchableOpacity onPress={() => changeTab('Personal Disscusion')}>
                        <Image source={require("../../Assets/Icons/PD.png")} style={{ height: 20, width: 20, right: 10 }} resizeMode={"contain"}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => changeTab('Recommendations')}>
                        <Entypo
                            name='swap'
                            size={20}
                            color='#0aa116'
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setShowModal(true)}>
                        <Ionicons
                            name='add-circle-outline'
                            size={40}
                            color={'#0aa116'}
                            style={{ top: 4 }}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => changeTab('Business Booked')}>
                        <Image source={require("../../Assets/Icons/BussinessLogo.png")} style={{ height: 30, width: 30, }} resizeMode={"contain"}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => changeTab('Invoices')}>
                        <Image source={require("../../Assets/Icons/Invoices.png")} style={{ height: 20, width: 20, }} resizeMode={"contain"}></Image>
                    </TouchableOpacity>
                </View>
                <View style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                    alignItems: "center",
                    bottom: 14,
                }}>
                    <Text style={{ fontSize: 10, color: activeTab == 'Personal Disscusion' ? '#0aa116' : 'black' }} onPress={() => changeTab('Personal Disscusion')}>PD'<Text style={{ fontSize: 8, color: activeTab == 'Personal Disscusion' ? '#0aa116' : 'black' }}>s</Text></Text>
                    <Text style={{ fontSize: 10, color: activeTab == 'Recommendations' ? '#0aa116' : 'black' }} onPress={() => changeTab('Recommendations')}>Recommendations</Text>
                    <Text style={{ fontSize: 10 }}></Text>
                    <Text style={{ fontSize: 10, color: activeTab == 'Business Booked' ? '#0aa116' : 'black' }} onPress={() => changeTab('Business Booked')}>Business Booked</Text>
                    <Text style={{ fontSize: 10, color: activeTab == 'Invoices' ? '#0aa116' : 'black' }} onPress={() => changeTab('Invoices')}>Invoices</Text>

                </View>
            </View>
            <Modal

                backdropOpacity={0.5}
                backdropColor="black"
                hasBackdrop={true}
                onBackdropPress={() => setShowModal(false)}
                onRequestClose={() => setShowModal(false)}
                isVisible={showModaL}>
                <View style={{ width: wp("80%"), height: hp("40%"), backgroundColor: "white", alignSelf: "center", justifyContent: "center", borderRadius: 10 }}>
                    <Text style={{ fontSize: 24, textAlign: "center", bottom: 20 }}>Create</Text>

                    <View style={{ flexDirection: "row", justifyContent: "space-around", alignItems: "center" }}>
                        <TouchableOpacity onPress={() => goToNavigate('Add')}>
                            <View style={{ backgroundColor: "#F2F2F2", padding: 10, borderRadius: 10, height: 100, width: wp('28%') }}>
                                <Image source={require("../../Assets/Icons/BusinessGiven.png")} style={{ height: 49, width: 49, alignSelf: "center" }} resizeMode={"contain"}></Image>

                                <Text style={{ marginTop: 10, fontSize: 10 }}>Recommendation</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => goToNavigate('PD')}>

                            <View style={{ backgroundColor: "#F2F2F2", padding: 10, borderRadius: 10, height: 100, width: wp('28%'), alignItems: "center" }}>


                                <Image source={require("../../Assets/Icons/PDRed.png")} style={{ height: 49, width: 49 }} resizeMode={"contain"}></Image>
                                <Text style={{ textAlign: "center", marginTop: 10 }}>PD</Text>

                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection:"row",justifyContent:"space-around",alignItems:"center",marginTop:15}}>
                       {/* <TouchableOpacity onPress={()=>navigation.navigate('CreateStrong')}>
                        <View style={{height:40,backgroundColor:'#F2F2F2',width:wp('28%'),alignItems:"center",justifyContent:"center",borderRadius:7}}>
                        <Text style={{textAlign:"center",fontSize:10,}}>Strong Connect</Text>
                    </View>
                    </TouchableOpacity> */}

<TouchableOpacity >

<View style={{ backgroundColor: "#F2F2F2", padding: 10, borderRadius: 10, height: 100, width: wp('28%'), alignItems: "center" }}>


    <Image source={require("../../Assets/Icons/Event.png")} style={{ height: 49, width: 49 }} resizeMode={"contain"}></Image>
    <Text style={{ textAlign: "center", marginTop: 10 }}>Events</Text>

</View>
</TouchableOpacity>
                    {/* <TouchableOpacity onPress={()=>navigation.navigate('CreateVistor')}>
                    <View style={{height:40,backgroundColor:'#F2F2F2',width:wp('28%'),alignItems:"center",justifyContent:"center",borderRadius:7}}>
                        <Text style={{textAlign:"center",fontSize:10,}}>Visitor</Text>
                    </View>
                    </TouchableOpacity> */}

<TouchableOpacity onPress={()=>navigation.navigate('CreateVistor')}>

<View style={{ backgroundColor: "#F2F2F2", padding: 10, borderRadius: 10, height: 100, width: wp('28%'), alignItems: "center" }}>


    <Image source={require("../../Assets/Icons/Visitor.png")} style={{ height: 49, width: 49 }} resizeMode={"contain"}></Image>
    <Text style={{ textAlign: "center", marginTop: 10 }}>Visitor</Text>

</View>
</TouchableOpacity>
                    </View>
                  
                </View>
            </Modal>
        </View>
    );
};

export default Home;

