import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground,
    Linking,
    BackHandler,
    StyleSheet,
    RefreshControl,
    ToastAndroid,
    Image, ScrollView, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo'

import MainHeader from '../../Components/MainHeader';
import FontAwesome from "react-native-vector-icons/FontAwesome"
import Ionicons from "react-native-vector-icons/Ionicons"
import { GetBusinessCategory, GetMyBridage, RecommendationSave,Search_Member_api } from "../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Clipboard from '@react-native-clipboard/clipboard';
import Loader from "../../Constants/Loader"

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../Utility"
import { TouchableOpacity } from 'react-native-gesture-handler';
import { SearchBar } from 'react-native-elements';
const MemberDetail = ({ navigation }) => {
    const dispatch = useDispatch()
    const [member, setMember] = useState([])
    const [showHeader,setShowHeader]=useState(true)
    const[searchName,setSearchName]=useState('')
    const [loading, setLoading] = useState(false)
const [refreshing,setrefreshing]=useState(false)
    const listData = [
        { id: 1 },
        { id: 2 },
        { id: 3 },
        { id: 4 },

    ]
    useEffect(() => {

        getDropDownData()
    }, []);
    const getDropDownData = async () => {


        let response = await dispatch(GetMyBridage())
        await setMember(response.data)
    }

    const refreshData = async () => {

setrefreshing(true)
      let response = await dispatch(GetMyBridage())
      await setMember(response.data)
      setShowHeader(true)
setrefreshing(false)

  }
    const openWhatsApp = (mobile) => {
        let msg = 'hello';
        if (mobile) {
          if (msg) {
            let url =
              "whatsapp://send?text=" +
            '' +
              "&phone=91" +
              mobile;
            Linking.openURL(url)
              .then(data => {
                console.log("WhatsApp Opened successfully " );
              })
              .catch(() => {
                alert("Make sure WhatsApp installed on your device");
              });
          } else {
            alert("Please enter message to send");
          }
        } else {
          alert("Please enter mobile no");
        }
      };
      const handlePress=()=>{
          navigation.goBack()
          return true
      }
      useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handlePress);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', handlePress);
        };
      }, []);


      const searchApi=async()=>{
        setLoading(true)
   let res=  await   dispatch(Search_Member_api(searchName))
   if(res.status==false){
    setLoading(false)

    ToastAndroid.show('No Record Found', ToastAndroid.showWithGravityAndOffset);
     
   }

   else{
  await  setMember(res.data)
  setShowHeader(true)
  setLoading(false)

   }
      }
    return (

        <View>
          <Loader isLoading={loading}></Loader>
         
            <View>
         {showHeader==true?
           
                <View style={{

                    flexDirection: 'row',
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 6,
                    },
                    shadowOpacity: 0.39,
                    shadowRadius: 8.30,

                    elevation: 13,
                    backgroundColor: "white",
                    justifyContent: "space-between", height: 49, alignItems: "center", paddingLeft: 14, paddingRight: 10
                }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon
                            style={styles.icon}
                            name="arrow-long-left"
                            size={24}
                            color="black"
                        />
                    </TouchableOpacity>
                    <Text style={{ fontSize: 17, color: "black", }}>{'Member Details'}</Text>
  <TouchableOpacity onPress={()=>setShowHeader(false)}>
  <Ionicons
  style={styles.icon,[{marginRight:10}]}

  name="search"
  size={24}
  color="black"
/>
</TouchableOpacity>
            </View>:
             <SearchBar
             placeholder="Enter Member Name"
             containerStyle={{backgroundColor:"white",borderColor:"white",borderWidth:1,}}
             inputStyle={{backgroundColor:"white"}}
             inputContainerStyle={{backgroundColor:"white"}}
             onChangeText={(text)=>setSearchName(text)}
             value={searchName}
             onBlur={()=>searchApi()}
             searchIcon={
               <TouchableOpacity onPress={()=>searchApi()}>
              <Ionicons
              style={styles.icon,[{marginRight:10}]}
            
              name="search"
              size={17}
              color="black"
            />
            </TouchableOpacity>
             }
           />
            }
        </View>
         
         
         
         
            <View style={{ marginBottom: 100 }}>





                <FlatList
                    data={member}
                    renderItem={({ item }) =>
                        <View style={{ width: wp('90%'), borderRadius: 10, borderColor: "#0aa116", borderWidth: 1, marginTop: 20, alignSelf: "center" }}>

                            <View style={{ width: wp('40%'),padding:5, backgroundColor: "#0aa116", alignSelf: "center", alignItems: "center", justifyContent: "center", marginBottom: 20 }}>
                                <Text style={{ color: "white", fontSize: 17, fontWeight: "bold" }}>{item.category}</Text>
                            </View>
                            <View style={{ flexDirection: "row" }}>
                                <View style={{ width: wp('40%'), alignItems: "center" }}>
                                    <Image

                                        source={{ uri: 'http://dilkhushpvtltd.com/bizarmy/public/' + item.profile_image }}

                                        style={{ height: hp('14%'), width: wp('28%'), borderRadius: 100, borderWidth: 3, borderColor: "#0aa116", marginBottom: 10 }} resizeMode={'cover'}></Image>
                                    <Text style={{ color: "black", fontSize: 20, fontWeight: "bold" }}>{item.name}</Text>
                                    {/* <Text style={{ color: "black", fontSize: 20, fontWeight: "bold" }}>Sharma</Text> */}
                                    <Text style={{ color: "black", fontSize: 14, marginTop: 20, marginBottom: 30 }}>(Bridage {item.brigade})</Text>

                                </View>
                                <View style={{ height: hp('24%'), borderLeftColor: "#0aa116", borderWidth: 1, alignSelf: "center" }}></View>

                                <View style={{ width: wp('34%'), marginLeft: 10, marginTop: 20 }}>
                                    <Text style={{ fontSize: 12 }}>Contact No.</Text>
                                    <Text style={{ fontSize: 14 }}>{item.mobile_no}</Text>
                                    <Text style={{ fontSize: 12, marginTop: 10 }}>Email Id</Text>
                                    <Text style={{ fontSize: 12 }}>{item.email}</Text>
                                    <Text style={{ fontSize: 12, marginTop: 10 }}>Firm Name</Text>
                                    <Text style={{ fontSize: 14 }}>{item.business_name}</Text>
                                    <Text style={{ fontSize: 12, marginTop: 10 }}>Address</Text>
                                    <Text style={{ fontSize: 14 }}>{item.address1}</Text>
                                    <Text style={{ fontSize: 14 }}>{item.address2}</Text>
                                    <Text style={{ fontSize: 14 }}>{item.address3}</Text>

                                </View>
                                <View>
                                    <TouchableOpacity onPress={()=>openWhatsApp(item.mobile_no)}>
                                    <FontAwesome
                                        name='whatsapp'
                                        size={27}
                                        color='#0aa116'
                                        style={{ marginTop: 20 }}
                                    />
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                    onPress={()=> Clipboard.setString(item.email)}
                                    >
                                    <Ionicons
                                        name='copy'
                                        size={27}
                                        color='grey'
                                        style={{ marginTop: 20 }}

                                    />
                                    <Text style={{ fontSize: 12, color: "grey" }}>copy </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <Text style={{width:wp('80%'),bottom:10, alignSelf:"center",textAlign:"center",marginTop:20}}>
                              {item.user_bio}
                            </Text>
                        </View>

                    }
                    refreshControl={
                      <RefreshControl
                        refreshing={refreshing}
                        onRefresh={()=>refreshData()}
                      />
                    }
                />
                {/* /////////////////// education //////////////////// */}

            </View>
        </View>
    );
};

export default MemberDetail;

const styles = StyleSheet.create({
  submitView: {
      borderWidth: 1, backgroundColor: "#0aa116", flexDirection: "row", justifyContent: "space-between",
      marginTop: hp("5%"), width: wp("80%"), height: hp("7%"), borderColor: "#0aa116", alignSelf: "center"
  },
  submitText: { color: "white", fontSize: 17, textAlign: "center", marginTop: hp("1.5%"), marginLeft: wp("4%") },
  iconnn: { alignSelf: "flex-end", marginRight: 10 },
});