import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text,BackHandler, ImageBackground, Image, ScrollView, FlatList } from 'react-native';
import MainHeader from '../../Components/MainHeader';
import { Get_Notification_List } from "../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../Utility"
const Notification = ({ navigation }) => {
    const dispatch = useDispatch()
const [listArray,setListArray]=useState([])
 



    useEffect(() => {

        getDropDownData()
    }, []);
    const getDropDownData = async () => {


        let response = await dispatch(Get_Notification_List())
        // await setMember(response.data)
        if(response.status==true){
            setListArray(response.data)
        }
    }
    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);
    return (

        <View>
            <MainHeader navigation={navigation} showBack={true} title={'Notifications'}></MainHeader>
            <ScrollView>
                <View style={{ marginBottom: 40 }}>
                    <FlatList
                        data={listArray}
                        renderItem={({ item }) =>
                            <View style={{


                                backgroundColor: "white",
                                shadowColor: "green",
                                shadowOffset: {
                                    width: 0,
                                    height: 4,
                                },
                                shadowOpacity: 0.20,
                                shadowRadius: 4.30,

                                elevation: 3,
                                alignSelf: "center", width: wp("90%"),
                                padding: 10,
                                marginTop: 20,
                                marginBottom: 20,
                                borderRadius: 4,
                                paddingBottom: 10,
                            }}>
                                {/* <Text style={{ fontSize: 10, width: wp('59%'), textAlign: 'left', color: "grey" }}>23 March 2021</Text> */}
                                <Text style={{ fontSize: 17, color: "black", marginTop: 10 }}>{item.title}</Text>
                                <Text style={{ fontSize: 13, color: "black", marginTop: 7, lineHeight: 24 }} >{item.description}</Text>



                            </View>
                        }

                    />
                </View>
            </ScrollView>
        </View>
    );
};

export default Notification;

