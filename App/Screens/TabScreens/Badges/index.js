import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image, ScrollView,BackHandler, FlatList } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import { Input, CheckBox, BottomSheet, ListItem } from 'react-native-elements';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import * as Utility from "../../../Utility"
import FontAwesome from "react-native-vector-icons/FontAwesome"

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import { TouchableOpacity } from 'react-native-gesture-handler';
import { PostPDRecords, Badges_api } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
var userId=''

const Badges = ({ navigation,handlePress }) => {
    const dispatch = useDispatch()
  
    const [PDRecord, setPDRecord] = useState([])
   

    useEffect(() => {
        getPdList()


        const unsubscribe = navigation.addListener('focus', () => {
            getPdList()

        });

        return () => {
            unsubscribe;
        };
    }, [navigation]);
    const getPdList = async () => {
        let id=await Utility.getFromLocalStorge('userId')

        userId=id
        let res = await dispatch(Badges_api())
        if (res.status == false) {
            setNoRecord(true)
        }
        else {
            setPDRecord(res.data)
        }
    }
    useEffect(() => {
       
        BackHandler.addEventListener('hardwareBackPress', goToBack);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', goToBack);
        };
   
      }, []);

      const goToBack=()=>{
          navigation.goBack()
          return true
      }

    return (

        <View style={{flex:1,backgroundColor:"white"}}>

            <MainHeader navigation={navigation} showBack={true} title={'Badges'}></MainHeader>
         
            {/* <View style={{ marginBottom: 200 }}> */}
                <FlatList
                    data={PDRecord}
                    renderItem={({ item }) =>
                    <View
                    style={{
                      // height: hp('10%'),
                      // width: wp('90%'),
                      // alignSelf: 'center',
                      // justifyContent: 'space-around',
                      // flexDirection: 'row',
                      // alignItems: 'center',
                      // marginTop:hp('4%')
                      backgroundColor: "white",
                      shadowColor: "#000",
                      shadowOffset: {
                          width: 0,
                          height: 6,
                      },
                      shadowOpacity: 0.39,
                      shadowRadius: 8.30,

                      elevation: 7,
                      flexDirection: "row", alignSelf: "center", width: wp("90%"),
                      padding: 10,
                      marginTop: 10,
                      marginBottom: 10,
                      alignItems:"center",
                    }}>
                    <Image
                      source={{
                        uri:
                        'http://dilkhushpvtltd.com/bizarmy/public/' + item.profile,
                      }}
                      style={{
                        height: hp('7%'),
                        width: wp('15%'),
                        borderRadius: 30,
                       
                      }}
                      resizeMode={'cover'}
                      ></Image>
                    <View style={{width: wp('50%'),alignSelf:"center",alignItems:"center"}}>
                      <Text>
                        {item.name}
                      </Text>
                      <Text>{item.brigade_no}</Text>
                    </View>
                    <Image
                      source={{
                        uri:
                        'http://dilkhushpvtltd.com/bizarmy/public/' + item.brigade_image,
                      }}
                      style={{
                        height: hp('7%'),
                        width: wp('15%'),
                        borderRadius: 30,
                       
                      }}
                      resizeMode={'cover'}
                      ></Image>
                  </View>
                    }
                /> 
            {/* </View> */}


        </View>
    );
};

export default Badges;

