import React, { useEffect, } from 'react';
import { useState,useRef } from 'react';
import { View, Text, ImageBackground, Image, ScrollView,Alert, FlatList, TouchableOpacity,BackHandler } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import MainHeader from '../../../Components/MainHeader';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { BannerApi, TrendingPostApi,Header_Status,BackStatus } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import FastImage from 'react-native-fast-image';
import * as Utility from "../../../Utility/index"
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { TapGestureHandler } from 'react-native-gesture-handler';
import ViewShot from 'react-native-view-shot';
import {captureRef} from 'react-native-view-shot'
import Share from "react-native-share"

var RNFS = require("react-native-fs");
const GuestUser = ({ navigation }) => {
    const dispatch = useDispatch()
    const [bannerUrl, setBannerUrl] = useState([])
    const [trendingPost, setTrendingPost] = useState([])
    const ProductsImageRef = useRef("")
    const [activeSlide, setActiveSlide] = useState(0)
    const viewShot=useRef()
    const viewShotPost=useRef()

    useEffect(() => {
        getBanner()
        getTrendingPost()
    }, []);
    const getBanner = async () => {
        let res = await dispatch(BannerApi())
        if (res.status == true) {
            // res.data.map((item) => {
            //     console.log("checkk item", item)
            //     setBannerUrl(item.banner)
            // })
            console.log("check data banner",res)
            setBannerUrl(res.data)
        }

    }
    const getTrendingPost = async () => {
        let res = await dispatch(TrendingPostApi())
        if (res.status == true) {
            setTrendingPost(res.data)
        }
    }
    const goToReadrMore = (item) => {
        console.log("check read", item)
        navigation.navigate('ReadMore', {
            ReadMoreData: item
        })
    }
    
    const goToMyDashboard=async()=>{

await dispatch(BackStatus(true))
        
       await dispatch(Header_Status(true))
        navigation.navigate('DashBoardDetail')
    }
    function handleBackButtonClick() {
        // navigation.goBack();
        console.log("checkback ")

        Alert.alert(
            '',
            'Are you sure you want to close the app?',
            [
                { text: 'No', onPress: () => { }, style: 'cancel' },
                { text: 'Yes', onPress: () => BackHandler.exitApp() },
            ],
            {
                cancelable: false,
            },
        );
        return true;
        return true;
      }
    
      useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
      }, []);
    return (

        <View>
            {/* <MainHeader navigation={navigation}></MainHeader> */}
            {/* <MainHeader navigation={navigation} showBack={true} title={'Trending Post'}></MainHeader> */}
            <View>
                <View style={{

                    flexDirection: 'row',
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 6,
                    },
                    shadowOpacity: 0.39,
                    shadowRadius: 8.30,
                    width:wp('100%'),

                    elevation: 13,
                    backgroundColor: "white",
                    justifyContent: "space-between", height: 49, alignItems: "center", paddingRight: 10
                }}>
                    

                <View style={{
                    flexDirection: 'row',
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 6,
                    },
                    shadowOpacity: 0.39,
                    shadowRadius: 8.30,

                    elevation: 13,
                    backgroundColor: "white",
                    width:wp('100%'),
                    justifyContent: "space-between", height: 49, alignItems: "center", paddingLeft: 14, paddingRight: 10
                }}>
                    {/* <Text style={{ fontSize: wp('6%'), color: "black", }}>𝐁𝐢𝐳𝐀𝐫𝐦𝐲</Text> */}
                    <Image source={require("../../../Assets/Icons/Biz-Army-LOGO-black.png")} style={{ height: 49, width: 100, }} resizeMode={"contain"}></Image>
                   

</View>
                </View>
        </View>

            <ScrollView>

              


                {/* <Image
                    source={{ uri: 'http://dilkhushpvtltd.com/bizarmy/public/' + bannerUrl }}
                    style={{
                        alignSelf: "center", height: hp('24%'), width: wp("90%"),
                        marginTop: 20,


                    }}
                    resizeMode={'stretch'}
                ></Image> */}
                

{/* <Carousel
                        style={{ backgroundColor: "red" }}

                        containerCustomStyle={{ backgroundColor: "#F2F2F200", alignSelf: "center", }}
                        // layout={'stack'} layoutCardOffset={`18`}
                        loop
                        useScrollView
                        autoplay
                        apparitionDelay={10}
                        autoplayDelay={1000}
                        loopClonesPerSide={100}
                        ref={ProductsImageRef}
                        data={bannerUrl}
                        renderItem={_renderItem}
                        sliderWidth={wp("90%")}
                        itemWidth={wp("90%")}
                        onSnapToItem={(index) => setActiveSlide(index)}
                        
                    /> */}
                    <ViewShot ref={viewShot}
    options={{format: 'jpg', quality: 0.9}}>
    <SwiperFlatList
      autoplay={true}
      autoplayDelay={5}
      autoplayLoop
      index={0}
      data={bannerUrl}
      style={{alignSelf:"center",width:wp('90%')}}
      renderItem={({ item }) => (
<View >


            <FastImage
                source={{ uri:'http://dilkhushpvtltd.com/bizarmy/public/' + item.banner }} style={{ height: hp("30%"), width: wp("90%"),alignSelf:"center" }}
                resizeMode={FastImage.resizeMode.contain}
            />
            </View>
      )}
    />

</ViewShot>      


             
               
               
                <View style={{ flexDirection: "row", alignSelf: "center" }}>
                    <Text style={{ alignSelf: "center", width: wp("80%"), fontSize: 20, marginTop: 20 }}>Trending Post</Text>
                  
                </View>
                <View style={{ marginBottom: 40, marginTop: 10 }}>
                    <FlatList
                        data={trendingPost}
                        renderItem={({ item }) =>
<View>
<ViewShot ref={viewShotPost}
    options={{format: 'jpg', quality: 0.9}}>

                            <View style={{
                                backgroundColor: "white",
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 6,
                                },
                                shadowOpacity: 0.39,
                                shadowRadius: 8.30,

                                elevation: 13,
                                flexDirection: "row", alignSelf: "center", width: wp("90%"),
                                padding: 10,
                                marginTop: 10,
                                marginBottom: 10
                            }}>
                                <Image
                                    source={{ uri: 'http://dilkhushpvtltd.com/bizarmy/public/' + item.image }}
                                    style={{
                                        alignSelf: "center", height: hp('8%'), width: wp("20%"),

                                    }}></Image>
                                <View>
                                    <Text style={{ fontSize: 14, width: wp('59%'), marginLeft: 10 }}>{item.title}</Text>
                                  <View style={{flexDirection:"row"}}>
                                    <Text style={{ fontSize: 14, width: wp('59%'), textAlign: "right", marginTop: 20, color: "#0aa116",right:10 }} onPress={() => goToReadrMore(item)}>READ MORE</Text>

                    </View>
                                </View>
                            </View>
                            </ViewShot>
                            </View>

                        }

                    />




                </View>



            </ScrollView>



        </View>
    );
};

export default GuestUser;
