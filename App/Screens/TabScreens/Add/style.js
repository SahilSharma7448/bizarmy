import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../../../Utility/index';

const styles = StyleSheet.create({

    inputTitle: { fontSize: 10, color: "grey", width: wp('80%'), alignSelf: 'center', },
    inputMainView: { width: wp('80%'), alignSelf: 'center', right: 10 },

    addPhotoImg: {
        alignSelf: "center", height: hp('14%'), width: wp("40%"),
        marginTop: 40

    },
    addPhotoBtn: {
        height: 30,
        borderRadius: 30,
        marginBottom: 30,
        width: wp("40%"), backgroundColor: "#0aa116", alignSelf: "center", alignItems: "center", justifyContent: "center", marginTop: 20
    },
    sqaureBorderInput: {
        borderWidth: 1,
        textAlignVertical: 'top',
        marginTop: 10, height: hp('17%'), borderColor: "grey"
    },
    submitBtn: {
        height: 34,
        borderRadius: 30,
        marginBottom: hp('10%'),
        width: wp("40%"), backgroundColor: "#0aa116", alignSelf: "center", alignItems: "center", justifyContent: "center",
    },


    placeholder: { fontSize: wp('4%'), right: 1 },
    heightin: { height: 49 },
    drop: {
        backgroundColor: '#ffffff00',
        right: wp('2%'),
        borderColor: '#ffffff00',
        borderWidth: 2,
        width: wp('80%'),
        alignSelf: 'center',
    },
    droptext:
        { borderColor: "#ffffff00", alignSelf: 'center', width: "70%", justifyContent: "center", right: wp('18%') }

});

export default styles;