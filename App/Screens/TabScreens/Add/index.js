import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text,BackHandler, ImageBackground, Image, ScrollView,ToastAndroid, Button, TextInput } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import styles from "./style"
import { Input, CheckBox, } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Picker } from '@react-native-community/picker';
import { GetBusinessCategory, GetMyBridage, RecommendationSave } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
import * as Utility from "../../../Utility"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Alert } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from "moment";
const Add = ({ navigation }) => {
    const dispatch = useDispatch()
    const [bussinessCategory, setBusniessCategory] = useState([])
    const [myBridage, setBridage] = useState([])
    const [selectedValue, setSelectedValue] = useState("");
    const [givenName, setGivenName] = useState('')
    const [customerNAme, setCustomerName] = useState('')
    const [customerDetail, setCustomerDetail] = useState('')
    const [aprroxValue, setApproxValue] = useState('')
    const [loading, setLoading] = useState(false)
    const [showDropDown, setShowDropDwon] = useState(false)
    const [dropDownValue, setdropDownValue] = useState('Recommendation status')
    const [openChecked, setOpenChecked] = useState(false)
    const [bookedChecked, setBookedChecked] = useState(false)
    const [canceledChecked, setCancledChecked] = useState(false)
    const [status, setStatus] = useState('')
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [SelectedDate, setSelectedDate] = useState('')
    useEffect(() => {

        getDropDownData()
    }, []);
    const getDropDownData = async () => {

        let res = await dispatch(GetBusinessCategory())
        console.log("chek business response", res)
        await setBusniessCategory(res.data)
        let response = await dispatch(GetMyBridage())
        await setBridage(response.data)
    }
    const saveRecord = async () => {

        if (await Utility.isFieldEmpty(selectedValue && givenName && customerNAme && customerDetail && aprroxValue && status && SelectedDate)){
        ToastAndroid.show("Please fill all fields.", ToastAndroid.showWithGravityAndOffset);
            
        return 
        }
            // Alert.alert('Please fill all fields')
        else {
            setLoading(true)
            let res = await dispatch(RecommendationSave(givenName, selectedValue, customerNAme, aprroxValue, customerDetail, status, SelectedDate))

            if (res.status == true) {
                setLoading(false)
                // Alert.alert(
                //     '',
                //     res.msg,
                //     [{ text: 'Ok', onPress: () => navigation.goBack() }],
                //     {
                //         cancelable: false,
                //     },
                // );
                // return true;
                ToastAndroid.show('Record Added Successfully', ToastAndroid.showWithGravityAndOffset);
                navigation.goBack() 
                return
            }
            else {
                setLoading(false)
                ToastAndroid.show(res.message, ToastAndroid.showWithGravityAndOffset);

                return 
            }
        }
    }
    const dropFun = () => {
        console.log("check data")
        setShowDropDwon(true)
    }
    const checkBoxPress = (value) => {
        if (value == 'open') {
            setOpenChecked(true)
            setBookedChecked(false)
            setCancledChecked(false)
            setStatus('OPEN')
            return
        }
        else if (value == 'book') {
            setBookedChecked(true)
            setOpenChecked(false)
            setCancledChecked(false)

            setStatus('BOOKED')


            return
        }
        else {
            setCancledChecked(true)
            setOpenChecked(false)
            setBookedChecked(false)
            setStatus('CANCELLED')


        }

    }

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
        let dates = moment(selectedDate).format("YYYY-MM-DD")
        console.log("check selected date", dates)
        setSelectedDate(dates)

    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };
    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);
    return (

        <View>
            <Loader isLoading={loading}></Loader>

            <MainHeader navigation={navigation} showBack={true} title={'Record Recommendation'}></MainHeader>
            <ScrollView>
                <View style={{ marginTop: 30 }}>



                    <Text style={styles.inputTitle}>Given To</Text>
                    <View style={{ flex: 1 }}>
                        <Picker
                            selectedValue={givenName}
                            mode={"dropdown"}
                            style={{ height: 50, width: wp('80%'), alignSelf: "center", marginRight: wp('4%') }}
                            onValueChange={(itemValue, itemIndex) => setGivenName(itemValue)}
                        >
                            <Picker.Item label="Select Given to" value="" />

                            {myBridage.map((item) => (
                                <Picker.Item label={item.name} value={item.id} />

                            ))}
                        </Picker>
                    </View>

                    <View style={{ borderBottomColor: "#999966", backgroundColor: "#dfdfdf", borderBottomWidth: 1, width: "74%", marginBottom: 20, alignSelf: "center", marginRight: 20 }}>

                    </View>

                    <Text style={styles.inputTitle}>Business Category </Text>


                    <View style={{ flex: 1 }}>
                        <Picker
                            selectedValue={selectedValue}
                            mode={"dropdown"}
                            style={{ height: 50, width: wp('80%'), alignSelf: "center", marginRight: wp('4%') }}
                            onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                        >
                            <Picker.Item label="Select categroy" value="" />

                            {bussinessCategory.map((item) => (
                                <Picker.Item label={item.category_name} value={item.id} />

                            ))}
                        </Picker>
                    </View>

                    <View style={{ borderBottomColor: "#999966", backgroundColor: "#dfdfdf", borderBottomWidth: 1, width: "74%", marginBottom: 20, alignSelf: "center", marginRight: 20 }}>

                    </View>



                    <Text style={styles.inputTitle}>Customer Name</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            placeholder={"Enter Customer Name"}

                            onChangeText={(text) => setCustomerName(text)}
                        />
                    </View>




                    <Text style={styles.inputTitle}>Approx Value</Text>
                    <View style={styles.inputMainView}>

                        <Input
                            keyboardType={"number-pad"}
                            placeholder={"Enter Approx Value"}
                            onChangeText={(text) => setApproxValue(text)}
                        />
                    </View>






                    <Text style={styles.inputTitle}>Date</Text>
                    <TouchableOpacity onPress={showDatepicker}>

                <View style={styles.inputMainView}>
                    <View style={{flexDirection:"row"}}>
                    {/* <Input
                        editable={false}
                        placeholder={"DD/MM/YYYY"}
                        value={SelectedDate}
                        // onChangeText={(text) => setCustomerName(text)}
                        rightIcon={
                                <Icon
                                    name='chevron-down'
                                    size={17}
                                    color='black'
                                />
                        }
                    /> */}
                                     <Text style={{marginLeft:10,height:hp('7%'),marginTop:10, justifyContent:"center",alignItems:"center",width:wp('69%')}}>{SelectedDate==''?'YYYY/MM/DD':SelectedDate}</Text>

                    <Icon
                                    name='chevron-down'
                                    size={17}
                                    color='black'
                                    style={{alignSelf:"center",alignItems:"center",justifyContent:"center"}}

                                />
                    </View>
                </View>
                </TouchableOpacity>
                <View style={{ borderBottomColor: "black", backgroundColor: "#dfdfdf", borderBottomWidth: 1, width: "74%", marginBottom: 20, alignSelf: "center", marginRight: 20 }}>

</View>

                    <Text style={styles.inputTitle}>Recommendation status</Text>

                    <View style={styles.inputMainView}>

                        {/* {showDropDown == true ? */}
                        <View style={{ marginBottom: 10 }}>
                            <View style={{ height: 30, marginLeft: 10, width: wp('77%'), borderRadius: 4, borderColor: "#ffcc00", borderWidth: 1, marginTop: 10, justifyContent: "center" }}>
                                <CheckBox
                                    title='Open'
                                    textStyle={{ color: "#ffcc00" }}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={openChecked}
                                    containerStyle={{ backgroundColor: "#ffffff00" }}
                                    checkedColor={"#ffcc00"}
                                    uncheckedColor={"#ffcc00"}
                                    onPress={() => checkBoxPress('open')}

                                />

                            </View>
                            <View style={{ height: 30, marginLeft: 10, width: wp('77%'), borderRadius: 4, borderColor: "#0aa116", borderWidth: 1, marginTop: 10, justifyContent: "center" }}>
                                <CheckBox
                                    title='Booked'
                                    textStyle={{ color: "#0aa116" }}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={bookedChecked}
                                    containerStyle={{ backgroundColor: "#ffffff00" }}
                                    checkedColor={"#0aa116"}
                                    uncheckedColor={"#0aa116"}
                                    onPress={() => checkBoxPress('book')}

                                />

                            </View>
                            <View style={{ height: 30, marginLeft: 10, width: wp('77%'), borderRadius: 4, borderColor: "red", borderWidth: 1, marginTop: 10, justifyContent: "center" }}>
                                <CheckBox
                                    title='Cancelled'
                                    textStyle={{ color: "red" }}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={canceledChecked}
                                    containerStyle={{ backgroundColor: "#ffffff00" }}
                                    checkedColor={"red"}
                                    uncheckedColor={"red"}
                                    onPress={() => checkBoxPress('cancel')}
                                />

                            </View>
                        </View>
                    </View>
                    <View style={{marginTop:10}}>
                    <Text style={styles.inputTitle}>Remarks</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            style={styles.sqaureBorderInput}
                            onChangeText={(text) => setCustomerDetail(text)}
                        />
                    </View>
                    </View>
                    <TouchableOpacity onPress={() => saveRecord()}>
                        <View style={styles.submitBtn}>
                            <Text style={{ color: "white" }}>Submit</Text>

                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                />
            )}
        </View>
    );
};

export default Add;

