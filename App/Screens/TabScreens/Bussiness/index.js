import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground,BackHandler, Image, ScrollView, FlatList } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import { PostPDRecords, BusinessBookedList,ModalStatusBusiness } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import Modal from 'react-native-modal';
import { Input, CheckBox, } from 'react-native-elements';

const Bussiness = ({ navigation,handlePress }) => {
    const dispatch = useDispatch()
    const header = useSelector((state) => state.SHOW_HEADER);
    const [businessData, setBusinessData] = useState([])
const backStatus= useSelector((state) => state.BACK_STATUS)

const [currentStatus,setCurrentStatus]=useState( useSelector((state) => state.BUSSINESS_STATUS))
const showModaL= useSelector((state) => state.MODAL_STATUS_BUSINESS)

    useEffect(() => {
        getPdList()


        const unsubscribe = navigation.addListener('focus', () => {
            getPdList()

        });

        return () => {
            unsubscribe;
        };
    }, [navigation]);
    const getPdList = async () => {
        let res = await dispatch(BusinessBookedList())
        if (res.status == false) {
            // setNoRecord(true)
        }
        else {
            // setPDRecord(res.data)
            setBusinessData(res.data)
        }
    }
    useEffect(() => {
        if(backStatus==false){
        BackHandler.addEventListener('hardwareBackPress', handlePress);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', handlePress);
        };
    }
    else{
        BackHandler.addEventListener('hardwareBackPress', goToBack);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', goToBack);
        };
    }
      }, []);

      const goToBack=()=>{
          navigation.goBack()
          return true
      }
      const changeStatus=async(value)=>{
        setCurrentStatus(value)
await dispatch(ModalStatusBusiness(false))

      }
      const modalOff=async()=>{
        console.log("check")
await dispatch(ModalStatusBusiness(false))
    }
    const openModal=async()=>{
        await dispatch(ModalStatusBusiness(true))

      }
    return (

        <View>
            {header==true?
            <MainHeader navigation={navigation} showBack={true} title={'Business Booked'} showFilter={true} filterPress={()=>openModal()}></MainHeader>
         :null}
            {businessData == '' ?

                <Text style={{ textAlign: "center", marginTop: 30, fontSize: 20 }}>No Record Found</Text>
                :
                <ScrollView style={{marginBottom:20}}>
                    {currentStatus=='All'?
<View>
                    <FlatList
                        data={businessData}
                        renderItem={({ item }) =>

                            <View style={{


                                backgroundColor: "white",
                                shadowColor: "green",
                                shadowOffset: {
                                    width: 0,
                                    height: 4,
                                },
                                shadowOpacity: 0.20,
                                shadowRadius: 4.30,

                                elevation: 3,
                                alignSelf: "center", width: wp("90%"),
                                // padding: 10,
                                marginTop: 20,
                                marginBottom: 20,
                                borderColor: item.status=='RECEIVED'?"#ffd633" :"#0aa116",
                                borderWidth: 2,
                                borderRadius: 10,
                                paddingBottom: 10,
                            }}>
                                <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                                    <Text style={{ fontSize: 10, padding: 10, width: wp('59%'), textAlign: 'left', color: "grey" }}>{item.booking_date}</Text>
                                    <View style={{ height: 24, width: wp('24%'), backgroundColor:item.status=='RECEIVED'?"#ffd633" :"#0aa116", alignItems: "center", justifyContent: "center" }}>
                                        <Text style={{ fontSize: 12, color: "white" }}>{item.status=='RECEIVED'?'Received':'Given'}</Text>

                                    </View>
                                </View>

                                <View style={{ flexDirection: "row", marginTop: 20 }}>
                                    <View style={{ flexDirection: "column", width: wp('34%'), paddingLeft: 10 }}>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10, }}>{item.status=='RECEIVED'?'Given by':' Given To'} </Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Amount</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Business Details</Text>
                                        {/* <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Payment Mode</Text> */}



                                    </View>
                                    <View style={{ flexDirection: "column", width: wp('49%'), paddingLeft: 10 }}>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10, }}>:  {item.status=='RECEIVED'?item.given_from_name:item.given_to_name}</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: {item.business_amt}</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: {item.business_detail}</Text>
                                        {/* <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: {item.payment_status}</Text> */}



                                    </View>

                                </View>

                            </View>
                        }
                    />
                    </View>:
                    
                    
                    
                    <View>
                    <FlatList
                        data={businessData}
                        renderItem={({ item }) =>
<View>
    {item.status==currentStatus?
                            <View style={{


                                backgroundColor: "white",
                                shadowColor: "green",
                                shadowOffset: {
                                    width: 0,
                                    height: 4,
                                },
                                shadowOpacity: 0.20,
                                shadowRadius: 4.30,

                                elevation: 3,
                                alignSelf: "center", width: wp("90%"),
                                // padding: 10,
                                marginTop: 20,
                                marginBottom: 20,
                                borderColor: item.status=='RECEIVED'?"#ffd633" :"#0aa116",
                                borderWidth: 2,
                                borderRadius: 10,
                                paddingBottom: 10,
                            }}>
                                <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                                    <Text style={{ fontSize: 10, padding: 10, width: wp('59%'), textAlign: 'left', color: "grey" }}>{item.booking_date}</Text>
                                    <View style={{ height: 24, width: wp('24%'), backgroundColor:item.status=='RECEIVED'?"#ffd633" :"#0aa116", alignItems: "center", justifyContent: "center" }}>
                                        <Text style={{ fontSize: 14, color: "white" }}>{item.status=='RECEIVED'?'Recevied':'Given'}</Text>

                                    </View>
                                </View>

                                <View style={{ flexDirection: "row", marginTop: 20 }}>
                                    <View style={{ flexDirection: "column", width: wp('34%'), paddingLeft: 10 }}>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10, }}>{item.status=='RECEIVED'?'Given by':' Given To'} </Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Amount</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Business Details</Text>



                                    </View>
                                    <View style={{ flexDirection: "column", width: wp('49%'), paddingLeft: 10 }}>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10, }}>: {item.status=='RECEIVED'?item.given_from_name:item.given_to_name}</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: {item.business_amt}</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: {item.business_detail}</Text>



                                    </View>

                                </View>

                            </View>:null}
                            </View>

                        }
                    />
                    </View>
                    
                    }
                    {/* <View style={{

                        backgroundColor: "white",
                        shadowColor: "#ffd633",
                        shadowOffset: {
                            width: 0,
                            height: 4,
                        },
                        shadowOpacity: 0.20,
                        shadowRadius: 4.30,

                        elevation: 3,
                        alignSelf: "center", width: wp("90%"),
                        // padding: 10,
                        marginTop: 20,
                        marginBottom: 70,
                        borderColor: "#ffd633",
                        borderWidth: 2,
                        borderRadius: 10,

                    }}>
                        <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                            <Text style={{ fontSize: 10, padding: 10, width: wp('59%'), textAlign: 'left', color: "grey" }}>23 March 2021</Text>
                            <View style={{ height: 24, width: wp('24%'), backgroundColor: "#ffd633", alignItems: "center", justifyContent: "center" }}>
                                <Text style={{ fontSize: 14, }}>Recevied</Text>

                            </View>
                        </View>
                        <View style={{ flexDirection: "row", marginTop: 20 }}>
                            <View style={{ flexDirection: "column", width: wp('34%'), paddingLeft: 10 }}>
                                <Text style={{ fontSize: 12, color: "black", marginTop: 10, }}>Given To</Text>
                                <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Ammount</Text>
                                <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Business Details</Text>
                                <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Payment Mode</Text>



                            </View>
                            <View style={{ flexDirection: "column", width: wp('49%'), paddingLeft: 10, marginBottom: 10 }}>
                                <Text style={{ fontSize: 12, color: "black", marginTop: 10, }}>: Rachit Khandelwal</Text>
                                <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: 140000</Text>
                                <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: Loan Approval</Text>
                                <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: Cheque</Text>



                            </View>

                        </View>

                    </View> */}
                </ScrollView>
            }


<Modal

backdropOpacity={0.5}
backdropColor="black"
hasBackdrop={true}
onBackdropPress={() => modalOff()}
onRequestClose={() => modalOff()}
isVisible={showModaL}>
<View style={{ width: wp("80%"), height: hp("30%"), backgroundColor: "white", alignSelf: "center", justifyContent: "center", borderRadius: 10 }}>
<View style={{ height: 30, marginLeft: 10, width: wp('70%'), borderRadius: 4, borderColor: "#ffcc00", borderWidth: 1, marginTop: 10, justifyContent: "center" }}>
                                <CheckBox
                                    title='Received'
                                    textStyle={{ color: "#ffcc00" }}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={currentStatus=='RECEIVED'?true:false}
                                    containerStyle={{ backgroundColor: "#ffffff00",borderColor:'#ffffff00' }}
                                    checkedColor={"#ffcc00"}
                                    uncheckedColor={"#ffcc00"}
                                    onPress={() =>changeStatus('RECEIVED')}

                                />

                            </View>
                            <View style={{ height: 30, marginLeft: 10, width: wp('70%'), borderRadius: 4, borderColor: "#0aa116", borderWidth: 1, marginTop: 10, justifyContent: "center" }}>
                                <CheckBox
                                    title='Given'
                                    textStyle={{ color: "#0aa116" }}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={currentStatus=='GIVEN'?true:false}

                                    containerStyle={{ backgroundColor: "#ffffff00",borderColor:'#ffffff00'  }}
                                    checkedColor={"#0aa116"}
                                    uncheckedColor={"#0aa116"}
                                    onPress={() =>changeStatus('GIVEN')}

                                />

                            </View>
                            <View style={{ height: 30, marginLeft: 10, width: wp('70%'), borderRadius: 4, borderColor: "red", borderWidth: 1, marginTop: 10, justifyContent: "center" }}>
                                <CheckBox
                                    title='All'
                                    textStyle={{ color: "red" }}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={currentStatus=='All'?true:false}

                                    containerStyle={{ backgroundColor: "#ffffff00" ,borderColor:'#ffffff00' }}
                                    checkedColor={"red"}
                                    uncheckedColor={"red"}
                                    onPress={() =>changeStatus('All')}

                                />

                            </View>
</View>
</Modal>
        </View>
    );
};

export default Bussiness;

