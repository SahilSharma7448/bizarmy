import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image,
    ToastAndroid,
    BackHandler,
    TouchableOpacity,
    ScrollView, FlatList,Alert } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import { PostPDRecords,ModalStatus, RecommendationListApi,Add_Business_Mark,Mark_Cancel,Header_Status, Current_Recommendation_Status } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import Loader from "../../../Constants/Loader"
import Modal from 'react-native-modal';
import { Input, CheckBox, } from 'react-native-elements';

const RecommendationList = ({ navigation,handlePress,data }) => {
    const dispatch = useDispatch()
    const header = useSelector((state) => state.SHOW_HEADER);
const [currentStatus,setCurrentStatus]=useState( useSelector((state) => state.CURRENT_R_STATUS))
console.log("check data",currentStatus)

const showModaL= useSelector((state) => state.MODAL_STATUS)
const backStatus= useSelector((state) => state.BACK_STATUS)
const [amount,setAmount]=useState('')
console.log("chec back statys",backStatus)
console.log("check current status",showModaL)
    const [noRecord, setNoRecord] = useState(false)
    const [PDRecord, setPDRecord] = useState([])
    const [loading, setLoading] = useState(false)
    const [openChecked, setOpenChecked] = useState(false)
    const [bookedChecked, setBookedChecked] = useState()
    const [canceledChecked, setCancledChecked] = useState(false)
    // useEffect(() => {
    //     getPdList()
    // }, []);



    useEffect(() => {
        getPdList()


        const unsubscribe = navigation.addListener('focus', () => {
            getPdList()

        });

        return () => {
            unsubscribe;
        };
    }, [navigation]);
    const getPdList = async () => {
        let res = await dispatch(RecommendationListApi())
        if (res.status == false) {
            setNoRecord(true)
        }
        else {
            setPDRecord(res.data)
        }
    }
    const markBooked=async(item,api)=>{
    
       
        
if(api==true){
setCancledChecked(false)
setLoading(true)
      await  dispatch(Header_Status(true))
        console.log("check item",item)
        let res=await dispatch( Add_Business_Mark(item.id,item.given_to_id,item.given_from_id,amount,item.date))
    if(res.status==true){
        setLoading(false)
        // Alert.alert(
        //     '',
        //     res.message,
        //     [{ text: 'Ok', onPress: () => navigation.navigate('Bussiness') }],
        //     {
        //         cancelable: false,
        //     },
        // );
        // return true;
        ToastAndroid.show(res.message, ToastAndroid.showWithGravityAndOffset);
        navigation.navigate('Bussiness')
        return

    }
    else{
        setLoading(true)

    }
}
else{
    setCancledChecked(true)
    setAmount(item.approx_value)
    setBookedChecked(item)
}
    }
    const markCancel=async(item)=>{
console.log("check mark id",item.id)
Alert.alert(
    '',
    'Are you sure Mark Cancel',
    [
        { text: 'No', onPress: () => { }, style: 'cancel' },
        { text: 'Yes', onPress: () => cancel(item.id) },
    ],
    {
        cancelable: false,
    },
);
return true;
    }
    const cancel=async(item_id)=>{
        setLoading(true)
let res=await dispatch(Mark_Cancel(item_id))
if(res.status==true){

    getPdList()
    setLoading(false)
}
else{
    setLoading(false)
}
    }
    useEffect(() => {
        if(backStatus==false){
        BackHandler.addEventListener('hardwareBackPress', handlePress);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', handlePress);
        };
    }
    else{
        BackHandler.addEventListener('hardwareBackPress', goToBack);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', goToBack);
        };
    }
      }, []);

      const goToBack=()=>{
          navigation.goBack()
          return true
      }
      const openModal=async()=>{
        await dispatch(ModalStatus(true))

      }
      const modalOff=async()=>{
          console.log("check")
await dispatch(ModalStatus(false))
      }
      const changeStatus=async(value)=>{
        setCurrentStatus(value)
await dispatch(ModalStatus(false))

      }

      const BookedRecord=async()=>{

      }
    return (

        <View>
            <Loader isLoading={loading}></Loader>
            {/* {header==true?

            <MainHeader navigation={navigation} showBack={true} title={'Recommendations'} showFilter={true} filterPress={()=>openModal()}></MainHeader>
      :null} */}
            {noRecord == false ?
                <ScrollView style={{marginBottom:20}}>

{currentStatus=='All'?

<FlatList
data={PDRecord}
renderItem={({ item }) =>
<View >
    <View style={{


        backgroundColor: "white",
        shadowColor: "green",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.20,
        shadowRadius: 4.30,

        elevation: 3,
        alignSelf: "center", width: wp("90%"),
        // padding: 10,
        marginTop: 20,
        marginBottom: 20,
        borderColor: item.r_status=='RECEIVED'?"#ffd633" :"#0aa116",
        borderWidth: 2,
        borderRadius: 10,
        paddingBottom: 10,
    }}>
        <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
            <Text style={{ fontSize: 10, padding: 10, width: wp('59%'), textAlign: 'left', color: "grey" }}>{item.date}</Text>
            <View style={{ height: 24, width: wp('24%'), backgroundColor: item.r_status=='RECEIVED'?"#ffd633" :"#0aa116", alignItems: "center", justifyContent: "center" }}>
                <Text style={{ fontSize: 12, color: "white" }}>{item.r_status=='RECEIVED'?'Received':'Given '}</Text>

            </View>
        </View>

        <View style={{ flexDirection: "row", marginTop: 20 }}>
            <View style={{ flexDirection: "column", width: wp('30%'), paddingLeft: 10 }}>
                <Text style={{ fontSize: 12, color: "black", marginTop: 10, }}>{item.r_status=='RECEIVED'?'Given by':' Given To'}</Text>
                <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Amount</Text>
           
                <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Business Details</Text>
                <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Customer Name</Text>



            </View>
            <View style={{ flexDirection: "column", width: wp('37%'), paddingLeft: 10, }}>
                <Text style={{ fontSize: 12, color: "black", marginTop: 10, }}>:{item.r_status=='RECEIVED'?item.given_from:item.given_to}</Text>
                <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: {item.approx_value}</Text>
                <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: {item.business_detail}</Text>
                <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: {item.customer_name}</Text>



            </View>
            {item.status == 'OPEN' ?
                <ImageBackground source={require("../../../Assets/Icons/open.png")}
                    resizeMode={"contain"}

                    style={{ height: 70, width: 70, alignSelf: "center", justifyContent: "center", }}></ImageBackground>
                : null}
            {item.status == 'BOOKED' ?
                <ImageBackground source={require("../../../Assets/Icons/Booked.png")}
                    resizeMode={"contain"}

                    style={{ height: 70, width: 70, alignSelf: "center", justifyContent: "center", }}></ImageBackground>
                : null}
            {item.status == 'CANCELLED' ?
                <ImageBackground source={require("../../../Assets/Icons/Canceled.png")}
                    resizeMode={"contain"}

                    style={{ height: 70, width: 70, alignSelf: "center", justifyContent: "center", }}></ImageBackground>
                : null}
        </View>
        {item.status=='OPEN'?
        <View style={{ flexDirection: "row", justifyContent: "space-around", marginTop: 10 }}>
          
           <TouchableOpacity onPress={()=>markBooked(item)}>
            <View style={{
                height: 30, width: wp('24%'),
                backgroundColor: "#0aa116",
                borderRadius: 30, alignSelf: "flex-end", alignItems: 'center', justifyContent: "center",
            }}>
                <Text style={{ fontSize: 10, color: "white", }}>Mark Booked</Text>

            </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>markCancel(item)}>
            <View style={{ height: 30, width: wp('24%'), borderRadius: 30, alignSelf: "flex-end", alignItems: 'center', justifyContent: "center", borderColor: "red", borderWidth: 1, }}>
                <Text style={{ fontSize: 10, color: "red", }}>Mark Cancel</Text>

            </View>
            </TouchableOpacity>
          {item.r_status=='GIVEN'?

            <TouchableOpacity onPress={() => navigation.navigate('EditRecommendation',

                {
                    detail: item
                })}>
                <View style={{ height: 30, width: wp('24%'), borderRadius: 30, alignSelf: "flex-end", alignItems: 'center', justifyContent: "center", borderColor: "grey", borderWidth: 1, }}>
                    <Text style={{ fontSize: 10, color: "grey", }}>Edit Detail</Text>

                </View>
            </TouchableOpacity>:null}
        </View>:null}
    </View>
    </View>
} />
:

                    <View style={{marginBottom:40}}>
                    <FlatList
                        data={PDRecord}
                        renderItem={({ item }) =>
                        <View >
                            {item.r_status==currentStatus?
                            <View style={{


                                backgroundColor: "white",
                                shadowColor: "green",
                                shadowOffset: {
                                    width: 0,
                                    height: 4,
                                },
                                shadowOpacity: 0.20,
                                shadowRadius: 4.30,

                                elevation: 3,
                                alignSelf: "center", width: wp("90%"),
                                // padding: 10,
                                marginTop: 20,
                                marginBottom: 20,
                                borderColor: item.r_status=='RECEIVED'?"#ffd633" :"#0aa116",
                                borderWidth: 2,
                                borderRadius: 10,
                                paddingBottom: 10,
                            }}>
                                <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                                    <Text style={{ fontSize: 10, padding: 10, width: wp('59%'), textAlign: 'left', color: "grey" }}>{item.date}</Text>
                                    <View style={{ height: 24, width: wp('24%'), backgroundColor: item.r_status=='RECEIVED'?"#ffd633" :"#0aa116", alignItems: "center", justifyContent: "center" }}>
                                        <Text style={{ fontSize: 14, color: "white" }}>{item.r_status=='RECEIVED'?'Received':'Given'}</Text>

                                    </View>
                                </View>

                                <View style={{ flexDirection: "row", marginTop: 20 }}>
                                    <View style={{ flexDirection: "column", width: wp('30%'), paddingLeft: 10 }}>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10, }}>{item.r_status=='RECEIVED'?'Given by':' Given To'}</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Amount</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Business Details</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>Customer Name</Text>




                                    </View>
                                    <View style={{ flexDirection: "column", width: wp('37%'), paddingLeft: 10, }}>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10, }}>: {item.r_status=='RECEIVED'?item.given_from:item.given_to}</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: {item.approx_value}</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: {item.business_detail}</Text>
                                        <Text style={{ fontSize: 12, color: "black", marginTop: 10 }}>: {item.customer_name}</Text>




                                    </View>
                                    {item.status == 'OPEN' ?
                                        <ImageBackground source={require("../../../Assets/Icons/open.png")}
                                            resizeMode={"contain"}

                                            style={{ height: 70, width: 70, alignSelf: "center", justifyContent: "center", }}></ImageBackground>
                                        : null}
                                    {item.status == 'BOOKED' ?
                                        <ImageBackground source={require("../../../Assets/Icons/Booked.png")}
                                            resizeMode={"contain"}

                                            style={{ height: 70, width: 70, alignSelf: "center", justifyContent: "center", }}></ImageBackground>
                                        : null}
                                    {item.status == 'CANCELLED' ?
                                        <ImageBackground source={require("../../../Assets/Icons/Canceled.png")}
                                            resizeMode={"contain"}

                                            style={{ height: 70, width: 70, alignSelf: "center", justifyContent: "center", }}></ImageBackground>
                                        : null}
                                </View>
                                {item.status=='OPEN'?


                                <View style={{ flexDirection: "row", justifyContent: "space-around", marginTop: 10 }}>
                                   <TouchableOpacity onPress={()=>markBooked(item)}>
                                    <View style={{
                                        height: 30, width: wp('24%'),
                                        backgroundColor: "#0aa116",
                                        borderRadius: 30, alignSelf: "flex-end", alignItems: 'center', justifyContent: "center",
                                    }}>
                                        <Text style={{ fontSize: 10, color: "white", }}>Mark Booked</Text>

                                    </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>markCancel(item)}>
                                    <View style={{ height: 30, width: wp('24%'), borderRadius: 30, alignSelf: "flex-end", alignItems: 'center', justifyContent: "center", borderColor: "red", borderWidth: 1, }}>
                                        <Text style={{ fontSize: 10, color: "red", }}>Mark Cancel</Text>

                                    </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => navigation.navigate('EditRecommendation',

                                        {
                                            detail: item
                                        })}>
                                        <View style={{ height: 30, width: wp('24%'), borderRadius: 30, alignSelf: "flex-end", alignItems: 'center', justifyContent: "center", borderColor: "grey", borderWidth: 1, }}>
                                            <Text style={{ fontSize: 10, color: "grey", }}>Edit Detail</Text>

                                        </View>
                                    </TouchableOpacity>
                                </View>:null}
                            </View>:null}
                            </View>
                        } /></View>}
                </ScrollView> :
                <Text style={{ textAlign: "center", marginTop: 30, fontSize: 20 }}>No Record Found</Text>

            }



<Modal

backdropOpacity={0.5}
backdropColor="black"
hasBackdrop={true}
onBackdropPress={() => setCancledChecked(false)}
onRequestClose={() => setCancledChecked(false)}
isVisible={canceledChecked}>
<View style={{ width: wp("80%"), height: hp("30%"), backgroundColor: "white", alignSelf: "center",  borderRadius: 10 }}>
<Text style={{marginTop:10,textAlign:"center",fontWeight:"bold",fontSize:20}}>
    Enter Amount
</Text>
<View  style={{marginTop:20,}}>

<Input
    keyboardType={"number-pad"}
    placeholder={"Enter Amount "}
    onChangeText={(text) => setAmount(text)}
    value={amount}
/>
<TouchableOpacity onPress={()=>markBooked(bookedChecked,true)}>
<View style={{
                                        height: 30, width: wp('24%'),
                                        backgroundColor: "#0aa116",
                                        borderRadius: 30, alignSelf: 'center', alignItems: 'center', justifyContent: "center",
                                    }}>
                                        <Text style={{ fontSize: 10, color: "white", }} >Submit</Text>

                                    </View>
                                    </TouchableOpacity>
</View>

</View>
</Modal>
        </View>
    );
};

export default RecommendationList;

