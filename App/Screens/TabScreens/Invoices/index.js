import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground,ToastAndroid, Image,BackHandler, ScrollView,FlatList } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import styles from "./style"
import { Input, CheckBox, } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import DropDownPicker from 'react-native-dropdown-picker';
import {GetInvoice,Paymet_Transtion} from "../../../Redux/Action"
import {useDispatch,useSelector} from 'react-redux'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import { TouchableOpacity } from 'react-native-gesture-handler';
import RazorpayCheckout from 'react-native-razorpay';
import Loader from "../../../Constants/Loader"

const Invoices = ({ navigation,handlePress }) => {
    const dispatch=useDispatch()
    const [PDRecord, setPDRecord] = useState([])
    const [noRecord,setNoRecord]=useState(false)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        getPdList()


        const unsubscribe = navigation.addListener('focus', () => {
            getPdList()

        });

        return () => {
            unsubscribe;
        };
    }, [navigation]);
    const getPdList = async () => {
        let res = await dispatch(GetInvoice())
        if (res.status == false) {
            setNoRecord(true)
        }
        else {
            setPDRecord(res.data)
        }
    }
    // function handleBackButtonClick() {
    //     // navigation.goBack();
    //     console.log("checkback ")

    //     // Alert.alert(
    //     //     '',
    //     //     'Are you sure Logout',
    //     //     [
    //     //         { text: 'No', onPress: () => { }, style: 'cancel' },
    //     //         { text: 'Yes', onPress: () => removeAuth() },
    //     //     ],
    //     //     {
    //     //         cancelable: false,
    //     //     },
    //     // );
    //     return true;
    //     return true;
    //   }
    
      useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handlePress);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', handlePress);
        };
      }, []);
      const payInvoice=async(item)=>{
          const amt=item.commision_amt+"0"
          console.log("check amt",amt)
   
        var options = {
            currency: 'INR',
            key: 'rzp_live_hyB4KyVneYO7ks', // Your api key
            amount: amt+"0",
          
            theme: {color: '#F37254'}
          }
          RazorpayCheckout.open(options).then(async(data) => {
            // handle success
            console.log(`Success: ${data.razorpay_payment_id}`);
            console.log('Success:', data);
            setLoading(true)
            let res=await dispatch(Paymet_Transtion(item.id,item.commision_amt,data.razorpay_payment_id))
            if(res.status==true){
                await dispatch(GetInvoice())
                setLoading(false)
            
                ToastAndroid.show(res.message, ToastAndroid.showWithGravityAndOffset);
            
            }
            else{
                setLoading(false)
            
                ToastAndroid.show('Failed', ToastAndroid.showWithGravityAndOffset);
            
            }
          }).catch((error) => {
            // handle failure
            alert(`Error: Cancel by User`);
          });
      }
    return (

        <View>
            <Loader isLoading={loading}></Loader>

            {/* <MainHeader showBack={true} title={"Invoices     "} navigation={navigation}></MainHeader> */}
            <ScrollView >
               
               {noRecord==false?
               
               <View style={{ marginBottom: 100 }}>
                    {/* ///////////////////////// yellow view/////////////////////// */}
                    <FlatList
                    data={PDRecord}
                    renderItem={({ item }) =>
                   
                    <View style={{ borderColor:item.payment_status=='UNPAID'? "#ffd633":'#0aa116', borderWidth: 1, width: wp("90%"), marginTop: hp('7%'), alignSelf: "center" }}>
                        <View style={{ backgroundColor:item.payment_status=='UNPAID'? "#ffd633":'#0aa116', height: 40, flexDirection: "row", justifyContent: "space-between", alignItems: "center", paddingLeft: 20, paddingRight: 20 }}>

                            <Text style={{ fontSize: 24, fontWeight: "600" }}>BIZ{item.id}</Text>
                            <Text style={{ fontSize: 14, }}>{item.due_date}</Text>

                        </View>

                        <View style={{ flexDirection: "row", marginTop: 20, marginBottom: 20 }}>
                            <View style={{ flexDirection: "column", width: wp("40%"), paddingLeft: wp("7%") }}>
                            <Text style={{ fontSize: 14, lineHeight: 30 }}>Given by   </Text>
                               
                                <Text style={{ fontSize: 14, lineHeight: 30 }}>Amount   </Text>
                                <Text style={{ fontSize: 14, lineHeight: 30 }}>Due Date  </Text>
                                <Text style={{ fontSize: 14, lineHeight: 30 }}>Status    </Text>


                            </View>
                            <View style={{ flexDirection: "column" }}>
                                <Text style={{ fontSize: 14, lineHeight: 30 }}>:   </Text>
                                <Text style={{ fontSize: 14, lineHeight: 30 }}>:   </Text>
                                <Text style={{ fontSize: 14, lineHeight: 30 }}>:   </Text>
                                <Text style={{ fontSize: 14, lineHeight: 30 }}>:   </Text>

                            </View>
                            <View style={{ flexDirection: "column", width: wp("37%"), alignItems: "flex-end" }}>
                            <Text style={{ fontSize: 14, lineHeight: 30 }}>{item.given_from_name}   </Text>
                               
                                <Text style={{ fontSize: 14, lineHeight: 30 }}>₹ {item.commision_amt}   </Text>
                                <Text style={{ fontSize: 14, lineHeight: 30 }}>{item.due_date} </Text>
                                <Text style={{ fontSize: 14, lineHeight: 30 }}>{item.payment_status}    </Text>


                            </View>
                        </View>
                        <View style={{ alignSelf: "center", width: wp("70%"), borderWidth: 1, borderColor: "#adad85" }}></View>
                        <View style={{ flexDirection: "row", width: wp("80%"), alignSelf: "center", marginTop: 30, marginBottom: 30, justifyContent: "space-around" }}>
                           {item.payment_status=='UNPAID'?
                         <TouchableOpacity onPress={()=>payInvoice(item)}>
                         <View style={{ backgroundColor: "#ffd633", height: 30, borderRadius: 20, width: wp("20%"), alignItems: "center", justifyContent: "center" }}>
                                <Text>PAY</Text>
                            </View>
                            </TouchableOpacity>
                            :null}
                            <TouchableOpacity onPress={()=>navigation.navigate('PdfView',
                            {
                                pdfLink:item.download_link
                            }
                            )}>
                            <View style={{ height: 30, borderRadius: 20, width: wp("40%"), alignItems: "center", justifyContent: "center", borderColor:item.payment_status=='UNPAID'? '#ffd633':'#0aa116', borderWidth: 1 }}>
                                <Text>Download Invoice</Text>
                            </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    }
                    />


                </View>:
                <Text style={{ textAlign: "center", marginTop: 30, fontSize: 20 }}>No Record Found</Text>

}
            </ScrollView >
        </View >
    );
};

export default Invoices;

