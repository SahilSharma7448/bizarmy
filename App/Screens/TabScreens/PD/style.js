import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../../../Utility/index';

const styles = StyleSheet.create({

    inputTitle: { fontSize: 10, color: "grey", width: wp('80%'), alignSelf: 'center', },
    inputMainView: { width: wp('80%'), alignSelf: 'center', right: 10, },

    addPhotoImg: {
        alignSelf: "center", height: hp('14%'), width: wp("40%"),
        marginTop: 40

    },
    addPhotoBtn: {
        height: 30,
        borderRadius: 30,
        marginBottom: 30,
        width: wp("40%"), backgroundColor: "#0aa116", alignSelf: "center", alignItems: "center", justifyContent: "center", marginTop: 20
    },
    sqaureBorderInput: {
        borderWidth: 1,
        marginTop: 20, borderColor: "grey",
        height: hp('17%'),
        textAlignVertical: "top"
    },
    submitBtn: {
        height: 34,
        borderRadius: 30,
        marginBottom: 70,
        width: wp("40%"), backgroundColor: "#0aa116", alignSelf: "center", alignItems: "center", justifyContent: "center",
    }

});

export default styles;