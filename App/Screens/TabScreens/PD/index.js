import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground,BackHandler, Image, ScrollView, Alert ,ToastAndroid} from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import styles from "./style"
import { Input, CheckBox, BottomSheet, ListItem } from 'react-native-elements';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import * as Utility from "../../../Utility"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import { TouchableOpacity } from 'react-native-gesture-handler';
import { PostPDRecords, GetMyBridage } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
import DateTimePicker from '@react-native-community/datetimepicker';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from "moment";
import { Picker } from '@react-native-community/picker';

const PD = ({ navigation }) => {
    const dispatch = useDispatch()
    const [isVisible, setIsVisible] = useState(false);
    const [tagName, setTagName] = useState('')
    const [description, setDescription] = useState('')
    const [defaultImage, setDefault] = useState('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAVwAAACRCAMAAAC4yfDAAAAAMFBMVEXZ2dnV1dW2traqqqrOzs7Jycm+vr7c3Nytra2zs7PGxsa7u7vKysrY2NjS0tLDw8N7mvbnAAAC50lEQVR4nO3c0XKbMBRFUQSYK4FB//+3lW1wgHJAlGSStns92Q7uNLuMcoVTigIAAAAAAAAAAAAAAAAAAAAAAAAAAAD4W1nhLkhvh2TRVxf45ru/gR/MmsqXF/iq49xV+so7u6AI1fDd38OPVVfNpTPP2ur2WX+Xf87VuAVxNeJ+IeJ+ofNx01w8zKZb4mrn4zZl05RdPz0lrnY6bqgfE9gQ3PicuNrJuNYNr8NdGF8hrnYy7jDtxyzeXw+Iq52M29bTo2F8H3G1k3Hje6/rOuIeOXvm3qdHnLnHTsZ15bTmNuMCQVzt7LTQ3J+H2zBVJq52es4tY5Hm3HuYdhHE1U7HtVtZdiG+nxNX+4MLN1b0s7cQV9uMG4f84MTVNuK60Pg6uy5xtd/iWp3K9iHm1iWuto5rzet6V5n7oS5xtVXcPnSvByly3h9AXG0RNy0Jt+mZtd5tv2WJuNo8rkU/LErnDA3E1eZxy3L5Nefvx3WJq73j2uB/mxByhgbialPctMRuzbZdtx7U1gsxcbUpbve+ErNgcTE0WFeu/w2Iqz3jmvPqCoPd50NDGdP+YnkocbVHXLtV+idXWounc7UPrX1sM0bE1R5xG+/2fm65cfidhofnBvn9VeJqddWF8uigtBo8zuD32DtfGoir1b5qD8cta7rHlmL+Ap9EZMi7WG5tWCy0s6WBuFp23PVLblwaiKtlxbVmY1kelwbiallxty/uphE4LQ3E1XLiBnGIuRD5Dyc7juM6r8eJtB2OxJWO4prb2b09lwZPXOUgrtXV/hXztHUmrrIfN6U7+jSCNVfbjWvx+HM04mp7cWebXI242l7c9Wdqm4ir6bh9yPq9EOJqMq7+cGKJuJqIa0OV+dtixNW246bx9pbXlrg7NuNaW2X/Dilxta24Fg+3Dh/HElfbusdNF3rucfMZNu7OFHzg7kyfI21xL+G+Ynu4Ix4AAAAAAAAAAAAAAAAAAAAAAAAAAAD+W78Aqjkan4b2aooAAAAASUVORK5CYII=')
    const [selectedImage, setSelectedImage] = useState('')
    const [loading, setLoading] = useState(false)
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [SelectedDate, setSelectedDate] = useState('')
    const [myBridage, setBridage] = useState([])
    const [givenName, setGivenName] = useState('')


    useEffect(() => {

        getDropDownData()
    }, []);
    const getDropDownData = async () => {


        let response = await dispatch(GetMyBridage())
        await setBridage(response.data)
    }
    const launchCameras = () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        launchCamera(options, (response) => { // Use launchImageLibrary to open image gallery
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // const source = { uri: response.uri };
                setSelectedImage(response.uri)
                setIsVisible(false)
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                // console.log(source)
            }

        })
    }



    const launchImageLibrarys = () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        launchImageLibrary(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                console.log("check response uri", response.uri)
                setSelectedImage(response.uri)
                setIsVisible(false)
            }
        });

    }

    const list = [
        {
            title: 'Choose from Gallery',
            onPress: () => launchImageLibrarys(),
        },
        {
            title: 'Choose from camera',
            onPress: () => launchCameras(),

        },
        {
            title: 'Cancel',
            containerStyle: { backgroundColor: 'red' },
            titleStyle: { color: 'white' },
            onPress: () => setIsVisible(false),
        },
    ];
    const submitPD = async () => {

        if (await Utility.isFieldEmpty(givenName && description && SelectedDate)) {
        ToastAndroid.show("Please fill all fields.", ToastAndroid.showWithGravityAndOffset);
           
            return 
            // Alert.alert("PLease fill all fields")
        }
        else if (await selectedImage == '') {
        ToastAndroid.show("Please attach image", ToastAndroid.showWithGravityAndOffset);
            
            return 
        }
        else {
            let userId = await Utility.getFromLocalStorge('userId')
            var formdata = new FormData();
            formdata.append('user_id', userId);
            formdata.append('tag_person', givenName);
            formdata.append('tag_description', description);
            formdata.append('date', SelectedDate);


            formdata.append('photo', {
                uri: selectedImage,
                name: "PD",
                type: 'image/jpg'
            });
            // let body = {
            //     user_id: userId,
            //     tag_person: tagName,
            //     tag_description: description,
            //     photo: selectedImage
            // }
            setLoading(true)
            let res = await dispatch(PostPDRecords(formdata))
            if (res.status == true) {
                setLoading(false)

                // Alert.alert(
                //     '',
                //     'Person Tagged Successfully',
                //     [{ text: 'Ok', onPress: () => navigation.goBack() }],
                //     {
                //         cancelable: false,
                //     },
                // );
                // return true;
                ToastAndroid.show("Person Tagged Successfully", ToastAndroid.showWithGravityAndOffset);
                navigation.goBack() 
                return
            }
            else {
                setLoading(false)
                ToastAndroid.show(res.message, ToastAndroid.showWithGravityAndOffset);

                return 
            }
        }
    }
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
        let dates = moment(selectedDate).format("YYYY-MM-DD")
        console.log("check selected date", dates)
        setSelectedDate(dates)

    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };
    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);
    return (

        <View>
            <Loader isLoading={loading}></Loader>

            <MainHeader navigation={navigation} showBack={true} title={'Record PD'}></MainHeader>
            <ScrollView>
                <View style={{ flexDirection: "row",  alignSelf: "center", alignItems: "center", justifyContent: "center" }}>
                    <Image
                        source={{ uri: selectedImage == "" ? defaultImage : selectedImage }}
                        style={styles.addPhotoImg}></Image>
                    {/* <EvilIcons
                        style={{ left: 20, marginTop: 20 }}
                        name=""
                        size={24}
                        color="black"
                    /> */}
                </View>
                <View style={styles.addPhotoBtn}>
                    <Text style={{ color: "white" }} onPress={() => setIsVisible(true)}>Add Photo</Text>

                </View>

                <Text style={styles.inputTitle}>Date</Text>
                <TouchableOpacity onPress={showDatepicker}>

                <View style={styles.inputMainView}>
                    <View style={{flexDirection:"row"}}>
                    {/* <Input
                        editable={false}
                        placeholder={"DD/MM/YYYY"}
                        value={SelectedDate}
                        // onChangeText={(text) => setCustomerName(text)}
                        rightIcon={
                                <Icon
                                    name='chevron-down'
                                    size={17}
                                    color='black'
                                />
                        }
                    /> */}
                    <Text style={{marginLeft:10,height:hp('7%'),marginTop:10, justifyContent:"center",alignItems:"center",width:wp('69%')}}>{SelectedDate==''?'YYYY/MM/DD':SelectedDate}</Text>
                    <Icon
                                    name='chevron-down'
                                    size={17}
                                    color='black'
                                    style={{alignSelf:"center",alignItems:"center",justifyContent:"center"}}

                                />
                    </View>
                </View>
                </TouchableOpacity>
                <View style={{ borderBottomColor: "black", backgroundColor: "#dfdfdf", borderBottomWidth: 1, width: "74%", marginBottom: 20, alignSelf: "center", marginRight: 20 }}>

</View>
                {/* <Text style={styles.inputTitle}>Tag Person</Text>
                <View style={styles.inputMainView}>
                    <Input
                        placeholder={"Enter Name"}
                        onChangeText={(text) => setTagName(text)}

                    />
                </View> */}
                <Text style={styles.inputTitle}>Tag Person</Text>
                <View style={{ flex: 1 }}>
                    <Picker
                        selectedValue={givenName}
                        mode={"dropdown"}
                        style={{ height: 50, width: wp('80%'), alignSelf: "center", marginRight: wp('4%') }}
                        onValueChange={(itemValue, itemIndex) => setGivenName(itemValue)}
                    >
                        <Picker.Item label="Tag Person" value="" />

                        {myBridage.map((item) => (
                            <Picker.Item label={item.name} value={item.id} />

                        ))}
                    </Picker>
                </View>
                <View style={{ borderBottomColor: "#999966", backgroundColor: "#dfdfdf", borderBottomWidth: 1, width: "74%", marginBottom: 20, alignSelf: "center", marginRight: 20 }}>

                </View>

                <Text style={styles.inputTitle}>PD Description</Text>
                <View style={styles.inputMainView}>
                    <Input
                        style={styles.sqaureBorderInput}
                        onChangeText={(text) => setDescription(text)}
                        multiline={true}
                        textAlign="left"

                    />
                </View>
                <TouchableOpacity onPress={() => submitPD()}>
                    <View style={styles.submitBtn}>
                        <Text style={{ color: "white" }}>Submit</Text>

                    </View>
                </TouchableOpacity>
            </ScrollView>
            <BottomSheet
                isVisible={isVisible}
                containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
            >
                {list.map((l, i) => (
                    <ListItem key={i} containerStyle={l.containerStyle} onPress={l.onPress}>
                        <ListItem.Content>
                            <ListItem.Title style={l.titleStyle}>{l.title}</ListItem.Title>
                        </ListItem.Content>
                    </ListItem>
                ))}
            </BottomSheet>
            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                />
            )}
        </View>
    );
};

export default PD;

