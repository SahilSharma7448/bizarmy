import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground,BackHandler, Image, ScrollView, Button, TextInput } from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import styles from "./style"
import { Input, CheckBox, } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Picker } from '@react-native-community/picker';
import { GetBusinessCategory, GetMyBridage, RecommendationSave, EditRecomendationForm } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
import * as Utility from "../../../Utility"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Alert } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from "moment";
const EditRecommendation = ({ route, navigation }) => {
    const { detail } = route.params
    console.log("check my recomendation dara", detail)
    const dispatch = useDispatch()
    const [bussinessCategory, setBusniessCategory] = useState([])
    const [myBridage, setBridage] = useState([])
    const [selectedValue, setSelectedValue] = useState("");
    const [givenName, setGivenName] = useState('')
    const [customerNAme, setCustomerName] = useState('')
    const [customerDetail, setCustomerDetail] = useState(detail.business_detail)
    const [aprroxValue, setApproxValue] = useState(detail.approx_value)
    const [loading, setLoading] = useState(false)
    const [showDropDown, setShowDropDwon] = useState(false)
    const [dropDownValue, setdropDownValue] = useState('Recommendation status')
    const [openChecked, setOpenChecked] = useState(false)
    const [bookedChecked, setBookedChecked] = useState(false)
    const [canceledChecked, setCancledChecked] = useState(false)
    const [status, setStatus] = useState('')
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [SelectedDate, setSelectedDate] = useState(detail.date)
    const [deafultStatus, setDefault] = useState(detail.status)
    const saveRecord = async () => {

        if (await Utility.isFieldEmpty(customerDetail && aprroxValue && SelectedDate))
            return Alert.alert('Please fill all fields')
        else {
            setLoading(true)
            let res = await dispatch(EditRecomendationForm(customerDetail, aprroxValue, status, SelectedDate, deafultStatus, detail.id))

            if (res.status == true) {
                setLoading(false)
                Alert.alert(
                    '',
                    res.msg,
                    [{ text: 'Ok', onPress: () => navigation.goBack() }],
                    {
                        cancelable: false,
                    },
                );
                return true;
            }
            else {
                setLoading(false)
                return Alert.alert(res.msg)
            }
        }
    }
    const dropFun = () => {
        console.log("check data")
        setShowDropDwon(true)
    }
    const checkBoxPress = (value) => {
        setDefault('')
        if (value == 'open') {
            setOpenChecked(true)
            setBookedChecked(false)
            setCancledChecked(false)
            setStatus('OPEN')
            return
        }
        else if (value == 'book') {
            setBookedChecked(true)
            setOpenChecked(false)
            setCancledChecked(false)

            setStatus('BOOKED')


            return
        }
        else {
            setCancledChecked(true)
            setOpenChecked(false)
            setBookedChecked(false)
            setStatus('CANCELLED')


        }

    }

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
        let dates = moment(selectedDate).format("YYYY-MM-DD")
        console.log("check selected date", dates)
        setSelectedDate(dates)

    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };
    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);
    return (

        <View>
            <Loader isLoading={loading}></Loader>

            <MainHeader navigation={navigation} showBack={true} title={'Record Recommendation'}></MainHeader>
            <ScrollView>
                <View style={{ marginTop: 30 }}>









                    <Text style={styles.inputTitle}>Approx Value</Text>
                    <View style={styles.inputMainView}>

                        <Input
                            keyboardType={"number-pad"}
                            placeholder={"Enter Approx Value"}
                            onChangeText={(text) => setApproxValue(text)}
                            value={aprroxValue}
                        />
                    </View>






                    <Text style={styles.inputTitle}>Date</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            editable={false}
                            placeholder={"DD/MM/YYYY"}
                            value={SelectedDate}
                            // onChangeText={(text) => setCustomerName(text)}
                            rightIcon={
                                <TouchableOpacity onPress={showDatepicker}>
                                    <Icon
                                        name='chevron-down'
                                        size={17}
                                        color='black'
                                    />
                                </TouchableOpacity>
                            }
                        />
                    </View>

                    <Text style={styles.inputTitle}>Recommendation status</Text>

                    <View style={styles.inputMainView}>

                        {/* {showDropDown == true ? */}
                        <View style={{ marginBottom: 10 }}>
                            <View style={{ height: 30, marginLeft: 10, width: wp('77%'), borderRadius: 4, borderColor: "#ffcc00", borderWidth: 1, marginTop: 10, justifyContent: "center" }}>
                                <CheckBox
                                    title='Open'
                                    textStyle={{ color: "#ffcc00" }}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={openChecked || deafultStatus == 'OPEN' ? true : false}
                                    containerStyle={{ backgroundColor: "#ffffff00" }}
                                    checkedColor={"#ffcc00"}
                                    uncheckedColor={"#ffcc00"}
                                    onPress={() => checkBoxPress('open')}

                                />

                            </View>
                            <View style={{ height: 30, marginLeft: 10, width: wp('77%'), borderRadius: 4, borderColor: "#0aa116", borderWidth: 1, marginTop: 10, justifyContent: "center" }}>
                                <CheckBox
                                    title='Booked'
                                    textStyle={{ color: "#0aa116" }}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={bookedChecked || deafultStatus == 'BOOKED' ? true : false}
                                    containerStyle={{ backgroundColor: "#ffffff00" }}
                                    checkedColor={"#0aa116"}
                                    uncheckedColor={"#0aa116"}
                                    onPress={() => checkBoxPress('book')}

                                />

                            </View>
                            <View style={{ height: 30, marginLeft: 10, width: wp('77%'), borderRadius: 4, borderColor: "red", borderWidth: 1, marginTop: 10, justifyContent: "center" }}>
                                <CheckBox
                                    title='Cancelled'
                                    textStyle={{ color: "red" }}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={canceledChecked || deafultStatus == 'CANCELLED' ? true : false}
                                    containerStyle={{ backgroundColor: "#ffffff00" }}
                                    checkedColor={"red"}
                                    uncheckedColor={"red"}
                                    onPress={() => checkBoxPress('cancel')}
                                />

                            </View>
                        </View>
                    </View>
                    <Text style={styles.inputTitle}>Remarks</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            style={styles.sqaureBorderInput}
                            onChangeText={(text) => setCustomerDetail(text)}
                            value={customerDetail}
                        />
                    </View>
                    <TouchableOpacity onPress={() => saveRecord()}>
                        <View style={styles.submitBtn}>
                            <Text style={{ color: "white" }}>Submit</Text>

                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                />
            )}
        </View>
    );
};

export default EditRecommendation;

