import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image, ScrollView,BackHandler, FlatList ,Linking} from 'react-native';
import MainHeader from '../../../Components/MainHeader';
import { Input, CheckBox, BottomSheet, ListItem } from 'react-native-elements';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import * as Utility from "../../../Utility"
import FontAwesome from "react-native-vector-icons/FontAwesome"

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import { TouchableOpacity } from 'react-native-gesture-handler';
import { PostPDRecords, PdListRecordApi } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
var userId=''

const PD = ({ navigation,handlePress }) => {
    const dispatch = useDispatch()
    const header = useSelector((state) => state.SHOW_HEADER);
    const backStatus= useSelector((state) => state.BACK_STATUS)

    const [noRecord, setNoRecord] = useState(false)
    const [PDRecord, setPDRecord] = useState([])
    const pdData = [
        { id: 1 },
        { id: 2 },

        { id: 3 },
        { id: 4 },

    ]
    // useEffect(() => {
    //     getPdList()
    // }, []);

    useEffect(() => {
        getPdList()


        const unsubscribe = navigation.addListener('focus', () => {
            getPdList()

        });

        return () => {
            unsubscribe;
        };
    }, [navigation]);
    const getPdList = async () => {
        let id=await Utility.getFromLocalStorge('userId')

        userId=id
        let res = await dispatch(PdListRecordApi())
        if (res.status == false) {
            setNoRecord(true)
        }
        else {
            setPDRecord(res.data)
        }
    }
    useEffect(() => {
        if(backStatus==false){
        BackHandler.addEventListener('hardwareBackPress', handlePress);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', handlePress);
        };
    }
    else{
        BackHandler.addEventListener('hardwareBackPress', goToBack);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', goToBack);
        };
    }
      }, []);

      const goToBack=()=>{
          navigation.goBack()
          return true
      }

      const openWhatsApp = (mobile) => {
          console.log("check mobile",mobile)
        let msg = 'hello';
        if (mobile) {
          if (msg) {
            let url =
              "whatsapp://send?text=" +
            '' +
              "&phone=91" +
              '';
            Linking.openURL(url)
              .then(data => {
                console.log("WhatsApp Opened successfully " );
              })
              .catch(() => {
                alert("Make sure WhatsApp installed on your device");
              });
          } else {
            alert("Please enter message to send");
          }
        } else {
          alert("Please enter mobile no");
        }
      };

    return (

        <View style={{}}>
{header==true?
            <MainHeader navigation={navigation} showBack={true} title={'Personal Discussion'}></MainHeader>
           :null}
            {/* <View style={{ marginBottom: 200 }}> */}
            {noRecord == false ?
                <FlatList
                    data={PDRecord}
                    renderItem={({ item }) =>
                        <View style={{
                            backgroundColor: "white",
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 6,
                            },
                            shadowOpacity: 0.39,
                            shadowRadius: 8.30,

                            elevation: 7,
                            alignSelf: "center", width: wp("90%"),
                            padding: 10,
                            marginTop: 30,
                            marginBottom: 10
                        }}>
                            <View style={{ flexDirection: "row", }}>
                                <Image
                                    source={{ uri: 'http://dilkhushpvtltd.com/bizarmy/public/' + item.photo }}
                                    style={{
                                        alignSelf: "center", height: hp('10%'), width: wp("24%"),

                                    }}></Image>
                                <View style={{ width: wp("58%"), alignItems: "center", flexDirection: "column" }}>
                                    <Text style={{ fontSize: 15, fontWeight: 'bold', lineHeight: 24 }}>{item.tag_person_name}</Text>
                                    <Text style={{ fontSize: 15, fontWeight: '100' }}>Date : {item.date}</Text>
                                    <View  style={{flexDirection:"row",justifyContent:"space-between",}}>
                               <TouchableOpacity onPress={()=>navigation.navigate('EditPD',{
                                   item:item
                               })}>
                                   {item.user_id==userId?
                                <View style={{ height: 30, width: wp('24%'), borderRadius: 30, alignSelf: "flex-end", alignItems: 'center', justifyContent: "center", borderColor: "grey", borderWidth: 1,marginTop:18 }}>
                    <Text style={{ fontSize: 10, color: "grey", }}>Edit Detail</Text>

                </View>:null}
                </TouchableOpacity >
                <TouchableOpacity onPress={()=>openWhatsApp(item)}>
                <FontAwesome
                                        name='whatsapp'
                                        size={27}
                                        color='#0aa116'
                                        style={{ marginTop: 20,marginLeft:40 }}
                                    />
                                    </TouchableOpacity>
                                    </View>
                                </View>

                             
                            </View>
                           
                            <View style={{ flexDirection: "row", marginTop: 20 }}>
                                <Text style={{ fontSize: 15, fontWeight: '100' }}>PD Description </Text>
                                <View style={{ borderTopColor: "black", borderTopWidth: 2, width: wp("53%"), top: 10 }}></View>
                            </View>
                            <Text style={{ fontSize: 15, fontWeight: '100', marginTop: 14 }}>{item.tag_description} </Text>

                        </View>

                    }
                /> :
                <Text style={{ textAlign: "center", marginTop: 30, fontSize: 20 }}>No Record Found</Text>
            }
            {/* </View> */}


        </View>
    );
};

export default PD;

