import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground,BackHandler, Image, StyleSheet, TouchableOpacity, FlatList, ScrollView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../../Utility/index"
// import Feather from 'react-native-vector-icons/Feather'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { PostPDRecords, MyDashboardData,Current_Recommendation_Status,Current_Business_Status } from "../../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import MainHeader from '../../../../Components/MainHeader';
import * as Utility from "../../../../Utility/index"
var id=''
const DashBoardDetail = ({ navigation, onPress, title, showBack }) => {
    const dispatch = useDispatch()
    const [myData, setMyData] = useState({})
    useEffect(() => {
        myDashBoardData()


        const unsubscribe = navigation.addListener('focus', () => {
            myDashBoardData()

        });

        return () => {
            unsubscribe;
        };
    }, [navigation]);
    const myDashBoardData = async () => {
        let userId=await Utility.getFromLocalStorge('userId')
        id=userId
        let res = await dispatch(MyDashboardData())
        if (res.status == false) {
            // setNoRecord(true)
        }
        else {
            setMyData(res)
        }
    }
    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);

    const goToStatus=async(value)=>{
await dispatch(Current_Recommendation_Status(value))
        navigation.navigate('RecommendationList')
    }
    const goToBusiness=async(value)=>{
await dispatch(Current_Business_Status(value))

        navigation.navigate('Bussiness')

    }
    return (

        <View style={{ flex: 1 }}>
            <MainHeader showBack={true} title={'My Dashboard'} navigation={navigation}></MainHeader>


            <ScrollView>
                <View style={{ flexDirection: "row" }}>
                    {/* <FlatList
                        data={screenName}
                        numColumns={2}
                        renderItem={({ item }) => */}
                        <TouchableOpacity onPress={()=>goToStatus('GIVEN')}>
                    <View style={{
                        shadowColor: "#000",
                        width: wp('40%'),
                        shadowOffset: {
                            width: 0,
                            height: 6,
                        },
                        shadowOpacity: 0.39,
                        shadowRadius: 8.30,
                        padding: 10,
                        elevation: 13,
                        backgroundColor: "white",
                        marginLeft: wp('7%'),
                        marginTop: 20,
                        marginBottom: 10
                    }}>
                        {/* <Icon
                                    name={item.name}
                                    size={24}
                                    color={item.color}
                                /> */}
                        <Image source={require("../../../../Assets/Icons/given.png")} style={{ height: 30, width: 30, }} resizeMode={"contain"}></Image>

                        <Text style={{ fontSize: 49, fontWeight: "700", textAlign: "center", }}>{myData.given_from_recommendation}</Text>
                        <Text style={{ fontSize: 12, textAlign: "center", color: "grey" }}>{"Recommendation\nGiven"}</Text>
                    </View>
                    </TouchableOpacity>
                    

                    <TouchableOpacity onPress={()=>goToStatus('RECEIVED')}>

                    <View style={{
                        shadowColor: "#000",
                        width: wp('40%'),
                        shadowOffset: {
                            width: 0,
                            height: 6,
                        },
                        shadowOpacity: 0.39,
                        shadowRadius: 8.30,
                        padding: 10,
                        elevation: 13,
                        backgroundColor: "white",
                        marginLeft: wp('7%'),
                        marginTop: 20,
                        marginBottom: 10
                    }}>
                        {/* <Icon
                                    name={item.name}
                                    size={24}
                                    color={item.color}
                                /> */}
                        <Image source={require("../../../../Assets/Icons/recevied.png")} style={{ height: 30, width: 30, }} resizeMode={"contain"}></Image>

                        <Text style={{ fontSize: 49, fontWeight: "700", textAlign: "center", }}>{myData.given_to_recommendation}</Text>
                        <Text style={{ fontSize: 12, textAlign: "center", color: "grey" }}>{"Recommendation\nReceived"}</Text>

                    </View>
                    </TouchableOpacity>
                    {/* }

                    /> */}
                </View>

                {/* /////////////////////// */}


                <View style={{ flexDirection: "row" }}>
                    {/* <FlatList
                        data={screenName}
                        numColumns={2}
                        renderItem={({ item }) => */}
                <TouchableOpacity onPress={()=>goToBusiness('GIVEN')}>

                    <View style={{
                        shadowColor: "#000",
                        width: wp('40%'),
                        shadowOffset: {
                            width: 0,
                            height: 6,
                        },
                        shadowOpacity: 0.39,
                        shadowRadius: 8.30,
                        padding: 10,
                        elevation: 13,
                        backgroundColor: "white",
                        marginLeft: wp('7%'),
                        marginTop: 20,
                        marginBottom: 10
                    }}>
                        {/* <Icon
                                    name={item.name}
                                    size={24}
                                    color={item.color}
                                /> */}
                        <Image source={require("../../../../Assets/Icons/BusinessGiven.png")} style={{ height: 30, width: 30, }} resizeMode={"contain"}></Image>

                        <Text style={{ fontSize: 49, fontWeight: "700", textAlign: "center", }}>{myData.business_given}</Text>
                        <Text style={{ fontSize: 12, textAlign: "center", color: "grey" }}>{"Business\nGiven"}</Text>

                    </View>
                    </TouchableOpacity>
                <TouchableOpacity onPress={()=>goToBusiness('RECEIVED')}>
                   
                    <View style={{
                        shadowColor: "#000",
                        width: wp('40%'),
                        shadowOffset: {
                            width: 0,
                            height: 6,
                        },
                        shadowOpacity: 0.39,
                        shadowRadius: 8.30,
                        padding: 10,
                        elevation: 13,
                        backgroundColor: "white",
                        marginLeft: wp('7%'),
                        marginTop: 20,
                        marginBottom: 10
                    }}>
                        {/* <Icon
                                    name={item.name}
                                    size={24}
                                    color={item.color}
                                /> */}
                        <Image source={require("../../../../Assets/Icons/BusinessReceived.png")} style={{ height: 30, width: 30, }} resizeMode={"contain"}></Image>

                        <Text style={{ fontSize: 49, fontWeight: "700", textAlign: "center", }}>{myData.business_recieved}</Text>
                        <Text style={{ fontSize: 12, textAlign: "center", color: "grey" }}>{"Business\nReceived"}</Text>

                    </View>
                    </TouchableOpacity>
                    {/* }

                    /> */}
                </View>

                {/* ////////////////////////// */}
                <View style={{ marginBottom: 40, flexDirection: "row" }}>
                    {/* <FlatList
                        data={screenName}
                        numColumns={2}
                        renderItem={({ item }) => */}
                <TouchableOpacity onPress={()=>navigation.navigate('PdList')}>

                    <View style={{
                        shadowColor: "#000",
                        width: wp('40%'),
                        shadowOffset: {
                            width: 0,
                            height: 6,
                        },
                        shadowOpacity: 0.39,
                        shadowRadius: 8.30,
                        padding: 10,
                        elevation: 13,
                        backgroundColor: "white",
                        marginLeft: wp('7%'),
                        marginTop: 20,
                        marginBottom: 10
                    }}>
                        {/* <Icon
                                    name={item.name}
                                    size={24}
                                    color={item.color}
                                /> */}
                        <Image source={require("../../../../Assets/Icons/PDRed.png")} style={{ height: 30, width: 30, }} resizeMode={"contain"}></Image>

                        <Text style={{ fontSize: 49, fontWeight: "700", textAlign: "center", }}>{myData.pd_done}</Text>
                        <Text style={{ fontSize: 12, textAlign: "center", color: "grey" }}>{"Personal\nDiscussion"}</Text>
                         
                    </View>
                    </TouchableOpacity>
                <TouchableOpacity onPress={()=>navigation.navigate('Attendance')}>

                    <View style={{
                        shadowColor: "#000",
                        width: wp('40%'),
                        shadowOffset: {
                            width: 0,
                            height: 6,
                        },
                        shadowOpacity: 0.39,
                        shadowRadius: 8.30,
                        padding: 10,
                        elevation: 13,
                        backgroundColor: "white",
                        marginLeft: wp('7%'),
                        marginTop: 20,
                        marginBottom: 10
                    }}>
                        {/* <Icon
                                    name={item.name}
                                    size={24}
                                    color={item.color}
                                /> */}
                        <Image source={require("../../../../Assets/Icons/Badge.png")} style={{ height: 30, width: 30, }} resizeMode={"contain"}></Image>

                        <Text style={{ fontSize: 49, fontWeight: "700", textAlign: "center", }}>{myData.attendance}</Text>
                        <Text style={{ fontSize: 12, textAlign: "center", color: "grey" }}>{"Attendance"}</Text>
                        <Text style={{ fontSize: 12, textAlign: "center", color: "grey" }}>{"       "}</Text>

                    </View>
                    </TouchableOpacity>
                    {/* }

                    /> */}
                </View>







            </ScrollView>
        </View>

    );
};

export default DashBoardDetail;

const styles = StyleSheet.create({

});