import React, { useEffect, } from 'react';
import { useState,useRef } from 'react';
import { View, Text, ImageBackground, Image, ScrollView,Alert, FlatList, TouchableOpacity,BackHandler } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import MainHeader from '../../../Components/MainHeader';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { BannerApi, TrendingPostApi,Header_Status,BackStatus } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import FastImage from 'react-native-fast-image';
import * as Utility from "../../../Utility/index"
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { TapGestureHandler } from 'react-native-gesture-handler';
import ViewShot from 'react-native-view-shot';
import {captureRef} from 'react-native-view-shot'
import Share from "react-native-share"
import useDynamicRefs from 'use-dynamic-refs';
var RNFS = require("react-native-fs");
const DashBoard = ({ navigation }) => {
    const dispatch = useDispatch()
    const [bannerUrl, setBannerUrl] = useState([])
    const [trendingPost, setTrendingPost] = useState([])
    const ProductsImageRef = useRef("")
    const [activeSlide, setActiveSlide] = useState(0)
    const viewShot=useRef()
    const viewShotPost=useRef()
    const [getRef, setRef] = useDynamicRefs();
    useEffect(() => {
        getBanner()
        getTrendingPost()
    }, []);
    const getBanner = async () => {
        let res = await dispatch(BannerApi())
        if (res.status == true) {
            // res.data.map((item) => {
            //     console.log("checkk item", item)
            //     setBannerUrl(item.banner)
            // })
            console.log("check data banner",res)
            setBannerUrl(res.data)
        }

    }
    const getTrendingPost = async () => {
        let res = await dispatch(TrendingPostApi())
        if (res.status == true) {
            setTrendingPost(res.data)
        }
    }
    const goToReadrMore = (item) => {
        console.log("check read", item)
        navigation.navigate('ReadMore', {
            ReadMoreData: item
        })
    }
   
 

     const shareBanner=(item)=>{
        let shareOptions = {
            title: 'Title',
            message: 'http://dilkhushpvtltd.com/bizarmy/public/' + item.banner,
            subject: 'Subject',
            url:'http://dilkhushpvtltd.com/bizarmy/public/' + item.banner
        };

        Share.share(shareOptions)
            .then(res => {
                console.log(res);
            })
            .catch(err => {
                err && console.log(err);
            });
     }
const   captureAndShareScreenshot = (item) => {

  captureRef(viewShot, {
    format: "jpg",
    quality: 0.8
  }).then(
    uri => {
      RNFS.readFile(uri, 'base64').then((res) => {
        let urlString = 'data:image/jpeg;base64,' + res;
        let options = {
          title: 'Share Title',
          message: '',

          url: urlString,
          type: 'image/jpeg',
        };
        Share.open(options)
          .then((res) => {
            console.log(res);
          })
          .catch((err) => {
            err && console.log(err);
          });
      });
    },
    error => console.error("Oops, snapshot failed", error)
  );
  };


    const goToMyDashboard=async()=>{

await dispatch(BackStatus(true))
        
       await dispatch(Header_Status(true))
        navigation.navigate('DashBoardDetail')
    }
    function handleBackButtonClick() {
        // navigation.goBack();
        console.log("checkback ")

        Alert.alert(
            '',
            'Are you sure you want to close the app?',
            [
                { text: 'No', onPress: () => { }, style: 'cancel' },
                { text: 'Yes', onPress: () => BackHandler.exitApp() },
            ],
            {
                cancelable: false,
            },
        );
        return true;
        return true;
      }
    
      useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
      }, []);
    return (

        <View>
            <MainHeader navigation={navigation}></MainHeader>
            {/* <MainHeader navigation={navigation} showBack={true} title={'Trending Post'}></MainHeader> */}

            <ScrollView>

                <View style={{ flexDirection: "row" }}>
                    <View style={{ width: wp('50%'), alignSelf: "center", alignItems: "center", height: hp('7%'), justifyContent: "center", }}>
                        <Text style={{ color: "#0aa116", fontSize: 14 }}>Home</Text>
                    </View>
                    <TouchableOpacity onPress={()=>navigation.navigate('Badges')}>
                    <View style={{ width: wp('50%'), alignSelf: "center", alignItems: "center", height: hp('7%'), justifyContent: "center", backgroundColor: "#ebebe0" }}>
                        <Text style={{ color: "red", fontSize: 14 }}>Badges</Text>
                    </View>
                    </TouchableOpacity>
                </View>


                {/* <Image
                    source={{ uri: 'http://dilkhushpvtltd.com/bizarmy/public/' + bannerUrl }}
                    style={{
                        alignSelf: "center", height: hp('24%'), width: wp("90%"),
                        marginTop: 20,


                    }}
                    resizeMode={'stretch'}
                ></Image> */}
                

{/* <Carousel
                        style={{ backgroundColor: "red" }}

                        containerCustomStyle={{ backgroundColor: "#F2F2F200", alignSelf: "center", }}
                        // layout={'stack'} layoutCardOffset={`18`}
                        loop
                        useScrollView
                        autoplay
                        apparitionDelay={10}
                        autoplayDelay={1000}
                        loopClonesPerSide={100}
                        ref={ProductsImageRef}
                        data={bannerUrl}
                        renderItem={_renderItem}
                        sliderWidth={wp("90%")}
                        itemWidth={wp("90%")}
                        onSnapToItem={(index) => setActiveSlide(index)}
                        
                    /> */}
                    <ViewShot ref={viewShot}
    options={{format: 'jpg', quality: 0.9}}>
    <SwiperFlatList
      autoplay={true}
      autoplayDelay={5}
      autoplayLoop
      index={0}
      data={bannerUrl}
      style={{alignSelf:"center",width:wp('95%')}}
      renderItem={({ item }) => (
<View style={{flexDirection:"row"}}>


            <FastImage
                source={{ uri:'http://dilkhushpvtltd.com/bizarmy/public/' + item.banner }} style={{ height: hp("30%"), width: wp("85%"),alignSelf:"center",marginLeft:5 }}
                resizeMode={FastImage.resizeMode.contain}
            />
            <TouchableOpacity onPress={() => captureAndShareScreenshot(item)}>
                        <EvilIcons
                            style={{ marginTop:hp('3%'),marginRight:20}}
                            name="share-google"
                            size={30}
                            color="black"
                        />
                    </TouchableOpacity>
            </View>
      )}
    />

</ViewShot>      


                <View style={{ flexDirection: "row", width: wp("90%"), alignSelf: "center", justifyContent: "space-around", marginTop: 10 }}>
                    <TouchableOpacity onPress={() =>goToMyDashboard()}>

                        <View style={{ height: 30, width: wp("40%"), borderRadius: 7, justifyContent: "center", backgroundColor: "red", alignItems: "center" }}>
                            <Text style={{ color: "white", fontSize: 10, fontWeight: "600" }}>My DashBoard</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate('Attendence')}>

                        <View style={{ height: 30, width: wp("40%"), borderRadius: 7, justifyContent: "center", backgroundColor: "red", alignItems: "center" }}>
                            <Text style={{ color: "white", fontSize: 10, fontWeight: "600" }}>Attendance</Text>
                        </View>
                    </TouchableOpacity>

                    
                </View>
                <View style={{ flexDirection: "row", width: wp("90%"), alignSelf: "center", justifyContent: "space-around", marginTop: 10 }}>
                <TouchableOpacity onPress={() => navigation.navigate('MemberDetail')}>
                        <View style={{ height: 30, width: wp("40%"), borderRadius: 7, justifyContent: "center", backgroundColor: "red", alignItems: "center" }}>
                            <Text style={{ color: "white", fontSize: 10, fontWeight: "600" }}>Member Detail's</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate('EventList')}>

                        <View style={{ height: 30, width: wp("40%"), borderRadius: 7, justifyContent: "center", backgroundColor: "red", alignItems: "center" }}>
                            <Text style={{ color: "white", fontSize: 10, fontWeight: "600" }}>Upcoming Event's</Text>
                        </View>
                    </TouchableOpacity>

                    
                </View>
               
                <View style={{ flexDirection: "row", alignSelf: "center" }}>
                    <Text style={{ alignSelf: "center", width: wp("80%"), fontSize: 20, marginTop: 20 }}>Trending Post</Text>
                  
                </View>
                <View style={{ marginBottom: 40, marginTop: 10 }}>
                    <FlatList
                        data={trendingPost}
                        renderItem={({ item ,index}) =>
<View>
<ViewShot  ref={setRef(item.id)}
    options={{format: 'jpg', quality: 0.9}}>

                            <View style={{
                                backgroundColor: "white",
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 6,
                                },
                                shadowOpacity: 0.39,
                                shadowRadius: 8.30,

                                elevation: 13,
                                flexDirection: "row", alignSelf: "center", width: wp("90%"),
                                padding: 10,
                                marginTop: 10,
                                marginBottom: 10
                            }}>
                                <Image
                                    source={{ uri: 'http://dilkhushpvtltd.com/bizarmy/public/' + item.image }}
                                    style={{
                                        alignSelf: "center", height: hp('8%'), width: wp("20%"),

                                    }}></Image>
                                <View>
                                    <Text style={{ fontSize: 14, width: wp('59%'), marginLeft: 10 }}>{item.title}</Text>
                                  <View style={{flexDirection:"row"}}>
                                    <Text style={{ fontSize: 14, width: wp('59%'), textAlign: "right", marginTop: 20, color: "#0aa116",right:10 }} onPress={() => goToReadrMore(item)}>READ MORE</Text>
                                   
                    </View>
                                </View>
                            </View>
                            </ViewShot>
                            </View>

                        }

                    />




                </View>



            </ScrollView>



        </View>
    );
};

export default DashBoard;




























// import React, {useEffect, useState} from 'react';
// import {View, Text, Image, ImageBackground} from 'react-native';
// import PATH from '../../../constant/image';
// import styles from './style';
// import * as Utility from '../../../utility';
// import {useDispatch, useSelector} from 'react-redux';
// import {User_Profile} from '../../../Redux/Action';
// import {
//   Refresh_Token,
//   Get_User_Group,
//   Get_Setting_Ventilation,
//   backButtonStatus,
// } from '../../../Redux/Action';
// import Loader from '../../../Components/Loder';
// import useDynamicRefs from 'use-dynamic-refs';
// import I18n from '../../../I18n ';
// import ViewShot from 'react-native-view-shot';
// import RNFS from 'react-native-fs';
// import {captureRef} from 'react-native-view-shot';
// import Share from 'react-native-share';
// const Splash = ({navigation}) => {
//   const dispatch = useDispatch();
//   const [loading, setLoading] = useState(false);
//   const [alertStatus, setAlertStatus] = useState();
//   const [selectedlanguage, setlatestlanguage] = useState('');
//   // console.log('selectedlanguagesplash...', selectedlanguage);
//   const [getRef, setRef] = useDynamicRefs();
//   useEffect(() => {
//     // timeoutHandle = setTimeout(() => {
//     // retrieveData();
//     // }, 1000);
//   }, []);
//   const dataArray = [
//     {
//       name: 'sahil1hgfh',
//       id: 1,
//     },
//     {
//       name: 'sahil2',
//       id: 2,
//     },
//     {
//       name: 'sahil3',
//       id: 3,
//     },
//     {
//       name: 'sahil8',
//       id: 4,
//     },
//   ];
//   ////////AFTER ONE SECOND FUNCATION CALL////
//   const retrieveData = async () => {
//     let token = await Utility.getFromLocalStorge('token');
//     let expryTime = await Utility.getFromLocalStorge('expryTime');

//     console.log('token', token);
//     if (token == null) {
//       navigation.navigate('SignIn');
//     } else {
//       await dispatch(backButtonStatus(false));
//       await dispatch(Refresh_Token());
//       let resp = await dispatch(User_Profile());
//       console.log('profile language', resp.language);
//       // if(resp){
//       setlatestlanguage(resp.language);
//       if (resp.language) {
//         if (resp.language == 'Russian') {
//           I18n.locale = 'rs';
//         }
//         if (resp.language == 'Estonian') {
//           I18n.locale = 'es';
//         }
//         if (resp.language == 'English') {
//           I18n.locale = 'en';
//         }
//       }
//       setLoading(true);

//       let res = await dispatch(Get_User_Group());
//       console.log('check status code', res);

//       if (res == -1) {
//         setLoading(false);

//         navigation.navigate('Welcome');
//         return;
//       } else if (res == 0) {
//         setLoading(false);

//         navigation.navigate('DeviceList');
//         return;
//       } else setLoading(true);

//       let response = await dispatch(Get_Setting_Ventilation());
//       await console.log('check setting api statsus///??/////////', response);
//       await setAlertStatus(response.uiFlags.filter_Alert);
//       if (response.uiFlags.filter_Alert == true) {
//         let userCurrentTime = await Utility.getFromLocalStorge(
//           'currentTimeUser',
//         );
//         console.log(
//           'current time/////////////////////???????????????????????????????',
//           userCurrentTime,
//         );
//         var d = new Date();
//         // d = Mon Feb 29 2016 08:00:09 GMT+0100 (W. Europe Standard Time)
//         var milliseconds = Date.parse(d);
//         // 1456729209000
//         milliseconds = milliseconds - 5  60  1000;
//         // - 5 minutes
//         d = new Date(milliseconds);
//         console.log('console ,mili', d);
//         if (userCurrentTime > d.toISOString()) {
//           console.log(' usrer selected big');
//           setLoading(false);

//           navigation.navigate('DashBoard');
//         } else {
//           setLoading(false);
//           console.log(' current time big');
//           navigation.navigate('FilterAlert');
//         }
//       } else {
//         setLoading(false);
//         navigation.navigate('DashBoard');
//       }

//       return;
//     }
//   };

//   const pic = index => {
//     const id1 = getRef(index);
//     console.log(id1);
//     captureRef(id1, {
//       format: 'jpg',
//       quality: 0.8,
//     }).then(uri => {
//       RNFS.readFile(uri, 'base64').then(res => {
//         let urlString = 'data:image/jpeg;base64,' + res;
//         let options = {
//           title: 'Share Title',
//           message: 'Share Message',
//           url: urlString,
//           type: 'image/jpeg',
//         };
//         Share.open(options)
//           .then(res => {
//             console.log(res);
//           })
//           .catch(err => {
//             err && console.log(err);
//           });
//       });
//     });
//   };
//   return (
//     <View>
//       <Loader isLoading={loading}></Loader>

//       <View style={styles.mainview}>
//         {/* <Image
//           source={PATH.airobot}
//           resizeMode="contain"
//           style={styles.imageview}
//         /> */}
//         {dataArray.map((item, index) => (
//           <View>
//             <ViewShot
//               ref={setRef(item.id)}
//               options={{format: 'jpg', quality: 0.9}}>
//               <Text
//                 style={{marginTop: 30, color: 'red'}}
//                 onPress={() => pic(item.id)}>
//                 {item.name}
//               </Text>
//             </ViewShot>
//           </View>
//         ))}
//       </View>
//     </View>
//   );
// };
// export default Splash;

// // if (new Date().toISOString() < expryTime) {
// //   console.log("check if  expry time", expryTime)

// //   let response = await dispatch(Get_Setting_Ventilation());
// //   await console.log("check setting api statsus///??/////////", response)
// //   await setAlertStatus(response.uiFlags.filter_Alert)
// //   if (response.uiFlags.filter_Alert == true) {
// //     let userCurrentTime = await Utility.getFromLocalStorge('currentTimeUser')
// //     console.log("current time/////////////////////???????????????????????????????", userCurrentTime)
// //     var d = new Date()
// //     // d = Mon Feb 29 2016 08:00:09 GMT+0100 (W. Europe Standard Time)
// //     var milliseconds = Date.parse(d)
// //     // 1456729209000
// //     milliseconds = milliseconds - (5  60  1000)
// //     // - 5 minutes
// //     d = new Date(milliseconds)
// //     console.log("console ,mili", d)
// //     if (userCurrentTime > d.toISOString()) {
// //       console.log(' usrer selected big')
// //       setLoading(true)

// //     }
// //     else {
// //       console.log(' current time big')
// //       navigation.navigate('FilterAlert')

// //     }

// //     // navigation.navigate('FliterAlert')
// //   }
// //   else {
// //     console.log("'api enter in this")
// //     setLoading(true)
// //     let res = await dispatch(Get_User_Group());
// //     console.log('check status code', res);

// //     if (res == -1) {
// //       setLoading(false);

// //       navigation.navigate('Welcome');
// //       return

// //     } else if (res == 0) {
// //       setLoading(false);

// //       navigation.navigate('DeviceList');
// //       return

// //     } else
// //       setLoading(false);

// //     navigation.navigate('DashBoard');
// //     return

// //   }
// // }
// // else {
// //   ////////////////////// HIT REFRESH API TOKEN /////////////////
// //   console.log("check else  expry time refresh api hit??????????????????????????????????????????", expryTime)
// //   let res = await dispatch(Refresh_Token())
// //   if (res == true) {
// //     setLoading(true)
// //     let response = await dispatch(Get_Setting_Ventilation());
// //     await console.log("check setting api statsus///??/////////", response)
// //     await setAlertStatus(response.uiFlags.filter_Alert)
// //     if (response.uiFlags.filter_Alert == true) {
// //       let userCurrentTime = await Utility.getFromLocalStorge('currentTimeUser')
// //       console.log("current time", userCurrentTime)
// //       var d = new Date()
// //       // d = Mon Feb 29 2016 08:00:09 GMT+0100 (W. Europe Standard Time)
// //       var milliseconds = Date.parse(d)
// //       // 1456729209000
// //       milliseconds = milliseconds - (5  60  1000)
// //       // - 5 minutes
// //       d = new Date(milliseconds)
// //       console.log("console ,mili", d)
// //       if (userCurrentTime > d.toISOString()) {
// //         console.log(' usrer selected big')
// //         setLoading(true)
// //         let res = await dispatch(Get_User_Group());
// //         console.log('check status code', res);

// //         if (res == -1) {
// //           setLoading(false);

// //           navigation.navigate('Welcome');
// //           return

// //         } else if (res == 0) {
// //           setLoading(false);

// //           navigation.navigate('DeviceList');
// //           return

// //         } else
// //           setLoading(false);

// //         navigation.navigate('DashBoard');
// //         return

// //       }
// //       else {
// //         console.log(' current time big')
// //         navigation.navigate('FilterAlert')

// //       }

// //       // navigation.navigate('FliterAlert')
// //     }
// //     else {
// //       console.log("'api enter in this????")

// //       setLoading(true)
// //       let res = await dispatch(Get_User_Group());
// //       console.log('check status code', res);

// //       if (res == -1) {
// //         setLoading(false);

// //         navigation.navigate('Welcome');
// //         return

// //       } else if (res == 0) {
// //         setLoading(false);

// //         navigation.navigate('DeviceList');
// //         return

// //       } else
// //         setLoading(false);

// //       navigation.navigate('DashBoard');
// //       return

// //     }

// //   }
// //   else {
// //     setLoading(false)
// //   }
// // }
// //     }
