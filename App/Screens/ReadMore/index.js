import React, { useEffect ,useState} from 'react';
import { View, Text, ImageBackground,BackHandler, Image, ScrollView, TouchableOpacity,useWindowDimensions } from 'react-native';
import MainHeader from '../../Components/MainHeader';
import HTML from "react-native-render-html";
import { WebView } from 'react-native-webview';
import ViewShot from 'react-native-view-shot';
import {captureScreen} from 'react-native-view-shot'
import Share from "react-native-share"
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import RNFS from 'react-native-fs';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../Utility"
const CommiteMember = ({ route, navigation }) => {
    const { ReadMoreData } = route.params
    const [showPage,setShowPage]=useState(false)
    console.log("check read more data", ReadMoreData)
    const contentWidth = useWindowDimensions().width;
    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);

  const   sharePost = () => {
    //     console.log(id1);
    captureScreen({
        format: "jpg",
        quality: 0.8
      }).then(
      uri => {
        RNFS.readFile(uri, 'base64').then((res) => {
          let urlString = 'data:image/jpeg;base64,' + res;
          let options = {
            title: 'Share Title',
            message:ReadMoreData.title+ '\n'+ 'http://dilkhushpvtltd.com/bizarmy/public/' + ReadMoreData.image,
  
            url: urlString,
            type: 'image/jpeg',
          };
          Share.open(options)
            .then((res) => {
              console.log(res);
            })
            .catch((err) => {
              err && console.log(err);
            });
        });
      },
      error => console.error("Oops, snapshot failed", error)
    );
    };
    return (

        <View style={{flex:1}}>
            <MainHeader navigation={navigation} showBack={true} title={'Trending Post'}></MainHeader>
           {showPage==false?
            <ScrollView>
                 <TouchableOpacity onPress={() => sharePost()}>
                        <EvilIcons
                            style={{ marginTop: 20 ,marginRight:20,alignSelf:'flex-end'}}
                            name="share-google"
                            size={30}
                            color="black"
                        />
                    </TouchableOpacity>
                <Image
                    source={{ uri: 'http://dilkhushpvtltd.com/bizarmy/public/' + ReadMoreData.image }}
                    style={{
                        alignSelf: "center", height: hp('22%'), width: wp("70%"),
                        marginTop: 20

                    }}></Image>
              
              <View style={{padding:20}}>
                <HTML source={{ html:ReadMoreData.desc }} contentWidth={contentWidth}  />
                </View>
            {ReadMoreData.url!==''?
                <Text style={{ fontSize: 14, padding: 20, alignSelf: 'flex-end',color:'blue',textDecorationLine:"underline" }} onPress={()=>setShowPage(true)}>{'View More'}</Text>
:null}
            </ScrollView>:
            <View style={{flex:1}}>
                 <WebView source={{ uri: ReadMoreData.url }} />
                </View>

            }
           
        </View>

    );
};

export default CommiteMember;

