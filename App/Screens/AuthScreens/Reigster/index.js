import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, TouchableOpacity,BackHandler, Image, ScrollView,ToastAndroid } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import Icon from 'react-native-vector-icons/FontAwesome';
import AntiDesign from "react-native-vector-icons/AntDesign"
import * as Utility from "../../../Utility/index"

import { Input, CheckBox, } from 'react-native-elements';
import styles from "./style"
import Button from "../../../Constants/Button"
import { Picker } from '@react-native-community/picker';
import { GetBusinessCategory, GetBridageCategory, UserCreate } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
import { Alert } from 'react-native';
const Register = ({ navigation }) => {
    const [selectedValue, setSelectedValue] = useState("");
    const [selectedBridage, setSelectedBridage] = useState("");
    const [isChecked, setIsChecked] = useState(false)
    const dispatch = useDispatch()
    const [bussinessCategory, setBusniessCategory] = useState([])
    const [bridageCategory, setBridageCategory] = useState([])
    const [mobileNo, setMobileNo] = useState('')
    const [userName, setUserName] = useState('')
    const [frimName, setFrimName] = useState('')
    const [email, setEmail] = useState('')
    const [loading, setLoading] = useState(false)

    useEffect(() => {

        getDropDownData()
    }, []);
    const getDropDownData = async () => {

        let res = await dispatch(GetBusinessCategory())
        await setBusniessCategory(res.data)
        let response = await dispatch(GetBridageCategory())
        await setBridageCategory(response.data)
    }

    const signUpFun = async () => {
        if (await Utility.isFieldEmpty(mobileNo && userName   && frimName && email)) {
        ToastAndroid.show("Please fill all fields.", ToastAndroid.showWithGravityAndOffset);
            
            return
            
            // Alert.alert("Please fill all fields")
        }
        else if (isChecked == false) {
        ToastAndroid.show("Please accept term and conditions.", ToastAndroid.showWithGravityAndOffset);

            return
            //  Alert.alert('PLease accept term and conditions')
        }
        else if (mobileNo.length < 10) {
        ToastAndroid.show("Please fill  valid mobileNo.", ToastAndroid.showWithGravityAndOffset);
            
            return 
            // Alert.alert("please fill valid mobile no")
        }
        else if (await Utility.isValidEmail(email)) {
        ToastAndroid.show("Please fill valid email.", ToastAndroid.showWithGravityAndOffset);

            return 
            // Alert.alert('PLease fill valid email')
        }
        else {
            setLoading(true)

            let res = await dispatch(UserCreate(mobileNo, userName, email, selectedValue, frimName))

            if (res.status == true) {
                setLoading(false)
                Alert.alert(
                    '',
                    'User Register Sucessfully',
                    [{ text: 'Ok', onPress: () => navigation.navigate('Login') }],
                    {
                        cancelable: false,
                    },
                );
                return true;
            }
            else {
                setLoading(false)
                ToastAndroid.show(res.message, ToastAndroid.showWithGravityAndOffset);

                return
                //  Alert.alert(res.message)
            }
        }
    }
    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);
    return (

        <View>
            <Loader isLoading={loading}></Loader>

            <ScrollView>
                <View style={styles.mainView}>
                    <Text style={styles.registrationTxt}>Registration</Text>
                    <Text style={styles.subTitleTxt}>Please fill up the detail ask below</Text>

                    <Text style={styles.inputTitle}>Mobile Number</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            placeholder={"Enter Mobile No"}
                            keyboardType={'number-pad'}
                            onChangeText={(text) => setMobileNo(text)}
                            leftIcon={
                                <Icon
                                    name='phone'
                                    size={20}
                                    color='black'
                                />
                            }
                        />
                    </View>

                    <Text style={styles.inputTitle}>Name</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            placeholder={"Enter Full Name"}
                            onChangeText={(text) => setUserName(text)}

                            leftIcon={
                                <AntiDesign
                                    name='user'
                                    size={20}
                                    color='black'
                                />
                            }
                        />
                    </View>

                    <Text style={styles.inputTitle}>Email</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            placeholder={"Enter Email "}
                            onChangeText={(text) => setEmail(text)}
                            leftIcon={
                                <Icon
                                    name='envelope'
                                    size={17}
                                    color='black'
                                />
                            }
                        // rightIcon={
                        //     <AntiDesign
                        //         name='eyeo'
                        //         size={20}
                        //         color='black'
                        //     />
                        // }
                        />
                    </View>


                    <Text style={styles.inputTitle}>Business Category </Text>
                    <View style={{ flexDirection: "row", width: wp('80%'), alignSelf: 'center', }}>



                        {/* <View style={{ flex: 1 }}>
                            <Picker
                                selectedValue={selectedValue}
                                mode={"dropdown"}
                                style={{ height: 50, width: wp('70%') }}
                                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                            >
                                <Picker.Item label="Selected Categroy" value="" />

                                {bussinessCategory.map((item) => (
                                    <Picker.Item label={item.category_name} value={item.category_name} />

                                ))}
                            </Picker>
                        </View> */}
 <View style={styles.inputMainView}>
                        <Input
                            placeholder={'Enter Category '}
                            onChangeText={(text) => setSelectedValue(text)}
keyboardType={'number-pad'}
                            leftIcon={
                                <Image source={require("../../../Assets/Icons/category.png")} style={{ height: 20, width: 20, }} resizeMode={"contain"}></Image>


                            }
                        />
                    </View>
                        
                    </View>

                


                    {/* <Text style={styles.inputTitle}>Selecte Brigade </Text>
                    <View style={{ flexDirection: "row", width: wp('80%'), alignSelf: 'center', }}>


                        <Image source={require("../../../Assets/Icons/Brigade.png")} style={{ height: 20, width: 20, marginTop: hp('2.2%') }} resizeMode={"contain"}></Image>


                        <View style={{ flex: 1 }}>
                            <Picker
                                selectedValue={selectedBridage}
                                mode={"dropdown"}
                                style={{ height: 50, width: wp('70%') }}
                                onValueChange={(itemValue, itemIndex) => setSelectedBridage(itemValue)}
                            >
                                <Picker.Item label="Selected Brigade" value="" />

                                {bridageCategory.map((item) => (
                                    <Picker.Item label={item.title} value={item.title} />

                                ))}
                            </Picker>
                        </View>
                    </View>

                    <View style={{ borderBottomColor: "black", backgroundColor: "#dfdfdf", borderBottomWidth: 1, width: "74%", marginBottom: 20, alignSelf: "center", marginRight: 20 }}> */}

                    {/* </View> */}






                    <Text style={styles.inputTitle}>Firm Name</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            placeholder={'Enter Firm Name'}
                            onChangeText={(text) => setFrimName(text)}

                            leftIcon={

                                <Image source={require("../../../Assets/Icons/Briefcase.png")} style={{ height: 20, width: 20, }} resizeMode={"contain"}></Image>

                            }
                        />
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: "flex-start", width: wp('80%'), alignSelf: "center", bottom: 10, marginRight: wp('10%'), }}>
                        <CheckBox
                            checked={isChecked}
                            checkedColor="black"
                            size={wp('5.5%', hp('4%'))}
                            onPress={() => setIsChecked(!isChecked)}
                        />
                        <Text style={{ marginTop: 10, fontSize: 12, color: "grey", width: wp('70%'), }}>{'By Creating an account are you agree to our\n term and condition'}</Text>


                    </View>

                    <Button title={"Submit"} onPress={() => signUpFun()}></Button>


                    <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                        <View style={{ flexDirection: "row", alignSelf: "center", marginTop: 20, marginBottom: 20 }}>
                            <Text style={{ fontSize: 14, color: "grey", }}>{'Already have an account?'}</Text>
                            <Text style={{ fontSize: 14, }}>{'Sign In'}</Text>

                        </View>
                    </TouchableOpacity>
                </View>

            </ScrollView>
        </View>
    );
};

export default Register;

