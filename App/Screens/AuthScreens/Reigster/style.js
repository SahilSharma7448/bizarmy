import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../../../Utility/index';

const styles = StyleSheet.create({
    mainView: { marginTop: hp('7%'), },
    registrationTxt: { fontSize: 30, fontWeight: '300', width: wp('80%'), alignSelf: 'center', },
    subTitleTxt: { fontSize: 10, color: "grey", marginTop: 7, marginBottom: hp('7%'), width: wp('80%'), alignSelf: 'center', },
    inputTitle: { fontSize: 10, color: "grey", width: wp('80%'), alignSelf: 'center', },
    inputMainView: { width: wp('80%'), alignSelf: 'center', right: 10 },



    placeholder: { fontSize: wp('4%'), right: 1 },
    heightin: { height: 49 },
    drop: {
        backgroundColor: '#ffffff00',
        right: wp('4%'),
        borderColor: '#ffffff00',
        borderWidth: 2,
        width: wp('70%'),
        alignSelf: 'center',
    },
    droptext:
        { borderColor: "#ffffff00", alignSelf: 'center', width: "70%", justifyContent: "center", right: wp('18%') }

});

export default styles;