import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
import * as Utility from "../../../Utility"
import SyncStorage from 'sync-storage';

const Splash = ({ navigation }) => {
  useEffect(() => {

    timeoutHandle = setTimeout(() => {
      retrieveData();
    }, 2000);
  });
  const retrieveData = async () => {
let userId=await Utility.getFromLocalStorge('userId')
let user=await Utility.getFromLocalStorge('name')
let roleId=await Utility.getFromLocalStorge('roleId')
console.log("check role id",roleId)
SyncStorage.set('user',  user);

if(userId!==null&& roleId!=="3"){


  navigation.navigate("SideDrawer")

}
else{
    navigation.navigate("Login")
}

  }
  return (

    <View>
      <View style={{ backgroundColor: "#990000", height: hp('100%'), width: wp('100%'), alignSelf: "center", alignItems: "center", justifyContent: "center" }}>


        <Image source={require("../../../Assets/Icons/Biz-Army-LOGO-White.png")} style={{ width: wp('80%'), height: wp('20%'), alignSelf: "center", }} resizeMode={"contain"}></Image>
      </View>
    </View>
  );
};

export default Splash;

