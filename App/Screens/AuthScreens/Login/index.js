import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, TouchableOpacity,BackHandler, Image, ScrollView,ToastAndroid, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input, CheckBox, } from 'react-native-elements';
import styles from "./style"
import Button from "../../../Constants/Button"
import * as Utility from "../../../Utility/index"
import { MobileLogin } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
import PushNotification from 'react-native-push-notification'
import messaging from '@react-native-firebase/messaging';
import DeviceInfo from 'react-native-device-info';
const Login = ({ navigation }) => {
    const dispatch = useDispatch()
    const [mobileNo, setMobileNo] = useState('')
    const [loading, setLoading] = useState(false)

    const loginFun = async () => {
        if (await Utility.isFieldEmpty(mobileNo)) {
        ToastAndroid.show("Please fill mobileNo.", ToastAndroid.showWithGravityAndOffset);

            return 
        }
        else if (await mobileNo.length < 10) {
        ToastAndroid.show("Please fill  valid mobileNo.", ToastAndroid.showWithGravityAndOffset);

            return 
            
            // Alert.alert('Please fill  valid mobileNo.')

        }
        else {
          let deviceId = DeviceInfo.getDeviceId();

let uniqueId = DeviceInfo.getUniqueId();
console.log("check token>>>>>>>>>>>>>>>",uniqueId)

       
            setLoading(true)
            let res = await dispatch(MobileLogin(mobileNo,uniqueId))
            if (res.status == true) {
                setLoading(false)

                navigation.navigate('ForgotPassword', {
                    phoneNo: mobileNo,
                })
            }
            else {
                setLoading(false)
                ToastAndroid.show(res.message, ToastAndroid.showWithGravityAndOffset);

                return 
                // Alert.alert(res.message)
            }

        }
    }
    const handlePress=()=>{
        // navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);









    useEffect(() => {
        // Update the document title using the browser API
        checkPermission();
        getToken();
      });
      PushNotification.configure({
        // (optional) Called when Token is generated (iOS and Android)
        onRegister: function (token) {
          console.log('TOKEN:', token);
        },
    
        // (required) Called when a remote is received or opened, or local notification is opened
        onNotification: function (notification) {
          console.log('NOTIFICATION:', notification);
          if (notification.foreground) {
            PushNotification.localNotification(notification);
          }
          // alert(notification.data.type);
          // process the notification
    
          // (required) Called when a remote is received or opened, or local notification is opened
          // notification.finish(PushNotificationIOS.FetchResult.NoData);
        },
    
        // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
        onAction: function (notification) {
          console.log('ACTION:', notification.action);
          console.log('NOTIFICATION:', notification);
    
          // process the action
        },
    
        // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
        onRegistrationError: function (err) {
          console.error(err.message, err);
        },
    
        // IOS ONLY (optional): default: all - Permissions to register.
        permissions: {
          alert: true,
          badge: true,
          sound: true,
        },
    
        popInitialNotification: true,
    
        requestPermissions: true,
      });
    
      //REmot notification permission request
      const checkPermission = async () => {
        console.log('check permission function call');
        const enabled = await messaging().hasPermission();
        console.log('check permission function call enable', enabled);
        if (enabled) {
          getToken();
        } else {
          requestPermission();
        }
      };
      ///remote notification token
      const getToken = async () => {
        console.log('get token call');
        // let fcmToken =await AsyncStorage.read('fcmTocken');
        // console.log("get token call fcm token",fcmToken)
        // if(!fcmToken){
    let    fcmToken = await messaging().getToken();
        // if(fcmToken){
        console.log('check fcm token', fcmToken);
        // await AsyncStorage.save('fcmToken',fcmToken);
        // }
        // }
      };
    
      ////remote location message user permission
      const requestPermission = async () => {
        console.log('requestPermission call');
        try {
          await messaging().requestPermission();
          //user has autherised
          getToken();
        } catch (error) {}
      };
    return (

        <View>
            <Loader isLoading={loading}></Loader>
            <ScrollView>
                <View style={styles.mainView}>
                    <Text style={styles.registrationTxt}>Login</Text>
                    <Text style={styles.subTitleTxt}>Enter your phone number to proceed</Text>

                    <Text style={styles.inputTitle}>Mobile Number</Text>
                    <View style={styles.inputMainView}>
                        <Input
                            placeholder={"Enter Mobile No"}
                            keyboardType={'number-pad'}
                            onChangeText={(text) => setMobileNo(text)}
                            leftIcon={
                                <Icon
                                    name='phone'
                                    size={20}
                                    color='black'
                                />
                            }
                        />
                    </View>


                    <Button title={"Get OTP"} onPress={() => loginFun()}></Button>


                    <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                        <View style={{ flexDirection: "row", alignSelf: "center", marginTop: 20, marginBottom: 20 }}>
                            <Text style={{ fontSize: 14, color: "grey", }}>{'Create an account?'}</Text>
                            <Text style={{ fontSize: 14, textDecorationLine: "underline" }} >{'Sign Up'}</Text>

                        </View>
                    </TouchableOpacity>
                </View>


            </ScrollView>
        </View>
    );
};

export default Login;

