import { Dimensions, StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../Utility"
const styles = StyleSheet.create({

    icon: { marginTop: "4%", marginLeft: "5%" },
    enterView: { alignSelf: "center", marginTop: hp("5%"), },
    enterText: { fontSize: 35, },
    pleaseView: { marginTop: hp("5%"), width: wp('80%') },
    pleaseText: { fontSize: 12, color: "#7a7976" },
    mobileText: { fontSize: 15, color: "#7a7976" },
    inputContainer: {
        borderBottomWidth: 1, borderBottomColor: "black", flexDirection: "row", width: wp("18%"), alignItems: "center", alignSelf: "center",
        justifyContent: "space-evenly"
    },
    otpStyle: { flexDirection: "row", alignItems: "center", marginTop: hp("5%") },


});
export default styles;