import React, { useState,useEffect } from 'react'
import { View, Text, TouchableOpacity,BackHandler, Alert,ToastAndroid } from 'react-native'
import OtpInputs from 'react-native-otp-inputs'
import Icon from 'react-native-vector-icons/Entypo'
import styles from "./style"
import Button from "../../../Constants/Button"
import { LoginApi } from "../../../Redux/Action"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../../../Constants/Loader"
import * as Utility from "../../../Utility"
import DeviceInfo from 'react-native-device-info';

const ForgotPassword = ({ route, navigation }) => {
    const { phoneNo, } = route.params;
    const dispatch = useDispatch()

    const [otp, setOtp] = useState('')
    const [loading, setLoading] = useState(false)

    const loginFun = async () => {

        console.log("check otp", otp)
        if (otp.length < 4) {
        ToastAndroid.show("Please fill otp field.", ToastAndroid.showWithGravityAndOffset);

            return
            //  Alert.alert("Please fill otp field")
        }
        else {
            let uniqueId = DeviceInfo.getUniqueId();
console.log("check token>>>>>>>>>>>>>>>",uniqueId)
            setLoading(true)
            let res = await dispatch(LoginApi(phoneNo, otp,uniqueId))
            if (res.status == true) {
                setLoading(false)
                let roleId=await Utility.getFromLocalStorge('roleId')
                if(roleId==3){
                navigation.navigate('GuestUser')

                }
                else{
                navigation.navigate('SideDrawer')
                }

            }
            else {
                setLoading(false)
                ToastAndroid.show(res.message, ToastAndroid.showWithGravityAndOffset);

                return 
                // Alert.alert(res.message)
            }
        }
    }
    const handlePress=()=>{
        navigation.goBack()
        return true
    }
    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', handlePress);
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handlePress);
      };
    }, []);
    return (
        <View>
            <Loader isLoading={loading}></Loader>

            <TouchableOpacity onPress={() => navigation.goBack()}>
                <Icon
                    style={styles.icon}
                    name="arrow-long-left"
                    size={30}
                    color="black"
                />
            </TouchableOpacity>
            <View style={styles.enterView}>
                <Text style={styles.enterText}>Enter OTP</Text>


                <View style={styles.pleaseView}>
                    <Text style={styles.pleaseText}>Please Enter the One Time Password Sent to your registered Email ID</Text>
                </View>

                <View>
                    <OtpInputs
                        placeholder={""}
                        handleChange={(code) => setOtp(code)}
                        numberOfInputs={4}
                        inputStyles={{ color: "black", textAlign: "center", fontWeight: "800", fontSize: 20 }}
                        inputContainerStyles={{ width: 57, marginLeft: 20 }}
                        underlineColorAndroid={"black"}
                        style={{ flexDirection: "row", alignSelf: 'flex-start', alignItems: 'flex-start', justifyContent: "space-between", marginTop: 40, right: 20 }}
                    />

                </View>
                <Button title={"Submit"} onPress={() => loginFun()}></Button>
            </View>
        </View>
    )
}
export default ForgotPassword


