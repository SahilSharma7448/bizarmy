import React, { Component } from 'react'

import { StyleSheet, View, Image } from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../Utility';
const Logo = ({ navigation, isLoading }) => {

    return (
        <View style={styles.backGroundView}>
            <Image
                source={require('../Assets/logoNew.png')}
                style={styles.logoImg}
                resizeMode={'contain'}
            ></Image>
        </View>
    )
}
export default Logo
const styles = StyleSheet.create({
    backGroundView: {
        height: hp('30%'),
        width: wp('100%'),
        backgroundColor: 'black',
        alignItems: 'center',
    },
    logoImg: { height: hp('20%'), width: wp('59%'), marginTop: 30 },
});



