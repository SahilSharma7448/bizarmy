import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../Utility/index"
import Icon from 'react-native-vector-icons/Entypo'

const Button = ({ navigation, onPress, title }) => {

    return (

        <View>
            <TouchableOpacity onPress={onPress} activeOpacity={1.1}>
                <View style={styles.submitView}>
                    <Text style={styles.submitText}>{title}</Text>
                    <Icon
                        style={styles.iconnn}
                        name="login"
                        size={18}
                        color="white"
                    />
                </View>
            </TouchableOpacity>
        </View>
    );
};

export default Button;

const styles = StyleSheet.create({
    submitView: {
        borderWidth: 1, backgroundColor: "#0aa116", flexDirection: "row", justifyContent: "space-between",
        marginTop: hp("5%"), width: wp("80%"), height: hp("7%"), borderColor: "#0aa116", alignSelf: "center", alignItems: "center"
    },
    submitText: { color: "white", fontSize: 17, textAlign: "center", marginLeft: wp("4%") },
    iconnn: { marginRight: wp("4%") },
});