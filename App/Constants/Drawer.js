import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image, StyleSheet, TouchableOpacity, FlatList,Alert, } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../Utility/index"
import Icon from 'react-native-vector-icons/Entypo'
import * as Utility from "../Utility"
import AsyncStorage from '@react-native-community/async-storage';
import SyncStorage from 'sync-storage';

const Drawer = ({ navigation, onPress, title, showBack }) => {
    const screenName = [
        { name: 'Home', route: "" },
        { name: 'My Profile', route: "MyProfile" },

        { name: 'Member Details', route: "MemberDetail" },
        { name: 'Committee Members', route: "CommiteMember" },
        { name: 'My Membership', route: "MemberShip" },
        { name: 'Strong Connects', route: "StrongList" },
        { name: 'Success Stories', route: "SuccessList" },
        { name: 'Events', route: "EventList" },
        { name: 'Scheme', route: "SchemeList" },

        { name: 'Notifications', route: "Notification" },
        { name: 'About us', route: "AboutUs" },
        { name: 'Help', route: "" },
        { name: 'Logout', route: "Logout" },

    ]
    const goToRoute =async (item) => {
        if(item.route=='Logout'){
Alert.alert(
    '',
    'Are you sure Logout',
    [
        { text: 'No', onPress: () => { }, style: 'cancel' },
        { text: 'Yes', onPress: () => removeAuth() },
    ],
    {
        cancelable: false,
    },
);
return true;
        }
        else{
        navigation.navigate(item.route)
        }
    }
    const removeAuth=async()=>{
await Utility.removeAuthKey('userId')
navigation.navigate('Login')
    }
//     useEffect(async() => {

//         // timeoutHandle = setTimeout(() => {
//         //   retrieveData();
//         // }, 2000);
//         // let Username =await Utility.getFromLocalStorge('name')
// // setName(Username)
// name=Username
//       });

const nameFun=()=>{
    // let userName=await Utility.getFromLocalStorge('name')
    const result = SyncStorage.get('user');
console.log(result,"????????????????????????////"); // 'bar'
console.log(SyncStorage.getAllKeys())
return   <View style={{ height: 49, backgroundColor: "#0aa116", paddingLeft: 20, justifyContent: "center" }}>
<Text style={{ fontSize: 14, fontStyle: "italic", color: "white", fontWeight: "700" }}>Hello! {result}</Text>
</View>
}
    return (

        <View style={{ flex: 1 }}>
         {nameFun()}
            <FlatList
                data={screenName}
                renderItem={({ item }) =>
                    <Text style={{ fontSize: 20, color: "black", fontWeight: "700", marginTop: 30, paddingLeft: 20 }} onPress={() => goToRoute(item)}>{item.name}</Text>
                }

            />
        </View>

    );
};

export default Drawer;

const styles = StyleSheet.create({

});