import React, { Component } from 'react'
import Spinner from 'react-native-loading-spinner-overlay';

import { StyleSheet } from 'react-native';
const Loader = ({ navigation, isLoading }) => {

    return (
        <Spinner
            allowFontScaling={false}
            visible={isLoading}
            textContent={`Please Wait`}
            textStyle={styles.spinnerTextStyle}
            animation="slide"
            color={"#0aa116"}
            textStyle={{ color: "white", fontSize: 20 }}
        />
    )
}
export default Loader
const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: "green"
    }
});



