import { Alert } from 'react-native';
import * as Services from '../api/services';
import * as Url from '../api/Url';
import * as Const from './Const';
import * as Utility from "../Utility/index"
import { registerCustomIconType } from 'react-native-elements';
export const REGISTER_DATA = 'REGISTER_DATA';
import AsyncStorage from '@react-native-community/async-storage';
import SyncStorage from 'sync-storage';
export const LoginApi = (mobileNo, otp,deviceId) => async () => {
    let body = {
        mobile_no: mobileNo,
        otp: otp,
        device_key:deviceId

    }
    let res = await Services.postApi(Url.LOGIN, "", body)
    console.log("check login otp", res)
    if (res.message == 'Login Successfully') {
        await Utility.setInLocalStorge('userId', res.data.id)
        await Utility.setInLocalStorge('name', res.data.name)
        await AsyncStorage.setItem('myname', res.data.name);
        await Utility.setInLocalStorge('roleId', res.data.role_id)
await Utility.setInLocalStorge('brigade_id',res.data.brigade)
SyncStorage.set('user',  res.data.name);

        return res;

    }
    return res;

}


export const MobileLogin = (mobileNo,deviceId) => async () => {
    let body = {
        mobile_no: mobileNo,
        // otp: 1234
        device_key:deviceId

    }
    console.log("check body???", body)
    let res = await Services.postApi(Url.LOGIN, "", body)
    console.log("check response????????????", res);
    if (res.message == 'Login Successfully') {
        await Utility.setInLocalStorge('userId', res.data.id)
        await Utility.setInLocalStorge('name', res.data.name)
        await Utility.setInLocalStorge('roleId', res.data.role_id)

        await AsyncStorage.setItem('myname', res.data.name);
        SyncStorage.set('user',  res.data.name);
        await Utility.setInLocalStorge('brigade_id',res.data.brigade)

        return res;

    }
    return res
}
export const GetBusinessCategory = () => async () => {
    let res = await Services.postApi(Url.BUSINESSCAT, "")
    return res;
}

export const GetBridageCategory = () => async () => {
    let res = await Services.postApi(Url.BRIDAGECAT, "")
    console.log("check business categoryt", res)
    return res;
}
export const UserCreate = (mobileNo, userName, email, selectedValue, frimName) => async () => {
    let body = {

        name: userName,
        email: email,
        business_name: frimName,
        // brigade: selectedBridage,
        category: selectedValue,
        mobile_no: mobileNo,

    }
    console.log("chek body", body)
    let res = await Services.postApi(Url.USERCREATE, '', body)
    console.log("check sign up respone", res)
    return res
}

export const PostPDRecords = (formData) => async () => {
    let res = await Services.FormDataPost(Url.SAVEPDRECORD, "", formData)
    console.log("check fomdata response", res)
    return res
}
export const BannerApi = () => async () => {
    let res = await Services.postApi(Url.BANNER, "",)
    console.log("check response", res)
    return res;
}
export const GetMyBridage = () => async () => {
    let userId = await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId
    }
    let res = await Services.postApi(Url.MYBRIDAGE, "", body)
    console.log("check response og my brigade", res)
    return res;
}

export const RecommendationSave = (givenName, cat, name, approx, detail, status, selectedDate) => async () => {
    let userId = await Utility.getFromLocalStorge('userId')
    let body = {
        given_from_id: parseInt(userId),
        given_to_id: givenName,
        business_cat_id: cat,
        customer_name: name,
        business_detail: detail,
        approx_value: approx,
        status: status,
        date: selectedDate

    }
    console.log("check body", body)
    let res = await Services.postApi(Url.RECOMMENDATIONSAVE, "", body)
    console.log("check response of reccomendation", res)
    return res;
}
export const TrendingPostApi = () => async () => {
    let res = await Services.postApi(Url.TRENDINGPOST)
    console.log('check response of trendaing post', res)
    return res
}
export const PdListRecordApi = () => async () => {
    let userId = await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId
    }
    let res = await Services.postApi(Url.LISTPDRECORD, "", body)
    console.log("check response og my brigade", res)
    return res;
}
export const Badges_api = () => async () => {
    let userId = await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId
    }
    let res = await Services.postApi(Url.BAGES, "", body)
    console.log("check response og my brigade", res)
    return res;
}
export const RecommendationListApi = () => async () => {
    let userId = await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId
    }
    let res = await Services.postApi(Url.RECOMNEATIONLIST, "", body)
    console.log("check response og my brigade", res)
    return res;
}
export const MyDashboardData = () => async () => {
    let userId = await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId
    }
    let res = await Services.postApi(Url.MYDASHBOARD, "", body)
    console.log("check response of dashboard data", res)
    return res;
}
export const AttendanceList = () => async () => {
    let userId = await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId
    }
    let res = await Services.postApi(Url.ATTENDENCELIST, "", body)
    console.log("check response of dashboard data", res)
    return res;
}
export const BusinessBookedList = () => async () => {
    let userId = await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId
    }
    let res = await Services.postApi(Url.BUSINESSBOOKED, "", body)
    console.log("check response of dashboard data", res)
    return res;
}
export const MyMemberShip = () => async () => {
    let userId = await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId
    }
    let res = await Services.postApi(Url.MEMBERSHIP, "", body)
    console.log("check response of dashboard data", res)
    return res;
}
export const EditRecomendationForm = (detail, value, status, selectedDate, deafault, id) => async () => {
    let body = {
        business_detail: detail,
        approx_value: value,
        status: status || deafault,
        date: selectedDate
    }
    console.log("check body", body)
    let res = await Services.postApi(Url.EDITRECOMMENDATION + id, "", body)
    console.log("check response of dashboard data", res)
    return res;
}
export const Add_Business_Mark=(id,givenTo,giveFrom,amt,selectedDate)=>async()=>{
    let body=

        {
            recommendation_id:id,
            given_from_id:giveFrom,
            given_to_id:givenTo,
            business_amt:amt,
            booking_date:selectedDate
        
    }
    console.log("chevck  body of business mark",body)
    let res=await Services.postApi(Url.MARK_BOOKED,'',body)
    console.log('check res',res)
    return res;
}
export const Mark_Cancel=(id)=>async()=>{
let body={
    recommedation_id:id,
    status:"CANCELLED"
}
console.log("check body",body)
let res=await Services.postApi(Url.MARK_CANCEL,'',body)
return res;
}
export const GetInvoice=()=>async()=>{
    let userId = await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId
    }
    let res = await Services.postApi(Url.GET_INVOICE, "", body)
    console.log("check response of dashboard data", res)
    return res;
}

export const GetMyProfile=()=>async()=>{
    let userId = await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId
    }
    let res = await Services.postApi(Url.GET_MY_PROFILE, "", body)
    console.log("check response of dashboard data", res)
    return res;
}
export const Header_Status=(value)=>(dispatch)=>{
    dispatch({
        type: Const.SHOW_HEADER,
        payload: value,
    });
}
export const Update_profile=(name,email,phone,adress1,adress2,adress3,bio)=>async()=>{
    let userId = await Utility.getFromLocalStorge('userId')

    let body={
user_id:userId,
name:name,
email:email,
mobile_no:phone,
address1:adress1,
address2:adress2,
address3:adress3,
user_bio:bio

    }
    console.log("check body",body)
    let res=await Services.postApi(Url.UPDATE_PROFILE,'',body)
    console.log("check messge",res)
    return res
}

export const Commite_Member_Api=()=>async()=>{
    let res=await Services.postApi(Url.COMMITE_MEMBER,'')
    console.log("check messge",res)
    return res

}

export const Get_Notification_List=()=>async()=>{
    let res=await Services.postApi(Url.NOTIFICATION_LIST,'')
    console.log("check messge",res)
    return res
}

export const Paymet_Transtion=(id,amt,recipt)=>async()=>{
    let userId = await Utility.getFromLocalStorge('userId')

let body ={
    booking_id:id,
    user_id:userId,
    amount:amt,
    transaction_id:recipt,
    payment_status:1

}
console.log("chekc response of payment",body)
let res=await Services.postApi(Url.PAYMENT_URL,'',body)
// 
console.log("check status of api payment",res)
return res

}

export const Current_Recommendation_Status=(value)=>async(dispatch)=>{
    dispatch({
        type: Const.CURRENT_R_STATUS,
        payload: value,
    });
}
export const ModalStatus=(value)=>async(dispatch)=>{
    dispatch({
        type: Const.MODAL_STATUS,
        payload: value,
    });
}
export const Current_Business_Status=(value)=>async(dispatch)=>{
    dispatch({
        type: Const.BUSSINESS_STATUS,
        payload: value,
    });
}
export const ModalStatusBusiness=(value)=>async(dispatch)=>{
    dispatch({
        type: Const.MODAL_STATUS_BUSINESS,
        payload: value,
    });
}

export const AttendenceStatus=(value)=>async(dispatch)=>{
    dispatch({
        type: Const.ATTENDENCE_STATUS,
        payload: value,
    });
}

export const BackStatus=(value)=>async(dispatch)=>{
    dispatch({
        type: Const.BACK_STATUS,
        payload: value,
    });
}
export const Create_Scheme=(name,start,end,des)=>async(dispatch)=>{
    let userId=await Utility.getFromLocalStorge('userId')
let body={
    user_id:userId,
    scheme_name:name,
    scheme_starting_date:start,
    scheme_ending_date:end,
    scheme_description:des,

}
console.log("check body",body)
let res=await Services.postApi(Url.CREATE_SCHEME,'',body)
console.log("check res",res)
if(res.status==true){
    return true
}
else {
    return false
}
}

export const Create_stromg_connect=(name,no,cat,firm,email)=>async(dispatch)=>{
    let userId=await Utility.getFromLocalStorge('userId')
let body ={
    user_id:userId,
    visitor_name:name,
    mobile_no:no,
    category_id:cat,
    firm_name:firm,
    email:email,

}
console.log("check body",body)
let res=await Services.postApi(Url.CREATE_CONNECTS,'',body)
if(res.status==true){
    return true
}
else{
    return false
}
}

export const Create_Visitor=(name,no,cat,firm,email)=>async(dispatch)=>{
    let userId=await Utility.getFromLocalStorge('userId')
let body ={
    user_id:userId,
    visitor_name:name,
    mobile_no:no,
    category_id:cat,
    firm_name:firm,
    email:email,

}
console.log("check body",body)
let res=await Services.postApi(Url.CREATE_VISITOR,'',body)
if(res.status==true){
    return true
}
else{
    return false
}
}

export const Get_Connects=()=>async()=>{
    let userId=await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId
    }
    let res = await Services.postApi(Url.GET_CONNECTS, "", body)
    console.log("check response of dashboard data", res)
    return res;
}
export const List_Scheme=()=>async()=>{
    let userId=await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId
    }
    let res = await Services.postApi(Url.LIST_SCHEME, "", body)
    console.log("check response of dashboard data", res)
    return res;
}
export const Get_Story =()=>async()=>{
    let userId=await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId
    }
    let res = await Services.postApi(Url.GET_STORY, "", body)
    console.log("check response of dashboard data", res)
    return res;
}

export const update_story=(id,title,memberId,BrigadeId,story)=>async()=>{
    let userId=await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId,
        story_id:id,
        title:title,
        story:story,
        member_id:memberId,
        brigade_id:BrigadeId,

    }
    console.log("check body of update",body)
    let res = await Services.postApi(Url.UPDATE_STORY, "", body)
    console.log("check response of dashboard data", res)
    return res;
}

export const add_story=(title,story)=>async()=>{
    let userId=await Utility.getFromLocalStorge('userId')
    let body = {
        user_id: userId,
        title:title,
        story:story,
        member_id:2,
        brigade_id:3,

    }
    console.log("check body of update",body)
    let res = await Services.postApi(Url.ADD_STORY, "", body)
    console.log("check response of dashboard data", res)
    return res;
}

export const upload_profile_pic=(formData)=>async()=>{
    let res = await Services.FormDataPost(Url.UPLOAD_PROFILE_PIC, "", formData)
    console.log("check fomdata response???????", res)
    return res
}
export const upload_profile_pic2=(formData)=>async()=>{
    let res = await Services.FormDataPost(Url.UPLOAD_PROFILE_PIC, "", formData)
    console.log("check fomdata response???????", res)
    return res
}
export const EditPDRecords = (formData) => async () => {
    let res = await Services.FormDataPost(Url.EDIT_PD, "", formData)
    console.log("check fomdata response", res)
    return res
}

export const Event_List_api=()=>async(dispatch)=>{
    let res = await Services.postApi(Url.EVENT_LIST)
    console.log("check fomdata response", res)
    return res
}


export const Search_Member_api=(name)=>async(dispatch)=>{
    let body = {
      search:name
    }
    console.log("check body of update",body)
    let res = await Services.postApi(Url.MEMBER_SEARCH,"",body)
    console.log("check fomdata response", res)
    return res
}



export const Update_Scheme=(id,name,start,end,des)=>async(dispatch)=>{
    let userId=await Utility.getFromLocalStorge('userId')
let body={
    scheme_id:id,
    user_id:userId,
    scheme_name:name,
    scheme_starting_date:start,
    scheme_ending_date:end,
    scheme_description:des,

}
console.log("check body",body)
let res=await Services.postApi(Url.UPDATE_SCHEME,'',body)
console.log("check res",res)
if(res.status==true){
    return true
}
else {
    return false
}
}

export const   add_Attendence_api=()=>async()=>{
let memberId=await Utility.getFromLocalStorge("userId")
let brigadeId=await Utility.getFromLocalStorge('brigade_id')
let body={
    member_id:memberId,
    brigade_id:brigadeId
}
let res=await Services.postApi(Url.ADD_ATTENDENCE,'',body)
console.log("check repose add attende",res)
return res
}