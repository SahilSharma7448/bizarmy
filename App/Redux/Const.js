export const Login_Data = 'Login_Data';
export const GETUSER_GROUP_DATA = 'GETUSER_GROUP_DATA';
export const GROUP_NAME = 'GROUP_NAME';
export const AllStatus_Ventilation = 'AllStatus_Ventilation'
export const User_Data = 'User_Data'
export const AllSetting_Ventilation = 'AllSetting_Ventilation'
export const SETCURRENTAIRFLOWVALUE = 'SETCURRENTAIRFLOWVALUE'
export const SETCURRENTCO2VALUE = 'SETCURRENTCO2VALUE'
export const CURRENTDEVICESTATUS = 'CURRENTDEVICESTATUS'

export const SHOW_HEADER='SHOW_HEADER'
export const CURRENT_R_STATUS='CURRENT_R_STATUS'
export const MODAL_STATUS='MODAL_STATUS'
export const BUSSINESS_STATUS='BUSSINESS_STATUS'
export  const MODAL_STATUS_BUSINESS='MODAL_STATUS_BUSINESS'
export const ATTENDENCE_STATUS ='ATTENDENCE_STATUS'
export const BACK_STATUS='BACK_STATUS'
