import {
SHOW_HEADER,
CURRENT_R_STATUS,
MODAL_STATUS,
BUSSINESS_STATUS,
MODAL_STATUS_BUSINESS,
ATTENDENCE_STATUS,
BACK_STATUS
} from './Const';
const initialState = {
SHOW_HEADER:false,
CURRENT_R_STATUS:'All',
MODAL_STATUS:false,
BUSSINESS_STATUS:'All',
MODAL_STATUS_BUSINESS:false,
ATTENDENCE_STATUS:false,
BACK_STATUS:false
};
export default function (state = initialState, { type, payload }) {
  switch (type) {


    case SHOW_HEADER:
      return {
        ...state,
        SHOW_HEADER: payload,
      };
      case CURRENT_R_STATUS :
      return {
        ...state,
        CURRENT_R_STATUS: payload,
      };
      case MODAL_STATUS :
        return {
          ...state,
          MODAL_STATUS: payload,
        };
        case BUSSINESS_STATUS :
          return {
            ...state,
            BUSSINESS_STATUS: payload,
          };
          case MODAL_STATUS_BUSINESS :
            return {
              ...state,
              MODAL_STATUS_BUSINESS: payload,
            };
            case ATTENDENCE_STATUS :
              return {
                ...state,
                MODAL_STATUS_BUSINESS: payload,
              };
              case BACK_STATUS :
                return {
                  ...state,
                  BACK_STATUS: payload,
                };
    default: {
      return state;
    }
  }
}
